# Texts

Welcome to the Latin Text Archive (LTA) at the [Berlin-Brandenburg Academy of Sciences and Humanities](https://www.bbaw.de/en/) (BBAW).

This repository contains texts from the [Latin Text Archive](https://lta.bbaw.de).

All LTA Texts are provided in [XML TEI P5](https://tei-c.org/guidelines/p5/) encoding.

The texts are licensed under [Creative Commons BY 4.0](https://creativecommons.org/licenses/by/4.0/).

## Contact
 
* Dr. Tim Geelhaar <<geelhaar@em.uni-frankfurt.de>>
* Frank Wiegand <<wiegand@bbaw.de>>
