#!/usr/bin/perl

use 5.012;
use warnings;

use utf8;
use File::Basename qw(basename);
use FindBin;
use XML::LibXML;

use lib "$FindBin::Bin";
use LTATools qw(find_or_create_node xle);

my $parser = XML::LibXML->new;
my $ns = 'http://www.tei-c.org/ns/1.0';
my $xpc = XML::LibXML::XPathContext->new;
$xpc->registerNs( 't', $ns );

my %map = (
    1 => q[Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 1: 802–954. Paris, Imprimerie Nationale, 1876],
    2 => q[Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 2: 954–987. Paris, Imprimerie Nationale, 1880],
    3 => q[Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 3: 987–1027. Paris, Imprimerie Nationale, 1884],
    4 => q[Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 4: 1027–1090. Paris, Imprimerie Nationale, 1888],
    5 => q[Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894],
);

my ( $xpath, $el );

foreach my $dir ( glob "$FindBin::Bin/../cluny_charters/*" ) {
    next unless -d $dir;
    my ($vol) = basename($dir) =~ /(\d+)$/;
    next unless $vol;
    foreach my $file ( glob "$dir/*.xml" ) {
        my $source = $parser->load_xml( location => $file );

        $xpath = '/t:TEI/t:teiHeader/t:fileDesc/t:sourceDesc/t:p/t:bibl[@type="Edition"]';
        ( $el ) = find_or_create_node( $source, $xpc, $xpath );
        $el->removeChildNodes();
        $el->appendText( $map{$vol} );
        
        $xpath = '/t:TEI/t:teiHeader/t:fileDesc/t:sourceDesc/t:p/t:bibl[@type="Volume"]';
        ( $el ) = find_or_create_node( $source, $xpc, $xpath );
        $el->removeChildNodes();
        $el->appendText( $vol );

        my $out = $source->toString(2);
        utf8::decode($out);

        # unescape hex entities
        $out =~ s/&#x([a-fA-F0-9]+);/chr(hex $1)/eg;

        open( my $fh, '>:utf8', $file ) or die $!;
        print $fh $out;
        close $fh;
    }
}
