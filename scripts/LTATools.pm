package LTATools;

@ISA = qw(Exporter);
@EXPORT_OK = qw(xle find_or_create_node);

use warnings;
use strict;

=head1 METHODS

=cut

=head2 xle

    Shortcut for XML::LibXML::Element->new()

=cut

sub xle {
    XML::LibXML::Element->new(shift);
}

=head2 find_or_create_node

    Finds or creates (and returns) a node according
    to a given XPath.

    usage: C<find_or_create_node( $source, $xpc, $xpath )>

=cut

sub find_or_create_node {
    my ( $source, $xpc, $xpath ) = @_;

    my ($ret) = $xpc->findnodes( $xpath, $source );
    return $ret if $ret;

    my $full_path = '';
    foreach my $part ( grep { $_ } split /\// => $xpath ) {
        my ($there) = $xpc->findnodes( "$full_path/$part", $source );
        if ( !$there ) {
            my ( $prefix, $name, $attr_name, $attr_value) = $part =~ /^(?:(\w+):)?(\w+)(?:\[\@(\w+)=['"](\w+)['"]\])?$/;
            my $el = xle( $name );
            $el->setNamespace( 'http://www.tei-c.org/ns/1.0', '', 1 );
            $el->setAttribute( $attr_name, $attr_value ) if $attr_name;
            my ($ins) = $xpc->findnodes( $full_path, $source );
            $ins->appendChild($el);
        }
        $full_path = "$full_path/$part";
    }

    ($ret) = $xpc->findnodes( $full_path, $source );
    return $ret;
}

1;
__END__
