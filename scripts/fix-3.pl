#!/usr/bin/perl

use 5.012;
use warnings;

use utf8;
use File::Basename qw(basename);
use FindBin;
use XML::LibXML;

use lib "$FindBin::Bin";
use LTATools qw(find_or_create_node xle);

my $parser = XML::LibXML->new;
my $ns = 'http://www.tei-c.org/ns/1.0';
my $xpc = XML::LibXML::XPathContext->new;
$xpc->registerNs( 't', $ns );

my ( $xpath, $el );

foreach my $dir ( glob "$FindBin::Bin/../cluny_charters/*" ) {
    next unless -d $dir;
    my ($vol) = basename($dir) =~ /(\d+)$/;
    next unless $vol;
    foreach my $file ( glob "$dir/*.xml" ) {
        my $source = $parser->load_xml( location => $file );

        ( $el ) = $xpc->findnodes('/t:TEI/t:teiHeader/t:fileDesc/t:titleStmt/t:respStmt/t:persName[@key="JS" and not(t:forename/text() = "Jashty")]', $source);
        next unless $el;
        my ( $forename ) = $xpc->findnodes('t:forename', $el);
        $forename->removeChildNodes();
        $forename->appendText( 'Jashty' );

        my $out = $source->toString(2);
        utf8::decode($out);

        # unescape hex entities
        $out =~ s/&#x([a-fA-F0-9]+);/chr(hex $1)/eg;

        open( my $fh, '>:utf8', $file ) or die $!;
        print $fh $out;
        close $fh;
    }
}
