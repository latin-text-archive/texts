<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 4066</title>
        <title type="sub">BULLA INNOCENTII PAPÆ II, QUA CONFIRMAT POSSESSIONES ROMANI
                    MONASTERII, PETENTE PETRO, CLUNIACENSI ABBATE.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1139.04.30</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1139</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">416</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=4066</bibl>
          <bibl type="Reference">(Cartulaire de Romainmotier, p. 177.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Charter</classCode>
        <classCode scheme="Text_Type_Class">Legal</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:33:20 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:33:20 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <p>
          <s>
            <w lemma="[{'id':481962,'ls':[{'id':481963,'name':'Innocentius','wfs':[{'id':664199,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Innocentius'},{'id':241370704,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Innocentius'}]}],'name':'Innocentius@NP'}]">Innocentius</w>
            <c> </c>
            <w lemma="[{'id':998014,'ls':[{'id':3910,'name':'episcopus','wfs':[{'id':998029,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'episcopus'}]}],'name':'episcopus@NN'}]">episcopus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':110027,'ls':[{'id':110028,'name':'sicut','wfs':[{'id':110029,'morph':{'Pos':'CON'},'name':'sicut'}]}],'name':'sicut@CON'}]">Sicut</w>
            <c> </c>
            <w lemma="[{'id':1098908,'ls':[{'id':1098943,'name':'injustum','wfs':[{'id':1098944,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'injusta'},{'id':1098949,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'injusta'},{'id':1098950,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'injusta'}]}],'name':'iniustum@NN'}]">injusta</w>
            <c> </c>
            <w lemma="[{'id':6740485,'ls':[{'id':7225,'name':'posco','wfs':[{'id':6740487,'morph':{'Casus':'ABLATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'poscentibus'},{'id':6740503,'morph':{'Casus':'ABLATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'poscentibus'},{'id':6740495,'morph':{'Casus':'ABLATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'poscentibus'},{'id':6740497,'morph':{'Casus':'DATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'poscentibus'},{'id':6740489,'morph':{'Casus':'DATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'poscentibus'},{'id':6740505,'morph':{'Casus':'DATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'poscentibus'}]}],'name':'posco@V'}]">poscentibus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w>
            <c>.</c>
            <c> </c>
            <note type="editorial" resp="TG">
              <s>
                <w lemma="[{'id':243116350,'ls':[{'id':243116351,'name':'NON_WORD','wfs':[{'id':244750471,'morph':{'Pos':'XY'},'name':'The'}]}],'name':'NON_WORD@XY'}]">The</w>
                <c> </c>
                <w>text</w>
                <c> </c>
                <w>must</w>
                <c> </c>
                <w lemma="[{'id':810862,'ls':[{'id':810863,'name':'bes','wfs':[{'id':243268621,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'be'}]}],'name':'bes@NN'}]">be</w>
                <c> </c>
                <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
                <c> </c>
                <w>Pahud</w>
                <c>,</c>
                <c> </c>
                <w lemma="[{'id':285465,'ls':[{'id':285466,'name':'Alexander','wfs':[{'id':243181758,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Alexandre'}]}],'name':'Alexander@NP'}]">Alexandre</w>
                <c> </c>
                <c>(</c>
                <w>ed</w>
                <c>.</c>
                <c>)</c>
                <c>,</c>
                <c> </c>
              </s>
              <s>
                <w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':594996790,'morph':{'Pos':'FM'},'name':'le'}]}],'name':'NON_LATIN@FM'}]">Le</w>
                <c> </c>
                <w>cartulaire</w>
                <c> </c>
                <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
                <c> </c>
                <w>Romainmôtier</w>
                <c> </c>
                <c>(</c>
                <w>XII</w>
                <c> </c>
                <w>siecle</w>
                <c>)</c>
                <c>:</c>
                <c> </c>
                <w lemma="[{'id':1111462,'ls':[{'id':1111463,'name':'introductio','wfs':[{'id':1111468,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'introduction'}]}],'name':'introductio@NN'}]">introduction</w>
                <c> </c>
                <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
                <c> </c>
                <w lemma="[{'id':986187,'ls':[{'id':3784,'name':'editio','wfs':[{'id':986192,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'edition'}]}],'name':'editio@NN'}]">edition</w>
                <c> </c>
                <w>critqieu</w>
                <c> </c>
                <c>(</c>
                <w>Cahiers</w>
                <c> </c>
                <w>lausannois</w>
                <c> </c>
                <w>d'histoire</w>
                <c> </c>
                <w>medievale</w>
                <c> </c>
                <w>21</w>
                <c>)</c>
                <c> </c>
                <w lemma="[{'id':244494563,'ls':[{'id':244494560,'name':'ProperName','wfs':[{'id':798954314,'morph':{'Pos':'NE'},'name':'Lausanne'}]}],'name':'ProperName@NE'}]">Lausanne</w>
                <c>:</c>
                <c> </c>
                <w>Universite</w>
                <c> </c>
                <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
                <c> </c>
                <w lemma="[{'id':244494563,'ls':[{'id':244494560,'name':'ProperName','wfs':[{'id':798954314,'morph':{'Pos':'NE'},'name':'Lausanne'}]}],'name':'ProperName@NE'}]">Lausanne</w>
                <c> </c>
                <w>1998</w>
                <c>,</c>
                <c> </c>
                <w>which</w>
                <c> </c>
                <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':532695117,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO'},'name':'has'},{'id':242816006,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'has'}]}],'name':'hic@PRO'}]">has</w>
                <c> </c>
                <w>been</w>
                <c> </c>
                <w>inaccessible</w>
                <c>.</c>
                <c> </c>
              </s>
            </note>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
