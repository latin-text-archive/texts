<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 4149</title>
        <title type="sub">AYMO ESYNGRES DE GROSUM DAT MONACHIS CLUNIACENSIBUS SUMMATAM SALIS
                    IN BADERNA.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1150 ca.</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1150</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">510</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=4149</bibl>
          <bibl type="Reference">(B. s. 18, fol. 296 vº.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Charter</classCode>
        <classCode scheme="Text_Type_Class">Legal</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:27:26 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:27:26 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <p>
          <s>
            <w lemma="[{'id':1192980,'ls':[{'id':1192981,'name':'notus','wfs':[{'id':1192990,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'notum'}]}],'name':'notus@NN'}]">Notum</w>
            <c> </c>
            <w lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':244519866,'morph':{'Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'sit'}]}],'name':'sum@V'}]">sit</w>
            <c> </c>
            <w lemma="[{'id':2744416,'ls':[{'id':6625,'name':'omnis','wfs':[{'id':2744427,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744419,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744435,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744434,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744428,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744420,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'}]}],'name':'omnis@ADJ'}]">omnibus</w>
            <c> </c>
            <w lemma="[{'id':245290,'ls':[{'id':8736,'name':'tam','wfs':[{'id':245291,'morph':{'Pos':'ADV'},'name':'tam'}]}],'name':'tam@ADV'}]">tam</w>
            <c> </c>
            <w lemma="[{'id':2358901,'ls':[{'id':2358902,'name':'futurus','wfs':[{'id':2358905,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'futuris'},{'id':2358904,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'futuris'},{'id':2358911,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'futuris'},{'id':2358910,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'futuris'},{'id':2358917,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'futuris'},{'id':2358916,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'futuris'}]}],'name':'futurus@ADJ'}]">futuris</w>
            <c> </c>
            <w lemma="[{'id':225505,'ls':[{'id':7683,'name':'quam','wfs':[{'id':225506,'morph':{'Pos':'ADV'},'name':'quam'}]}],'name':'quam@ADV'}]">quam</w>
            <c> </c>
            <w lemma="[{'id':2874459,'ls':[{'id':242458017,'name':'presens','wfs':[{'id':244213486,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213439,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213461,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213466,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213434,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213491,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'}]}],'name':'praesens@ADJ'}]">presentibus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109986,'ls':[{'id':109987,'name':'quod','wfs':[{'id':109988,'morph':{'Pos':'CON'},'name':'quod'}]}],'name':'quod@CON'}]">quod</w>
            <c> </c>
            <w lemma="[{'id':496423619,'ls':[{'id':496423620,'name':'Aymo','wfs':[{'id':496423621,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Aymo'}]}],'name':'Aymo@NP'}]">Aymo</w>
            <pb n="511"/>
            <w>Esyngrez</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w lemma="[{'id':1057969,'ls':[{'id':1057970,'name':'grosa','wfs':[{'id':1057976,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'grosum'}]}],'name':'grosa@NN'}]">Grosum</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':244511016,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382851706,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382871756,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':594773492,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]">qui</w>
            <c> </c>
            <w lemma="[{'id':4706561,'ls':[{'id':3041,'name':'decedo','wfs':[{'id':4706671,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'decessit'},{'id':368074,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'INTRANSITIVE','Voice':'ACTIVE'},'name':'decessit'}]}],'name':'decedo@V'}]">decessit</w>
            <c> </c>
            <w lemma="[{'id':109487,'ls':[{'id':109488,'name':'apud','wfs':[{'id':109489,'morph':{'Pos':'AP'},'name':'apud'}]}],'name':'apud@AP'}]">apud</w>
            <c> </c>
            <w lemma="[{'id':535775,'ls':[{'id':535776,'name':'Nazara','wfs':[{'id':535792,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NE'},'name':'Nazara'},{'id':535793,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NE'},'name':'Nazara'},{'id':535787,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NE'},'name':'Nazara'}]}],'name':'Nazara@NE'}]">Nazara</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':1118450,'ls':[{'id':5549,'name':'iter','wfs':[{'id':241447852,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'itinere'}]}],'name':'iter@NN'}]">itinere</w>
            <c> </c>
            <w lemma="[{'id':1739027,'ls':[{'id':1318,'name':'beatus','wfs':[{'id':241731816,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'beati'},{'id':1739075,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'beati'},{'id':1739072,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'beati'},{'id':241731812,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'beati'},{'id':1739156,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'beati'},{'id':1739133,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'beati'}]}],'name':'beatus@ADJ'}]">beati</w>
            <c> </c>
            <w lemma="[{'id':241372198,'ls':[{'id':241372199,'name':'Jacobus','wfs':[{'id':241372202,'morph':{'Pos':'NP'},'name':'jacobi'}]}],'name':'Jacobus@NP'}]">Jacobi</w>
            <c> </c>
            <w lemma="[{'id':767705,'ls':[{'id':244741368,'name':'apostolus','wfs':[{'id':767711,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'apostoli'},{'id':767718,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'apostoli'},{'id':767712,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'apostoli'}]}],'name':'apostolus@NN'}]">apostoli</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':5109084,'ls':[{'id':3001,'name':'do','wfs':[{'id':5109405,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'dedit'}]}],'name':'do@V'}]">dedit</w>
            <c> </c>
            <w lemma="[{'id':958093,'ls':[{'id':958094,'name':'deus','wfs':[{'id':958106,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'deo'},{'id':958105,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'deo'}]}],'name':'deus@NN'}]">deo</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':592238,'ls':[{'id':592239,'name':'Sanctus','wfs':[{'id':592251,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancto'},{'id':592250,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancto'},{'id':488483803,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancto'},{'id':488483802,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancto'}]}],'name':'Sanctus@NP'}]">Sancto</w>
            <c> </c>
            <w lemma="[{'id':558713,'ls':[{'id':558714,'name':'Petrus','wfs':[{'id':558725,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Petro'},{'id':558726,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Petro'}]}],'name':'Petrus@NP'}]">Petro</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':555507,'ls':[{'id':555508,'name':'Paulus','wfs':[{'id':688243,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Paulo'},{'id':688244,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Paulo'}]}],'name':'Paulus@NP'}]">Paulo</w>
            <c> </c>
            <w lemma="[{'id':1481107,'ls':[{'id':1481108,'name':'Cluniacensis','wfs':[{'id':1481151,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481140,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481146,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481139,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481152,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481145,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'}]}],'name':'Cluniacensis@ADJ'}]">Cluniacensi</w>
            <c>,</c>
            <c> </c>
            <w>sommatam</w>
            <c> </c>
            <w lemma="[{'id':1338403,'ls':[{'id':8058,'name':'sal','wfs':[{'id':243454055,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'salis'},{'id':243454052,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'salis'},{'id':1338418,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'salis'}]}],'name':'sal@NN'}]">salis</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w>baderna</w>
            <c> </c>
            <w lemma="[{'id':225505,'ls':[{'id':7683,'name':'quam','wfs':[{'id':225506,'morph':{'Pos':'ADV'},'name':'quam'}]}],'name':'quam@ADV'}]">quam</w>
            <c> </c>
            <w lemma="[{'id':3415565,'ls':[{'id':360,'name':'adquiro','wfs':[{'id':561925233,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PLUPERFECT','Voice':'ACTIVE'},'name':'adquisierat'}]}],'name':'acquiro@V'}]">adquisierat</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':6206113,'ls':[{'id':5954,'name':'mando','wfs':[{'id':244515776,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'mandavitque'}]}],'name':'mando@V'}]">mandavitque</w>
            <c> </c>
            <w lemma="[{'id':1040499,'ls':[{'id':4492,'name':'frater','wfs':[{'id':290588,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'},{'id':241435784,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'}]}],'name':'frater@NN'}]">fratribus</w>
            <c> </c>
            <w lemma="[{'id':1406782,'ls':[{'id':488516594,'name':'suus','wfs':[{'id':488516607,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'suis'},{'id':488516608,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'suis'}]}],'name':'suus@NN'}]">suis</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':1444118,'ls':[{'id':9084,'name':'uxor','wfs':[{'id':1444130,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'uxori'},{'id':243491695,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'uxori'}]}],'name':'uxor@NN'}]">uxori</w>
            <c> </c>
            <w lemma="[{'id':241528,'ls':[{'id':241529,'name':'sue','wfs':[{'id':241531,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'sue'}]}],'name':'sue@ADV'}]">sue</w>
            <c> </c>
            <w lemma="[{'id':109986,'ls':[{'id':109987,'name':'quod','wfs':[{'id':109988,'morph':{'Pos':'CON'},'name':'quod'}]}],'name':'quod@CON'}]">quod</w>
            <c> </c>
            <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':244509722,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187641,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187642,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':242816022,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'}]}],'name':'hic@PRO'}]">hoc</w>
            <c> </c>
            <w lemma="[{'id':7086882,'ls':[{'id':7788,'name':'reddo','wfs':[{'id':7087148,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'IMPERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'redderent'}]}],'name':'reddo@V'}]">redderent</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':110020,'ls':[{'id':110021,'name':'si','wfs':[{'id':110022,'morph':{'Pos':'CON'},'name':'si'}]}],'name':'si@CON'}]">Si</w>
            <c> </c>
            <w lemma="[{'id':109792,'ls':[{'id':109793,'name':'autem','wfs':[{'id':109794,'morph':{'Pos':'CON'},'name':'autem'}]}],'name':'autem@CON'}]">autem</w>
            <c> </c>
            <w lemma="[{'id':6326127,'ls':[{'id':6326128,'name':'nolo','wfs':[{'id':244516006,'morph':{'Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'IMPERFECT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'nollent'}]}],'name':'nolo@V'}]">nollent</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':110077,'ls':[{'id':110078,'name':'vel','wfs':[{'id':110080,'morph':{'Pos':'CON'},'name':'vel'}]}],'name':'vel@CON'}]">vel</w>
            <c> </c>
            <w lemma="[{'id':109559,'ls':[{'id':109560,'name':'e','wfs':[{'id':109561,'morph':{'Pos':'AP'},'name':'e'}]}],'name':'e@AP'}]">e</w>
            <c> </c>
            <w lemma="[{'id':1178111,'ls':[{'id':6265,'name':'mundus','wfs':[{'id':1178137,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'mundo'},{'id':1178138,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'mundo'}]}],'name':'mundus@NN'}]">mundo</w>
            <c> </c>
            <w lemma="[{'id':6240092,'ls':[{'id':6105,'name':'migro','wfs':[{'id':6240379,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'IMPERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'migrarent'}]}],'name':'migro@V'}]">migrarent</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1173036,'ls':[{'id':1173037,'name':'monachus','wfs':[{'id':1173044,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'monachi'},{'id':1173043,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'monachi'},{'id':1173050,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'monachi'}]}],'name':'monachus@NN'}]">monachi</w>
            <c> </c>
            <w lemma="[{'id':2859931,'ls':[{'id':2860044,'name':'predictus','wfs':[{'id':2860083,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'predicti'},{'id':2860130,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'predicti'},{'id':2860146,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'predicti'},{'id':2860085,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'predicti'}]}],'name':'praedictus@ADJ'}]">predicti</w>
            <c> </c>
            <w lemma="[{'id':1143697,'ls':[{'id':5796,'name':'locus','wfs':[{'id':1143705,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'loci'},{'id':1143712,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'loci'},{'id':1143706,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'loci'}]}],'name':'locus@NN'}]">loci</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w>baderna</w>
            <c> </c>
            <w lemma="[{'id':3384337,'ls':[{'id':3384338,'name':'accipio','wfs':[{'id':3384611,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'IMPERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'acciperent'}]}],'name':'accipio@V'}]">acciperent</w>
            <c> </c>
            <w lemma="[{'id':242816449,'ls':[{'id':242816493,'name':'quicunque','wfs':[{'id':242816510,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'INDEFINITE'},'name':'quicunque'},{'id':297496420,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'INDEFINITE'},'name':'quicunque'}]}],'name':'quicumque@PRO'}]">quicunque</w>
            <c> </c>
            <w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510015,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'eam'},{'id':382849839,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'eam'}]}],'name':'is@PRO'}]">eam</w>
            <c> </c>
            <w lemma="[{'id':7783838,'ls':[{'id':8779,'name':'teneo','wfs':[{'id':7784171,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'IMPERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'teneret'}]}],'name':'teneo@V'}]">teneret</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':1415950,'ls':[{'id':1415951,'name':'testis','wfs':[{'id':1415959,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415969,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':498535603,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415968,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415952,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415961,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415960,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'}]}],'name':'testis@NN'}]">Testes</w>
            <c> </c>
            <w lemma="[{'id':482230,'ls':[{'id':488454214,'name':'Johannes','wfs':[{'id':488454220,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Johannes'},{'id':488454219,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Johannes'}]}],'name':'Ioannes@NP'}]">Johannes</w>
            <c> </c>
            <w lemma="[{'id':570385110,'ls':[{'id':572662151,'name':'Rosset','wfs':[{'id':572662154,'morph':{'Casus':'DATIVE','DeclensionType':'INDECLINABLE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NE'},'name':'Rosset'},{'id':572662155,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'INDECLINABLE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NE'},'name':'Rosset'},{'id':572662152,'morph':{'Casus':'NOMINATIVE','DeclensionType':'INDECLINABLE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NE'},'name':'Rosset'},{'id':572662153,'morph':{'Casus':'GENITIVE','DeclensionType':'INDECLINABLE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NE'},'name':'Rosset'},{'id':572662156,'morph':{'Casus':'ABLATIVE','DeclensionType':'INDECLINABLE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NE'},'name':'Rosset'}]}],'name':'Rossetum@NE'}]">Rosset</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1157529,'ls':[{'id':241454096,'name':'marcha','wfs':[{'id':245539095,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'marchis'},{'id':245539096,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'marchis'}]}],'name':'marca@NN'}]">Marchis</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w lemma="[{'id':1057969,'ls':[{'id':1057970,'name':'grosa','wfs':[{'id':1057976,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'grosum'}]}],'name':'grosa@NN'}]">Grosum</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':608889,'ls':[{'id':608890,'name':'Stephanus','wfs':[{'id':608905,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Stephanus'}]}],'name':'Stephanus@NP'}]">Stephanus</w>
            <c> </c>
            <w>Bertins</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':558713,'ls':[{'id':558714,'name':'Petrus','wfs':[{'id':558729,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Petrus'}]}],'name':'Petrus@NP'}]">Petrus</w>
            <c> </c>
            <w>Dibum</w>
            <c>,</c>
            <c> </c>
            <w>Albespinus</w>
            <c> </c>
            <w lemma="[{'id':2768942,'ls':[{'id':6825,'name':'parvus','wfs':[{'id':2769195,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'SUPERLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'minimus'}]}],'name':'parvus@ADJ'}]">Minimus</w>
            <c>,</c>
            <c> </c>
            <w>juglers</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':491157,'ls':[{'id':491158,'name':'Lambertus','wfs':[{'id':243227669,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Lambertus'},{'id':491172,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Lambertus'}]}],'name':'Lambertus@NP'}]">Lambertus</w>
            <c> </c>
            <w lemma="[{'id':1164559,'ls':[{'id':6075,'name':'mercator','wfs':[{'id':1164572,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'mercator'},{'id':243393738,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'mercator'}]}],'name':'mercator@NN'}]">mercator</w>
            <c>.</c>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
