<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 4155</title>
        <title type="sub">CHARTA RIVIANI MONTIS.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1150 ca.</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1150</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">513</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=4155</bibl>
          <bibl type="Reference">(B. s. 25.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Charter</classCode>
        <classCode scheme="Text_Type_Class">Legal</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:27:23 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:27:23 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <p>
          <s>
            <w lemma="[{'id':849184,'ls':[{'id':835608,'name':'carta','wfs':[{'id':835621,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'carta'},{'id':835617,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'carta'},{'id':835622,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'carta'}]}],'name':'charta@NN'}]">Carta</w>
            <c> </c>
            <w>Riviani</w>
            <c> </c>
            <w lemma="[{'id':1174057,'ls':[{'id':6203,'name':'mons','wfs':[{'id':1174070,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'montis'}]}],'name':'mons@NN'}]">Montis</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':7319987,'ls':[{'id':8159,'name':'scio','wfs':[{'id':7320171,'morph':{'ConjugationType':'FOURTH_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'sciant'}]}],'name':'scio@V'}]">Sciant</w>
            <c> </c>
            <w lemma="[{'id':6837973,'ls':[{'id':244517229,'name':'presento','wfs':[{'id':598763831,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'presentes'}]},{'id':803027822,'name':'presento','wfs':[{'id':803028215,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'presentes'}]}],'name':'praesento@V'}]">presentes</w>
            <c> </c>
            <w lemma="[{'id':109772,'ls':[{'id':109773,'name':'atque','wfs':[{'id':109774,'morph':{'Pos':'CON'},'name':'atque'}]}],'name':'atque@CON'}]">atque</w>
            <c> </c>
            <w lemma="[{'id':1352021,'ls':[{'id':1352022,'name':'secuturus','wfs':[{'id':1352028,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'secuturi'},{'id':1352029,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'secuturi'},{'id':1352023,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'secuturi'},{'id':1352035,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'secuturi'}]}],'name':'secuturus@NN'}]">secuturi</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109986,'ls':[{'id':109987,'name':'quod','wfs':[{'id':109988,'morph':{'Pos':'CON'},'name':'quod'}]}],'name':'quod@CON'}]">quod</w>
            <c> </c>
            <w lemma="[{'id':242816519,'ls':[{'id':242816520,'name':'quidam','wfs':[{'id':244511146,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'INDEFINITE'},'name':'quidam'},{'id':244511164,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'INDEFINITE'},'name':'quidam'}]}],'name':'quidam@PRO'}]">quidam</w>
            <c> </c>
            <w lemma="[{'id':1167745,'ls':[{'id':6107,'name':'miles','wfs':[{'id':243394616,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'milites'},{'id':243394597,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'milites'},{'id':243394605,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'milites'},{'id':243394615,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'milites'},{'id':243394607,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'milites'},{'id':243394606,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'milites'}]}],'name':'miles@NN'}]">milites</w>
            <c> </c>
            <w lemma="[{'id':109453,'ls':[{'id':109454,'name':'ad','wfs':[{'id':109455,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'ad@AP'},{'id':109433,'ls':[{'id':607803638,'name':'ad','wfs':[{'id':607803639,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'a@AP'}]">ad</w>
            <c> </c>
            <w lemma="[{'id':911948,'ls':[{'id':911952,'name':'conversio','wfs':[{'id':243304495,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'conversionem'}]}],'name':'conversio@NN'}]">conversionem</w>
            <c> </c>
            <w lemma="[{'id':7964639,'ls':[{'id':9166,'name':'venio','wfs':[{'id':7965049,'morph':{'Casus':'ACCUSATIVE','ConjugationType':'FOURTH_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'INTRANSITIVE','Voice':'ACTIVE'},'name':'venientes'},{'id':7965041,'morph':{'Casus':'ACCUSATIVE','ConjugationType':'FOURTH_CONJUGATION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'INTRANSITIVE','Voice':'ACTIVE'},'name':'venientes'},{'id':7965048,'morph':{'Casus':'NOMINATIVE','ConjugationType':'FOURTH_CONJUGATION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'INTRANSITIVE','Voice':'ACTIVE'},'name':'venientes'},{'id':7965056,'morph':{'Casus':'NOMINATIVE','ConjugationType':'FOURTH_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'INTRANSITIVE','Voice':'ACTIVE'},'name':'venientes'},{'id':556556003,'morph':{'Pos':'V'},'name':'venientes'}]}],'name':'venio@V'}]">venientes</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':5004553,'ls':[{'id':5004554,'name':'dimitto','wfs':[{'id':5004839,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'dimiserunt'}]}],'name':'dimitto@V'}]">dimiserunt</w>
            <c> </c>
            <w lemma="[{'id':592238,'ls':[{'id':592239,'name':'Sanctus','wfs':[{'id':592251,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancto'},{'id':592250,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancto'},{'id':488483803,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancto'},{'id':488483802,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancto'}]}],'name':'Sanctus@NP'}]">Sancto</w>
            <c> </c>
            <w lemma="[{'id':558713,'ls':[{'id':558714,'name':'Petrus','wfs':[{'id':558725,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Petro'},{'id':558726,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Petro'}]}],'name':'Petrus@NP'}]">Petro</w>
            <c> </c>
            <w lemma="[{'id':1156314,'ls':[{'id':1156315,'name':'mansus','wfs':[{'id':1156324,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'mansum'}]}],'name':'mansus@NN'}]">mansum</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w>Vizella</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109541,'ls':[{'id':109542,'name':'cum','wfs':[{'id':109543,'morph':{'Pos':'AP'},'name':'cum'}]}],'name':'cum@AP'}]">cum</w>
            <c> </c>
            <w lemma="[{'id':3287090,'ls':[{'id':3287203,'name':'vineus','wfs':[{'id':3287266,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'vinea'},{'id':3287258,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'vinea'},{'id':3287275,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'vinea'},{'id':3287246,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'vinea'},{'id':3287278,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'vinea'},{'id':3287260,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'vinea'}]}],'name':'vineus@ADJ'}]">vinea</w>
            <c> </c>
            <w lemma="[{'id':109772,'ls':[{'id':109773,'name':'atque','wfs':[{'id':109774,'morph':{'Pos':'CON'},'name':'atque'}]}],'name':'atque@CON'}]">atque</w>
            <c> </c>
            <w lemma="[{'id':2744416,'ls':[{'id':6625,'name':'omnis','wfs':[{'id':2744427,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744419,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744435,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744434,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744428,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'},{'id':2744420,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnibus'}]}],'name':'omnis@ADJ'}]">omnibus</w>
            <c> </c>
            <w lemma="[{'id':6672007,'ls':[{'id':7026,'name':'pertineo','wfs':[{'id':244516414,'morph':{'Pos':'V'},'name':'pertinentiis'}]}],'name':'pertineo@V'}]">pertinentiis</w>
            <c>:</c>
            <c> </c>
            <w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':242816237,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'id'},{'id':244510109,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'id'}]}],'name':'is@PRO'}]">id</w>
            <c> </c>
            <w lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':242825881,'morph':{'Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'est'}]}],'name':'sum@V'}]">est</w>
            <c> </c>
            <w lemma="[{'id':256364,'ls':[{'id':256365,'name':'tres','wfs':[{'id':256375,'morph':{'Casus':'ACCUSATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NUM'},'name':'tres'},{'id':256374,'morph':{'Casus':'VOCATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NUM'},'name':'tres'},{'id':256373,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NUM'},'name':'tres'},{'id':256394,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NUM'},'name':'tres'},{'id':256393,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NUM'},'name':'tres'},{'id':256383,'morph':{'Casus':'VOCATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NUM'},'name':'tres'},{'id':256366,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NUM'},'name':'tres'},{'id':256382,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NUM'},'name':'tres'}]}],'name':'tres@NUM'}]">tres</w>
            <c> </c>
            <w lemma="[{'id':1223665,'ls':[{'id':6775,'name':'panis','wfs':[{'id':1223683,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'panes'},{'id':1223687,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'panes'},{'id':1223688,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'panes'}]}],'name':'panis@NN'}]">panes</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1359727,'ls':[{'id':1359728,'name':'sextarium','wfs':[{'id':243460832,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'sextarium'},{'id':241480472,'morph':{'Pos':'NN'},'name':'sextarium'},{'id':241480473,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NN'},'name':'sextarium'},{'id':243460836,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'sextarium'},{'id':1359730,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'sextarium'}]}],'name':'sextarium@NN'}]">sextarium</w>
            <c> </c>
            <w lemma="[{'id':8013358,'ls':[{'id':9265,'name':'vincio','wfs':[{'id':8013991,'morph':{'ConjugationType':'FOURTH_CONJUGATION','Mood':'IMPERATIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'vini'}]}],'name':'vincio@V'}]">vini</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]">iiii</w>
            <c> </c>
            <w lemma="[{'id':804337,'ls':[{'id':1252,'name':'avus','wfs':[{'id':501627532,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'avene'}]}],'name':'avus@NN'}]">avene</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1161055,'ls':[{'id':1161056,'name':'medietas','wfs':[{'id':501675878,'morph':{'Pos':'NN'},'name':'medietatemque'},{'id':501675879,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'medietatemque'}]}],'name':'medietas@NN'}]">medietatemque</w>
            <c> </c>
            <w lemma="[{'id':8013358,'ls':[{'id':9265,'name':'vincio','wfs':[{'id':8013991,'morph':{'ConjugationType':'FOURTH_CONJUGATION','Mood':'IMPERATIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'vini'}]}],'name':'vincio@V'}]">vini</w>
            <c> </c>
            <w lemma="[{'id':243177186,'ls':[{'id':243177187,'name':'supradicte','wfs':[{'id':243177189,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'supradicte'}]}],'name':'supradicte@ADV'}]">supradicte</w>
            <c> </c>
            <w lemma="[{'id':1465588,'ls':[{'id':9271,'name':'vinea','wfs':[{'id':758299895,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'vinee'},{'id':743491085,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'vinee'},{'id':743491083,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'vinee'}]}],'name':'vinea@NN'}]">vinee</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':244509722,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187641,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187642,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':242816022,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'}]}],'name':'hic@PRO'}]">Hoc</w>
            <c> </c>
            <w lemma="[{'id':109792,'ls':[{'id':109793,'name':'autem','wfs':[{'id':109794,'morph':{'Pos':'CON'},'name':'autem'}]}],'name':'autem@CON'}]">autem</w>
            <c> </c>
            <w lemma="[{'id':5109084,'ls':[{'id':3001,'name':'do','wfs':[{'id':5109363,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'dederunt'}]}],'name':'do@V'}]">dederunt</w>
            <c> </c>
            <w lemma="[{'id':473266,'ls':[{'id':473267,'name':'Humbertus','wfs':[{'id':473281,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Humbertus'},{'id':661229,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Humbertus'}]}],'name':'Humbertus@NP'}]">Humbertus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':473020,'ls':[{'id':473021,'name':'Hugo','wfs':[{'id':488451312,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Hugo'},{'id':241369646,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Hugo'}]}],'name':'Hugo@NP'}]">Hugo</w>
            <c> </c>
            <w>Rebotins</w>
            <c>.</c>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
