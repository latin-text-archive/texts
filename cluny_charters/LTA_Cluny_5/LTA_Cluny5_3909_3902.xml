<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 3902</title>
        <title type="sub">CHARTA QUA LEODEGARIUS, EPISCOPUS VIVARIENSIS, ABBATI CLUNIACENSI
                    ET AYMARO, PRIORI RUPPONENSI SEX ECCLESIAS IN MANDAMENTO SANCTI ALBANI SITAS,
                    IBIQUE NOMINATIM APPELLATAS, CONFERT.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1112</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1112</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">254</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3902</bibl>
          <bibl type="Reference">(D. Vaissete, Hist. de Languedoc, t. II,
                    c. 380.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Charter</classCode>
        <classCode scheme="Text_Type_Class">Legal</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:42:22 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:42:22 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <pb n="254"/>
        <p>
          <s>
            <w lemma="[{'id':1192980,'ls':[{'id':1192981,'name':'notus','wfs':[{'id':1192990,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'notum'}]}],'name':'notus@NN'}]">Notum</w>
            <c> </c>
            <w lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':244519866,'morph':{'Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'sit'}]}],'name':'sum@V'}]">sit</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w>
            <c>.</c>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109986,'ls':[{'id':109987,'name':'quod','wfs':[{'id':109988,'morph':{'Pos':'CON'},'name':'quod'}]}],'name':'quod@CON'}]">quod</w>
            <c> </c>
            <w lemma="[{'id':242815965,'ls':[{'id':242815966,'name':'ego','wfs':[{'id':244509626,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509632,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509629,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'}]}],'name':'ego@PRO'}]">ego</w>
            <c> </c>
            <w lemma="[{'id':496986,'ls':[{'id':496987,'name':'Leodegarius','wfs':[{'id':669446,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Leodegarius'},{'id':669435,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Leodegarius'},{'id':496988,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Leodegarius'}]}],'name':'Leodegarius@NP'}]">Leodegarius</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':245550023,'ls':[{'id':245550024,'name':'Vivariensis','wfs':[{'id':786815766,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Vivariensis'},{'id':786815778,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Vivariensis'},{'id':786815756,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Vivariensis'},{'id':786815711,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Vivariensis'},{'id':786815758,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Vivariensis'},{'id':245550025,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Vivariensis'},{'id':786815768,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Vivariensis'},{'id':786815691,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Vivariensis'},{'id':786815754,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Vivariensis'},{'id':786815770,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Vivariensis'}]}],'name':'Vivariensis@ADJ'}]">Vivariensis</w>
            <c> </c>
            <w lemma="[{'id':985146,'ls':[{'id':3766,'name':'ecclesia','wfs':[{'id':497261383,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesiæ'},{'id':497261382,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesiæ'},{'id':241428906,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'ecclesiæ'}]}],'name':'ecclesia@NN'}]">ecclesiæ</w>
            <c> </c>
            <w lemma="[{'id':1168860,'ls':[{'id':6116,'name':'minister','wfs':[{'id':1168873,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'minister'}]}],'name':'minister@NN'}]">minister</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w>
            <c>.</c>
            <c> </c>
            <note resp="TG">
              <s>
                <w lemma="[{'id':243116350,'ls':[{'id':243116351,'name':'NON_WORD','wfs':[{'id':244750471,'morph':{'Pos':'XY'},'name':'The'}]}],'name':'NON_WORD@XY'}]">The</w>
                <c> </c>
                <w>text</w>
                <c> </c>
                <w>could</w>
                <c> </c>
                <w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':245547189,'morph':{'Pos':'FM'},'name':'not'}]}],'name':'NON_LATIN@FM'}]">not</w>
                <c> </c>
                <w lemma="[{'id':810862,'ls':[{'id':810863,'name':'bes','wfs':[{'id':243268621,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'be'}]}],'name':'bes@NN'}]">be</w>
                <c> </c>
                <w>found</w>
                <c>.</c>
              </s>
            </note>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
