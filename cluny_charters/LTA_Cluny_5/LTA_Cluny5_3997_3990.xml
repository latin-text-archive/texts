<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 3990</title>
        <title type="sub">EPISTOLA HONORII PAPÆ II AD MONACHOS CLUNIACENSES, UT A PONTIO,
                    MONASTERII CLUNIACENSIS INVASORE, TANQUAM SCHISMATICO ABSTINEANT.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1126.04.24</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1126</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">344</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3990</bibl>
          <bibl type="Reference">(Mabillon, Ann., t. VI, app., p. 648; D. Bouq., t. XV,
                        p. 261; Mansi, t. XXI, p. 337.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Letter (papal)</classCode>
        <classCode scheme="Text_Type_Class">Letters</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Letters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:37:29 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:37:29 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <p>
          <s>
            <w lemma="[{'id':467732,'ls':[{'id':467733,'name':'Honorius','wfs':[{'id':467735,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Honorius'},{'id':241369551,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NP'},'name':'Honorius'},{'id':297475924,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Honorius'},{'id':241369548,'morph':{'Pos':'NP'},'name':'Honorius'}]}],'name':'Honorius@NP'}]">Honorius</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':5609409,'ls':[{'id':4722,'name':'gravo','wfs':[{'id':5610693,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'gravamur'}]}],'name':'gravo@V'}]">Gravamur</w>
            <c> </c>
            <w lemma="[{'id':718759,'ls':[{'id':718760,'name':'admodum','wfs':[{'id':718775,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'admodum'},{'id':718777,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'admodum'},{'id':718769,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'admodum'}]}],'name':'admodum@NN'}]">admodum</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w>
            <c>.</c>
            <c> </c>
            <note resp="TG">
              <s>
                <w>Being</w>
                <c> </c>
                <w lemma="[{'id':109433,'ls':[{'id':109434,'name':'a','wfs':[{'id':109435,'morph':{'Pos':'AP'},'name':'a'}]}],'name':'a@AP'}]">a</w>
                <c> </c>
                <w>letter</w>
                <c> </c>
                <w>this</w>
                <c> </c>
                <w>document</w>
                <c> </c>
                <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':532695117,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO'},'name':'has'},{'id':242816006,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'has'}]}],'name':'hic@PRO'}]">has</w>
                <c> </c>
                <w>been</w>
                <c> </c>
                <w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':245547189,'morph':{'Pos':'FM'},'name':'not'}]}],'name':'NON_LATIN@FM'}]">not</w>
                <c> </c>
                <w>included</w>
                <c> </c>
                <w>by</w>
                <c> </c>
                <w lemma="[{'id':241356359,'ls':[{'id':241356360,'name':'Bernard','wfs':[{'id':241356363,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NP'},'name':'Bernard'},{'id':241356361,'morph':{'Pos':'NP'},'name':'Bernard'}]}],'name':'Bernard@NP'}]">Bernard</w>
                <c> </c>
                <w>and</w>
                <c> </c>
                <w>Bruel</w>
                <c>,</c>
                <c> </c>
                <w>who</w>
                <c> </c>
                <w>erroneously</w>
                <c> </c>
                <w lemma="[{'id':7101054,'ls':[{'id':7806,'name':'refero','wfs':[{'id':524912,'morph':{'Mood':'IMPERATIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'refer'}]}],'name':'refero@V'}]">refer</w>
                <c> </c>
                <w>to</w>
                <c> </c>
                <w lemma="[{'id':1294819,'ls':[{'id':743418636,'name':'p','wfs':[{'id':743418639,'morph':{'Casus':'ACCUSATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418637,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418643,'morph':{'Casus':'DATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418642,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418641,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'}]}],'name':'puer@NN'}]">p</w>
                <c>.</c>
                <c> </c>
                <w>648</w>
                <c> </c>
                <w>instead</w>
                <c> </c>
                <w>of</w>
                <c> </c>
                <w lemma="[{'id':1294819,'ls':[{'id':743418636,'name':'p','wfs':[{'id':743418639,'morph':{'Casus':'ACCUSATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418637,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418643,'morph':{'Casus':'DATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418642,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418641,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'}]}],'name':'puer@NN'}]">p</w>
                <c>.</c>
                <c> </c>
                <w>600</w>
                <w lemma="[{'id':243116350,'ls':[{'id':243116351,'name':'NON_WORD','wfs':[{'id':245538849,'morph':{'Pos':'XY'},'name':'f'}]}],'name':'NON_WORD@XY'}]">f</w>
                <c>.</c>
                <c> </c>
                <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
                <c> </c>
                <w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510087,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'j'}]}],'name':'is@PRO'}]">J</w>
                <c>.</c>
                <c> </c>
                <w>Mabillon</w>
                <c>,</c>
                <c> </c>
                <w lemma="[{'id':758440,'ls':[{'id':808,'name':'annalis','wfs':[{'id':758450,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'annales'},{'id':758441,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'annales'},{'id':758449,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'annales'}]}],'name':'annalis@NN'}]">Annales</w>
                <c> </c>
                <w lemma="[{'id':1216283,'ls':[{'id':6689,'name':'ordo','wfs':[{'id':1216288,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'ordinis'}]}],'name':'ordo@NN'}]">Ordinis</w>
                <c> </c>
                <w lemma="[{'id':243116350,'ls':[{'id':243116351,'name':'NON_WORD','wfs':[{'id':244750562,'morph':{'Pos':'XY'},'name':'S'}]}],'name':'NON_WORD@XY'}]">S</w>
                <c>.</c>
                <c> </c>
                <w lemma="[{'id':324999,'ls':[{'id':325000,'name':'Benedictus','wfs':[{'id':325012,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Benedicti'}]}],'name':'Benedictus@NP'}]">Benedicti</w>
                <c> </c>
                <w lemma="[{'id':1205900,'ls':[{'id':1205901,'name':'occidentalia','wfs':[{'id':243408414,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'occidentalium'},{'id':1205902,'morph':{'Casus':'GENITIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'occidentalium'}]}],'name':'occidentalia@NN'}]">occidentalium</w>
                <c> </c>
                <w lemma="[{'id':1173036,'ls':[{'id':1173037,'name':'monachus','wfs':[{'id':1173041,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'monachorum'}]}],'name':'monachus@NN'}]">monachorum</w>
                <c> </c>
                <w lemma="[{'id':1229490,'ls':[{'id':1229491,'name':'patriarches','wfs':[{'id':1229498,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'patriarchae'},{'id':1229499,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'patriarchae'},{'id':1229504,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'patriarchae'},{'id':1229503,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'patriarchae'}]}],'name':'patriarches@NN'}]">patriarchae</w>
                <c>.</c>
              </s>
              <s>
                <w lemma="[{'id':243116350,'ls':[{'id':243116351,'name':'NON_WORD','wfs':[{'id':244750560,'morph':{'Pos':'XY'},'name':'T'}]}],'name':'NON_WORD@XY'}]">T</w>
                <c>.</c>
                <c> </c>
                <w>6</w>
                <c> </c>
                <w lemma="[{'id':244494563,'ls':[{'id':244494560,'name':'ProperName','wfs':[{'id':595342013,'morph':{'Pos':'NE'},'name':'lucca'}]}],'name':'ProperName@NE'}]">Lucca</w>
                <c> </c>
                <w>1745</w>
                <c>.</c>
                <c> </c>
                <c>(</c>
                <w>TG</w>
                <c>)</c>
                <c> </c>
              </s>
            </note>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
