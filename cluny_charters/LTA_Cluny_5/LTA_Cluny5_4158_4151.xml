<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 4151</title>
        <title type="sub">CHARTA DONATIONIS WIDONIS PASSE IN VILLA BLETERENS, ETC.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1150 ca.</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1150</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">511</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=4151</bibl>
          <bibl type="Reference">(B. s. 20.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Charter</classCode>
        <classCode scheme="Text_Type_Class">Legal</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:27:26 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:27:26 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <p>
          <s>
            <w lemma="[{'id':689274,'ls':[{'id':689275,'name':'Wido','wfs':[{'id':775467,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Wido'},{'id':241396033,'morph':{'Pos':'NP'},'name':'Wido'},{'id':488512058,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Wido'},{'id':241396037,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NP'},'name':'Wido'},{'id':689276,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Wido'}]}],'name':'Wido@NP'}]">Wido</w>
            <c> </c>
            <w lemma="[{'id':109792,'ls':[{'id':109793,'name':'autem','wfs':[{'id':109794,'morph':{'Pos':'CON'},'name':'autem'}]}],'name':'autem@CON'}]">autem</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':869904,'ls':[{'id':2018,'name':'cognomentum','wfs':[{'id':869929,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'cognomento'},{'id':869930,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'cognomento'}]}],'name':'cognomen@NN'}]">cognomento</w>
            <c> </c>
            <w lemma="[{'id':6534354,'ls':[{'id':6845,'name':'patior','wfs':[{'id':653276528,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'passe'},{'id':653276526,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'SINGULAR','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'passe'},{'id':653276527,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'SINGULAR','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'passe'}]}],'name':'patior@V'}]">Passe</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':244511016,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382851706,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382871756,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':594773492,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]">qui</w>
            <c> </c>
            <w lemma="[{'id':249793,'ls':[{'id':9033,'name':'una','wfs':[{'id':249794,'morph':{'Pos':'ADV'},'name':'una'}]}],'name':'una@ADV'}]">una</w>
            <c> </c>
            <w lemma="[{'id':109541,'ls':[{'id':109542,'name':'cum','wfs':[{'id':109543,'morph':{'Pos':'AP'},'name':'cum'}]}],'name':'cum@AP'}]">cum</w>
            <c> </c>
            <w lemma="[{'id':1159310,'ls':[{'id':5997,'name':'mater','wfs':[{'id':243406922,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'matre'}]}],'name':'mater@NN'}]">matre</w>
            <c> </c>
            <w lemma="[{'id':109722,'ls':[{'id':109723,'name':'ac','wfs':[{'id':109724,'morph':{'Pos':'CON'},'name':'ac'}]}],'name':'ac@CON'}]">ac</w>
            <c> </c>
            <w lemma="[{'id':1040499,'ls':[{'id':4492,'name':'frater','wfs':[{'id':290588,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'},{'id':241435784,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'}]}],'name':'frater@NN'}]">fratribus</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w lemma="[{'id':1406782,'ls':[{'id':488516594,'name':'suus','wfs':[{'id':488516607,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'suis'},{'id':488516608,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'suis'}]}],'name':'suus@NN'}]">suis</w>
            <c> </c>
            <w lemma="[{'id':5115044,'ls':[{'id':3709,'name':'dono','wfs':[{'id':5115401,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'donavit'}]}],'name':'dono@V'}]">donavit</w>
            <c> </c>
            <w lemma="[{'id':1322669,'ls':[{'id':964024,'name':'res','wfs':[{'id':242825947,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'rebus'},{'id':242825946,'morph':{'Casus':'DATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'rebus'}]}],'name':'res@NN'}]">rebus</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':241936645,'ls':[{'id':241936646,'name':'diversus','wfs':[{'id':243214846,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'diversis'},{'id':1070194,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'diversis'},{'id':243327323,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'diversis'},{'id':243359460,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'diversis'},{'id':1070199,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'diversis'},{'id':243214848,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'diversis'}]}],'name':'diversus@ADJ'}]">diversis</w>
            <c> </c>
            <w lemma="[{'id':1143697,'ls':[{'id':5796,'name':'locus','wfs':[{'id':644158959,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'locis'},{'id':644158958,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'locis'},{'id':1143703,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'locis'},{'id':1143702,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'locis'}]}],'name':'locus@NN'}]">locis</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':233002,'ls':[{'id':8162,'name':'scilicet','wfs':[{'id':241336076,'morph':{'Pos':'ADV'},'name':'scilicet'}]}],'name':'scilicet@ADV'}]">scilicet</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':1464168,'ls':[{'id':9261,'name':'villa','wfs':[{'id':241495492,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'villa'},{'id':1464234,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'villa'},{'id':1464229,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'villa'}]}],'name':'villa@NN'}]">villa</w>
            <c> </c>
            <w>Bleterens</w>
            <c> </c>
            <w lemma="[{'id':483627522,'ls':[{'id':483627539,'name':'curtilus','wfs':[{'id':483627547,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'curtilum'}]}],'name':'curtile@NN'}]">curtilum</w>
            <c> </c>
            <w lemma="[{'id':109541,'ls':[{'id':109542,'name':'cum','wfs':[{'id':109543,'morph':{'Pos':'AP'},'name':'cum'}]}],'name':'cum@AP'}]">cum</w>
            <c> </c>
            <w lemma="[{'id':1155992,'ls':[{'id':5966,'name':'mansio','wfs':[{'id':243390954,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'mansione'}]}],'name':'mansio@NN'}]">mansione</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w>Valoria</w>
            <c> </c>
            <w lemma="[{'id':256816,'ls':[{'id':256817,'name':'unus','wfs':[{'id':256855,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NUM'},'name':'unum'},{'id':256863,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NUM'},'name':'unum'},{'id':531794820,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NUM'},'name':'unum'},{'id':256861,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NUM'},'name':'unum'}]}],'name':'unus@NUM'}]">unum</w>
            <c> </c>
            <w lemma="[{'id':1156314,'ls':[{'id':1156315,'name':'mansus','wfs':[{'id':1156324,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'mansum'}]}],'name':'mansus@NN'}]">mansum</w>
            <c> </c>
            <w lemma="[{'id':241279615,'ls':[{'id':241279619,'name':'cum','wfs':[{'id':241279620,'morph':{'Pos':'CON'},'name':'cum'}]}],'name':'cum@CON'}]">cum</w>
            <c> </c>
            <w lemma="[{'id':6672007,'ls':[{'id':7026,'name':'pertineo','wfs':[{'id':244516414,'morph':{'Pos':'V'},'name':'pertinentiis'}]}],'name':'pertineo@V'}]">pertinentiis</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':595330133,'ls':[{'id':595330592,'name':'alius','wfs':[{'id':595330601,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'alio'}]}],'name':'alius@NN'}]">alio</w>
            <c> </c>
            <w lemma="[{'id':109792,'ls':[{'id':109793,'name':'autem','wfs':[{'id':109794,'morph':{'Pos':'CON'},'name':'autem'}]}],'name':'autem@CON'}]">autem</w>
            <c> </c>
            <w lemma="[{'id':1143697,'ls':[{'id':5796,'name':'locus','wfs':[{'id':1143711,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'loco'},{'id':1143710,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'loco'}]}],'name':'locus@NN'}]">loco</w>
            <c> </c>
            <w lemma="[{'id':7790314,'ls':[{'id':8800,'name':'terreo','wfs':[{'id':7790566,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'IMPERATIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'terre'}]}],'name':'terreo@V'}]">terre</w>
            <c> </c>
            <w lemma="[{'id':566352441,'ls':[{'id':566352442,'name':'jornalis','wfs':[{'id':570390400,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'jornalem'}]}],'name':'iornalis@NN'}]">jornalem</w>
            <c>;</c>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':242816248,'ls':[{'id':242816249,'name':'iste','wfs':[{'id':244510130,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'isto'},{'id':501623025,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'isto'}]}],'name':'iste@PRO'}]">isto</w>
            <c> </c>
            <w lemma="[{'id':252583,'ls':[{'id':9197,'name':'vero','wfs':[{'id':550000524,'morph':{'Pos':'ADV'},'name':'vero'}]}],'name':'vero@ADV'}]">vero</w>
            <c> </c>
            <w lemma="[{'id':566352441,'ls':[{'id':496033321,'name':'iornale','wfs':[{'id':594155523,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'jornale'},{'id':594155524,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'jornale'}]}],'name':'iornalis@NN'}]">jornale</w>
            <c> </c>
            <w lemma="[{'id':241347455,'ls':[{'id':241347456,'name':'quartus','wfs':[{'id':241347460,'morph':{'Pos':'ORD'},'name':'quartam'},{'id':3553717,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ORD'},'name':'quartam'}]}],'name':'quartus@ORD'}]">quartam</w>
            <c> </c>
            <w lemma="[{'id':236350,'ls':[{'id':8380,'name':'solummodo','wfs':[{'id':236351,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'solummodo'}]}],'name':'solummodo@ADV'}]">solummodo</w>
            <c> </c>
            <w lemma="[{'id':3384337,'ls':[{'id':3384338,'name':'accipio','wfs':[{'id':3384644,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'FUTURE','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'accipiet'}]}],'name':'accipio@V'}]">accipiet</w>
            <c> </c>
            <w lemma="[{'id':1049746,'ls':[{'id':1049747,'name':'garba','wfs':[{'id':1049755,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'garbam'}]}],'name':'garba@NN'}]">garbam</w>
            <c> </c>
            <w lemma="[{'id':1354946,'ls':[{'id':1354963,'name':'senior','wfs':[{'id':1354976,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'senior'},{'id':243459337,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'senior'}]}],'name':'senex@NN'},{'id':496028267,'ls':[{'id':496028268,'name':'senior','wfs':[{'id':496028281,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'senior'}]}],'name':'senior@NN'}]">senior</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':5109084,'ls':[{'id':3001,'name':'do','wfs':[{'id':5109405,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'dedit'}]}],'name':'do@V'}]">Dedit</w>
            <c> </c>
            <w lemma="[{'id':109834,'ls':[{'id':109835,'name':'etiam','wfs':[{'id':109836,'morph':{'Pos':'CON'},'name':'etiam'}]}],'name':'etiam@CON'}]">etiam</w>
            <c> </c>
            <w lemma="[{'id':1358723,'ls':[{'id':8286,'name':'servus','wfs':[{'id':1358781,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'servum'}]}],'name':'servus@NN'}]">servum</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1191767,'ls':[{'id':6409,'name':'nomen','wfs':[{'id':243402774,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'nomine'}]}],'name':'nomen@NN'}]">nomine</w>
            <c> </c>
            <w lemma="[{'id':608889,'ls':[{'id':608890,'name':'Stephanus','wfs':[{'id':608899,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Stephanum'}]}],'name':'Stephanus@NP'}]">Stephanum</w>
            <c>.</c>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
