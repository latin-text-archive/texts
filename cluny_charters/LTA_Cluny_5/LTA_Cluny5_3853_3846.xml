<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 3846</title>
        <title type="sub">EPISTOLA HENRICI IV, IMPERATORIS, AD HUGONEM, ABBATEM
                    CLUNIACENSEM, CUI GRATIAS AGIT DE RESTITUTA EJUS ORATIONIBUS SANITATE, ET IPSUM
                    ROGAT UT A SACRO FONTE FILIUM SUSCIPIAT.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">11</idno>
        <idno type="C1">11</idno>
        <idno type="C2">11</idno>
        <idno type="Time">1106 ca.</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1106</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">201</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3846</bibl>
          <bibl type="Reference">(D’Achery, Spic., éd. in-4º, t. II, p. 396; éd. in-fol.,
                        t. III, p. 443.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Letter</classCode>
        <classCode scheme="Text_Type_Class">Letters</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Letters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:45:26 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:45:27 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <p>
          <s>
            <w lemma="[{'id':241368622,'ls':[{'id':241368623,'name':'Henricus','wfs':[{'id':241368634,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Henricus'},{'id':241368624,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Henricus'}]}],'name':'Henricus@NP'}]">Henricus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':1467768,'ls':[{'id':1467787,'name':'visa','wfs':[{'id':1467790,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'visis'},{'id':1467789,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'visis'}]}],'name':'visa@NN'}]">Visis</w>
            <c> </c>
            <w lemma="[{'id':1340677,'ls':[{'id':8096,'name':'sanctitas','wfs':[{'id':1340689,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'sanctitatis'}]}],'name':'sanctitas@NN'}]">sanctitatis</w>
            <c> </c>
            <w lemma="[{'id':244512131,'ls':[{'id':244512132,'name':'tuus','wfs':[{'id':481109493,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'tuæ'},{'id':481109492,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'tuæ'},{'id':481109490,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'tuæ'}]}],'name':'tuus@PRO'}]">tuæ</w>
            <c> </c>
            <w lemma="[{'id':1142644,'ls':[{'id':5789,'name':'littera','wfs':[{'id':1142661,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'litteris'},{'id':1142662,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'litteris'}]}],'name':'littera@NN'}]">litteris</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w>
            <c>.</c>
            <c> </c>
            <note resp="TG">
              <s>
                <w lemma="[{'id':629068,'ls':[{'id':629069,'name':'Thia','wfs':[{'id':629072,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NP'},'name':'This'},{'id':629071,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NP'},'name':'This'}]}],'name':'Thia@NP'}]">This</w>
                <c> </c>
                <w>letter</w>
                <c> </c>
                <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':532695117,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO'},'name':'has'},{'id':242816006,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'has'}]}],'name':'hic@PRO'}]">has</w>
                <c> </c>
                <w>been</w>
                <c> </c>
                <w>left</w>
                <c> </c>
                <w>out</w>
                <c> </c>
                <w>by</w>
                <c> </c>
                <w lemma="[{'id':241356359,'ls':[{'id':241356360,'name':'Bernard','wfs':[{'id':241356363,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NP'},'name':'Bernard'},{'id':241356361,'morph':{'Pos':'NP'},'name':'Bernard'}]}],'name':'Bernard@NP'}]">Bernard</w>
                <c> </c>
                <w>and</w>
                <c> </c>
                <w>Bruel</w>
                <c>.</c>
                <c> </c>
                <c>(</c>
              </s>
              <s>
                <w>TG</w>
                <c>)</c>
              </s>
            </note>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
