<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 4148</title>
        <title type="sub">CHARTA QUA GALTERIUS, PRIOR DE DOMNA MARIA, CONCEDIT ECCLESIÆ DE
                    ESCUREI PRATUM, ETC.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1150 ca.</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1150</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">510</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=4148</bibl>
          <bibl type="Reference">(Bibl. nat. cop. 64-243.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Charter</classCode>
        <classCode scheme="Text_Type_Class">Legal</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:27:27 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:27:27 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <p>
          <s>
            <w lemma="[{'id':242815965,'ls':[{'id':242815966,'name':'ego','wfs':[{'id':244509626,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509632,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509629,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'}]}],'name':'ego@PRO'}]">Ego</w>
            <c> </c>
            <w lemma="[{'id':241366317,'ls':[{'id':241366318,'name':'Galterius','wfs':[{'id':241366323,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NP'},'name':'Galterius'},{'id':241366319,'morph':{'Pos':'NP'},'name':'Galterius'}]}],'name':'Galterius@NP'}]">Galterius</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1278789,'ls':[{'id':1278790,'name':'prior','wfs':[{'id':1278803,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'prior'},{'id':243431696,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'prior'}]}],'name':'prior@NN'}]">prior</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w lemma="[{'id':241362652,'ls':[{'id':241362653,'name':'Donna','wfs':[{'id':241362654,'morph':{'Pos':'NP'},'name':'Donna'},{'id':241362656,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NP'},'name':'Donna'}]}],'name':'Donna@NP'}]">Donna</w>
            <c> </c>
            <w lemma="[{'id':518783,'ls':[{'id':518784,'name':'Maria','wfs':[{'id':241376010,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NP'},'name':'Maria'},{'id':241376008,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NP'},'name':'Maria'},{'id':518795,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NP'},'name':'Maria'},{'id':518801,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NP'},'name':'Maria'},{'id':518800,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NP'},'name':'Maria'}]}],'name':'Maria@NP'}]">Maria</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1133332,'ls':[{'id':5649,'name':'laus','wfs':[{'id':243383169,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'laude'}]}],'name':'laus@NN'}]">laude</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':788875,'ls':[{'id':788878,'name':'assensus','wfs':[{'id':243261099,'morph':{'Casus':'ABLATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'assensu'}]}],'name':'assensus@NN'}]">assensu</w>
            <c> </c>
            <w lemma="[{'id':831740,'ls':[{'id':1588,'name':'capitulum','wfs':[{'id':831752,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'capituli'}]}],'name':'capitulum@NN'}]">capituli</w>
            <c> </c>
            <w lemma="[{'id':245551298,'ls':[{'id':245551299,'name':'nostri','wfs':[{'id':245551300,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nostri'},{'id':245551301,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nostri'}]}],'name':'nostri@NN'}]">nostri</w>
            <c> </c>
            <w lemma="[{'id':109772,'ls':[{'id':109773,'name':'atque','wfs':[{'id':109774,'morph':{'Pos':'CON'},'name':'atque'}]}],'name':'atque@CON'}]">atque</w>
            <c> </c>
            <w lemma="[{'id':1168901,'ls':[{'id':1168902,'name':'ministrale','wfs':[{'id':1168908,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'ministralium'}]}],'name':'ministrale@NN'}]">ministralium</w>
            <c> </c>
            <w lemma="[{'id':244510439,'ls':[{'id':244510440,'name':'noster','wfs':[{'id':251223001,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'nostrorum'},{'id':244510457,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'nostrorum'}]}],'name':'noster@PRO'}]">nostrorum</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':241539205,'ls':[{'id':241539211,'name':'Julianus','wfs':[{'id':243575767,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Juliani'},{'id':243575783,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Juliani'},{'id':241539217,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Juliani'},{'id':243575729,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Juliani'},{'id':243575731,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Juliani'},{'id':241539213,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Juliani'},{'id':241539215,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Juliani'}]}],'name':'Iulianus@ADJ'}]">Juliani</w>
            <c> </c>
            <w lemma="[{'id':1463978,'ls':[{'id':1464017,'name':'villicus','wfs':[{'id':1464030,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'villici'},{'id':1464023,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'villici'},{'id':1464018,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'villici'},{'id':1464024,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'villici'}]}],'name':'vilicus@NN'}]">villici</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':241372536,'ls':[{'id':241372537,'name':'Josbertus','wfs':[{'id':483609491,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Josberti'}]}],'name':'Josbertus@NP'}]">Josberti</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':830949,'ls':[{'id':830950,'name':'capellanus','wfs':[{'id':830956,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'capellani'},{'id':830957,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'capellani'},{'id':830951,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'capellani'},{'id':830963,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'capellani'}]}],'name':'capellanus@NN'}]">capellani</w>
            <c> </c>
            <w lemma="[{'id':245551298,'ls':[{'id':245551299,'name':'nostri','wfs':[{'id':245551300,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nostri'},{'id':245551301,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nostri'}]}],'name':'nostri@NN'}]">nostri</w>
            <c> </c>
            <w lemma="[{'id':558713,'ls':[{'id':558714,'name':'Petrus','wfs':[{'id':558720,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NP'},'name':'Petri'},{'id':689091,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Petri'},{'id':558721,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NP'},'name':'Petri'}]}],'name':'Petrus@NP'}]">Petri</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1965276,'ls':[{'id':2295,'name':'concessus','wfs':[{'id':1965317,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'concessi'},{'id':1965315,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'concessi'},{'id':1965362,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'concessi'},{'id':1965378,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'concessi'}]}],'name':'concessus@ADJ'}]">concessi</w>
            <c> </c>
            <w lemma="[{'id':958093,'ls':[{'id':958094,'name':'deus','wfs':[{'id':958106,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'deo'},{'id':958105,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'deo'}]}],'name':'deus@NN'}]">deo</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':1040499,'ls':[{'id':4492,'name':'frater','wfs':[{'id':290588,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'},{'id':241435784,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'}]}],'name':'frater@NN'}]">fratribus</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w>Escurei</w>
            <c> </c>
            <w lemma="[{'id':1277337,'ls':[{'id':7372,'name':'pratum','wfs':[{'id':1277350,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'pratum'},{'id':1277345,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'pratum'},{'id':1277352,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'pratum'}]}],'name':'pratum@NN'}]">pratum</w>
            <c> </c>
            <w lemma="[{'id':109541,'ls':[{'id':109542,'name':'cum','wfs':[{'id':109543,'morph':{'Pos':'AP'},'name':'cum'}]}],'name':'cum@AP'}]">cum</w>
            <c> </c>
            <w lemma="[{'id':4511478,'ls':[{'id':4511618,'name':'costo','wfs':[{'id':4511739,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'IMPERATIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'costa'}]}],'name':'consto@V'}]">costa</w>
            <c> </c>
            <w lemma="[{'id':1174057,'ls':[{'id':6203,'name':'mons','wfs':[{'id':1174070,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'montis'}]}],'name':'mons@NN'}]">montis</w>
            <c> </c>
            <w lemma="[{'id':241279545,'ls':[{'id':109594,'name':'juxta','wfs':[{'id':109595,'morph':{'Pos':'AP'},'name':'juxta'}]}],'name':'iuxta@AP'}]">juxta</w>
            <c> </c>
            <w lemma="[{'id':693270,'ls':[{'id':693271,'name':'abbatia','wfs':[{'id':693281,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'abbatiam'}]}],'name':'abbatia@NN'}]">abbatiam</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109722,'ls':[{'id':595016465,'name':'hac','wfs':[{'id':594156675,'morph':{'Pos':'CON'},'name':'hac'}]}],'name':'ac@CON'}]">hac</w>
            <c> </c>
            <w lemma="[{'id':741321149,'ls':[{'id':2380,'name':'conditio','wfs':[{'id':243296686,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'conditione'}]}],'name':'condicio@NN'}]">conditione</w>
            <c> </c>
            <w lemma="[{'id':110061,'ls':[{'id':110062,'name':'ut','wfs':[{'id':110063,'morph':{'Pos':'CON'},'name':'ut'}]}],'name':'ut@CON'}]">ut</w>
            <c> </c>
            <w lemma="[{'id':241293379,'ls':[{'id':241293380,'name':'annuatim','wfs':[{'id':241293382,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'annuatim'}]}],'name':'annuatim@ADV'}]">annuatim</w>
            <c> </c>
            <w lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':498533006,'morph':{'Pos':'NUM'},'name':'xii'}]}],'name':'NUMERAL@NUM'}]">xii</w>
            <c> </c>
            <w lemma="[{'id':1195507,'ls':[{'id':6458,'name':'nummus','wfs':[{'id':1195514,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nummi'},{'id':1195520,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'nummi'},{'id':1195513,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nummi'},{'id':1195508,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nummi'}]}],'name':'nummus@NN'}]">nummi</w>
            <c> </c>
            <w lemma="[{'id':241514930,'ls':[{'id':561924968,'name':'Cathalaunensis','wfs':[{'id':786705884,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cathalaunenses'},{'id':786705869,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cathalaunenses'},{'id':786705886,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cathalaunenses'},{'id':786705903,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cathalaunenses'},{'id':786705888,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cathalaunenses'},{'id':786705905,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cathalaunenses'}]}],'name':'Catalaunensis@ADJ'}]">Cathalaunenses</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109453,'ls':[{'id':109454,'name':'ad','wfs':[{'id':109455,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'ad@AP'},{'id':109433,'ls':[{'id':607803638,'name':'ad','wfs':[{'id':607803639,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'a@AP'}]">ad</w>
            <c> </c>
            <w lemma="[{'id':1032381,'ls':[{'id':1032382,'name':'festus','wfs':[{'id':1032391,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'festum'}]}],'name':'festus@NN'}]">festum</w>
            <c> </c>
            <w lemma="[{'id':3010352,'ls':[{'id':8092,'name':'sanctus','wfs':[{'id':3010397,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'sancti'},{'id':3010481,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'sancti'},{'id':3010400,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'sancti'},{'id':3010458,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'sancti'}]}],'name':'sanctus@ADJ'}]">sancti</w>
            <c> </c>
            <w lemma="[{'id':482230,'ls':[{'id':488454214,'name':'Johannes','wfs':[{'id':488454218,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Johannis'}]}],'name':'Ioannes@NP'}]">Johannis</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':498533007,'morph':{'Pos':'NUM'},'name':'viii'}]}],'name':'NUMERAL@NUM'}]">viii</w>
            <c> </c>
            <w lemma="[{'id':964023,'ls':[{'id':242825925,'name':'dies','wfs':[{'id':594734982,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'dies'},{'id':242825933,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'COMMON','Numerus':'PLURAL','Pos':'NN'},'name':'dies'},{'id':594735014,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'dies'},{'id':594734999,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'dies'},{'id':242825932,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'COMMON','Numerus':'PLURAL','Pos':'NN'},'name':'dies'},{'id':594734996,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'dies'},{'id':594735022,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'dies'},{'id':242825940,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'COMMON','Numerus':'PLURAL','Pos':'NN'},'name':'dies'},{'id':242825927,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'COMMON','Numerus':'SINGULAR','Pos':'NN'},'name':'dies'},{'id':242825926,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'COMMON','Numerus':'SINGULAR','Pos':'NN'},'name':'dies'},{'id':594735005,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'dies'}]}],'name':'dies@NN'}]">dies</w>
            <c> </c>
            <w lemma="[{'id':109480,'ls':[{'id':109481,'name':'ante','wfs':[{'id':109482,'morph':{'Pos':'AP'},'name':'ante'}]}],'name':'ante@AP'}]">ante</w>
            <c> </c>
            <w lemma="[{'id':110077,'ls':[{'id':110078,'name':'vel','wfs':[{'id':110080,'morph':{'Pos':'CON'},'name':'vel'}]}],'name':'vel@CON'}]">vel</w>
            <c> </c>
            <w lemma="[{'id':1279011,'ls':[{'id':1279012,'name':'prius','wfs':[{'id':243431744,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'prius'},{'id':1279025,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'prius'},{'id':243431740,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'prius'}]}],'name':'prius@NN'}]">prius</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':7086882,'ls':[{'id':7788,'name':'reddo','wfs':[{'id':7087164,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'reddantur'}]}],'name':'reddo@V'}]">reddantur</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">Et</w>
            <c> </c>
            <w lemma="[{'id':109868,'ls':[{'id':109869,'name':'ne','wfs':[{'id':109870,'morph':{'Pos':'CON'},'name':'ne'}]}],'name':'ne@CON'}]">ne</w>
            <c> </c>
            <w lemma="[{'id':119570,'ls':[{'id':119571,'name':'aliqua','wfs':[{'id':119572,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'aliqua'}]}],'name':'aliqua@ADV'}]">aliqua</w>
            <c> </c>
            <w lemma="[{'id':109669,'ls':[{'id':109670,'name':'super','wfs':[{'id':109671,'morph':{'Pos':'AP'},'name':'super'}]}],'name':'super@AP'}]">super</w>
            <c> </c>
            <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':244509722,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187641,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187642,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':242816022,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'}]}],'name':'hic@PRO'}]">hoc</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':2851447,'ls':[{'id':7236,'name':'posterus','wfs':[{'id':2851579,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'posterum'},{'id':2851563,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'posterum'},{'id':2851584,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'posterum'},{'id':2851541,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'posterum'}]}],'name':'posterus@ADJ'}]">posterum</w>
            <c> </c>
            <w lemma="[{'id':826990,'ls':[{'id':827005,'name':'calumpnia','wfs':[{'id':827016,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'calumpnia'},{'id':827012,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'calumpnia'},{'id':827017,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'calumpnia'}]}],'name':'calumnia@NN'}]">calumpnia</w>
            <c> </c>
            <w lemma="[{'id':6502069,'ls':[{'id':244530730,'name':'orior','wfs':[{'id':6502349,'morph':{'ConjugationType':'FOURTH_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'oriatur'}]}],'name':'orior@V'}]">oriatur</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':5511842,'ls':[{'id':4349,'name':'fio','wfs':[{'id':244466101,'morph':{'Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'PASSIVE'},'name':'fieri'},{'id':442877,'morph':{'Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'fieri'}]}],'name':'fio@V'}]">fieri</w>
            <c> </c>
            <w lemma="[{'id':188837,'ls':[{'id':188838,'name':'inde','wfs':[{'id':188841,'morph':{'Pos':'ADV'},'name':'inde'}]}],'name':'inde@ADV'}]">inde</w>
            <c> </c>
            <w lemma="[{'id':850286,'ls':[{'id':241412883,'name':'cyrographum','wfs':[{'id':241412886,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NN'},'name':'cyrographum'},{'id':241412884,'morph':{'Pos':'NN'},'name':'cyrographum'}]}],'name':'chirographum@NN'}]">cyrographum</w>
            <c> </c>
            <w lemma="[{'id':8038609,'ls':[{'id':9327,'name':'volo','wfs':[{'id':243116354,'morph':{'Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'volumus'}]}],'name':'volo@V'}]">volumus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':244510900,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'cujus'},{'id':457578056,'morph':{'Casus':'GENITIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'cujus'},{'id':457578057,'morph':{'Casus':'GENITIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'cujus'}]}],'name':'qui@PRO'}]">cujus</w>
            <c> </c>
            <w lemma="[{'id':1227224,'ls':[{'id':6813,'name':'pars','wfs':[{'id':243414707,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'partem'}]}],'name':'pars@NN'}]">partem</w>
            <c> </c>
            <w lemma="[{'id':985146,'ls':[{'id':3766,'name':'ecclesia','wfs':[{'id':985189,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesia'},{'id':985188,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesia'},{'id':985184,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesia'}]}],'name':'ecclesia@NN'}]">ecclesia</w>
            <c> </c>
            <w lemma="[{'id':245551289,'ls':[{'id':245551290,'name':'nostra','wfs':[{'id':245551291,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'nostra'},{'id':245551292,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'nostra'},{'id':245551297,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'nostra'}]}],'name':'nostra@NN'}]">nostra</w>
            <c> </c>
            <w lemma="[{'id':5618354,'ls':[{'id':4757,'name':'habeo','wfs':[{'id':5618707,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'habeat'}]}],'name':'habeo@V'}]">habeat</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1594858,'ls':[{'id':1594859,'name':'alter','wfs':[{'id':1594918,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'alteram'}]}],'name':'alter@ADJ'}]">alteram</w>
            <c> </c>
            <w lemma="[{'id':242816114,'ls':[{'id':242816115,'name':'ille','wfs':[{'id':244509856,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illa'},{'id':244509849,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illa'},{'id':244509870,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illa'},{'id':242816118,'morph':{'Casus':'ABLATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illa'}]}],'name':'ille@PRO'}]">illa</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w>Escureio</w>
            <c>.</c>
            <note>
              <s>
                <c>(</c>
                <w lemma="[{'id':630670,'ls':[{'id':630695,'name':'Trax','wfs':[{'id':243224825,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NE'},'name':'Trace'}]}],'name':'Thrax@NE'}]">Trace</w>
                <c> </c>
                <w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':496021821,'morph':{'Pos':'FM'},'name':'du'}]}],'name':'NON_LATIN@FM'}]">du</w>
                <c> </c>
                <w>cyrographe</w>
                <c>.</c>
                <c>)</c>
              </s>
            </note>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
