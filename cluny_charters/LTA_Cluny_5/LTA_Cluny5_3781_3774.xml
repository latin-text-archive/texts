<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 3774</title>
        <title type="sub">CHARTA QUA NOTUM EST RAIMUNDUM COMITEM VENTAS OMNIUM RERUM QUÆ IN
                    DUABUS DOMIBUS SALINIS VENDUNTUR DEDISSE.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1100 ca.</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1100</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">125</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3774</bibl>
          <bibl type="Reference">(B. s. 9.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Charter</classCode>
        <classCode scheme="Text_Type_Class">Legal</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:49:04 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:49:04 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <p>
          <s>
            <w lemma="[{'id':1192851,'ls':[{'id':241458491,'name':'noticia','wfs':[{'id':597980760,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'noticie'},{'id':241458494,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'noticie'},{'id':597980759,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'noticie'}]}],'name':'notitia@NN'}]">Noticie</w>
            <c> </c>
            <w lemma="[{'id':1048464,'ls':[{'id':1048465,'name':'futurus','wfs':[{'id':1048469,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'futurorum'}]}],'name':'futurus@NN'}]">futurorum</w>
            <c> </c>
            <w lemma="[{'id':4269480,'ls':[{'id':2135,'name':'commendo','wfs':[{'id':4269733,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'IMPERATIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'commendare'},{'id':4269561,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'commendare'},{'id':496795031,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Mood':'GERUND','Numerus':'SINGULAR','Pos':'V'},'name':'commendare'}]}],'name':'commendo@V'}]">commendare</w>
            <c> </c>
            <w lemma="[{'id':8038609,'ls':[{'id':9327,'name':'volo','wfs':[{'id':243116354,'morph':{'Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'volumus'}]}],'name':'volo@V'}]">volumus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109986,'ls':[{'id':109987,'name':'quod','wfs':[{'id':109988,'morph':{'Pos':'CON'},'name':'quod'}]}],'name':'quod@CON'}]">quod</w>
            <c> </c>
            <w lemma="[{'id':241384710,'ls':[{'id':241384711,'name':'Raimundus','wfs':[{'id':241384717,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NP'},'name':'Raimundus'},{'id':241384712,'morph':{'Pos':'NP'},'name':'Raimundus'}]}],'name':'Raimundus@NP'}]">Raimundus</w>
            <c> </c>
            <w lemma="[{'id':877368,'ls':[{'id':2109,'name':'comes','wfs':[{'id':243290436,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'comes'},{'id':243290437,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'comes'}]}],'name':'comes@NN'}]">comes</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1040499,'ls':[{'id':4492,'name':'frater','wfs':[{'id':1040511,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'frater'},{'id':243349942,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'frater'}]}],'name':'frater@NN'}]">frater</w>
            <c> </c>
            <w lemma="[{'id':4307140,'ls':[{'id':244530658,'name':'como','wfs':[{'id':4307444,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'comitis'}]}],'name':'como@V'}]">comitis</w>
            <c> </c>
            <w lemma="[{'id':608889,'ls':[{'id':608890,'name':'Stephanus','wfs':[{'id':608897,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NP'},'name':'Stephani'},{'id':608896,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NP'},'name':'Stephani'},{'id':608891,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NP'},'name':'Stephani'},{'id':608903,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Stephani'}]}],'name':'Stephanus@NP'}]">Stephani</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':5115044,'ls':[{'id':3709,'name':'dono','wfs':[{'id':5115401,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'donavit'}]}],'name':'dono@V'}]">donavit</w>
            <c> </c>
            <w lemma="[{'id':958093,'ls':[{'id':958094,'name':'deus','wfs':[{'id':958106,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'deo'},{'id':958105,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'deo'}]}],'name':'deus@NN'}]">deo</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':1340758,'ls':[{'id':1340759,'name':'sanctus','wfs':[{'id':1340761,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'sanctis'},{'id':1340762,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'sanctis'}]}],'name':'sanctus@NN'}]">sanctis</w>
            <c> </c>
            <w lemma="[{'id':767705,'ls':[{'id':244741368,'name':'apostolus','wfs':[{'id':767707,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'apostolis'},{'id':767708,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'apostolis'}]}],'name':'apostolus@NN'}]">apostolis</w>
            <c> </c>
            <w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510032,'morph':{'Casus':'GENITIVE','Genus':'COMMON','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ejus'}]}],'name':'is@PRO'}]">ejus</w>
            <c> </c>
            <w lemma="[{'id':558713,'ls':[{'id':558714,'name':'Petrus','wfs':[{'id':558725,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Petro'},{'id':558726,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Petro'}]}],'name':'Petrus@NP'}]">Petro</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':555507,'ls':[{'id':555508,'name':'Paulus','wfs':[{'id':688243,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Paulo'},{'id':688244,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Paulo'}]}],'name':'Paulus@NP'}]">Paulo</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':1173121,'ls':[{'id':6194,'name':'monasterium','wfs':[{'id':243396633,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'monasterio'},{'id':243396632,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'monasterio'}]}],'name':'monasterium@NN'}]">monasterio</w>
            <c> </c>
            <w lemma="[{'id':1481107,'ls':[{'id':241518242,'name':'Cluniensis','wfs':[{'id':241518285,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniensi'},{'id':241518279,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniensi'},{'id':241518286,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniensi'},{'id':241518273,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniensi'},{'id':241518280,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniensi'},{'id':241518274,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniensi'}]}],'name':'Cluniacensis@ADJ'}]">Cluniensi</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':242816365,'morph':{'Casus':'DATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'cui'},{'id':244990008,'morph':{'Casus':'DATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'cui'},{'id':244990009,'morph':{'Casus':'DATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'cui'}]}],'name':'qui@PRO'}]">cui</w>
            <c> </c>
            <w lemma="[{'id':979873,'ls':[{'id':979891,'name':'domnus','wfs':[{'id':979906,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'domnus'}]}],'name':'dominus@NN'}]">domnus</w>
            <c> </c>
            <w lemma="[{'id':693208,'ls':[{'id':693227,'name':'abbas','wfs':[{'id':693246,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'abbas'}]}],'name':'abbas@NN'}]">abbas</w>
            <c> </c>
            <w lemma="[{'id':473020,'ls':[{'id':473021,'name':'Hugo','wfs':[{'id':488451312,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Hugo'},{'id':241369646,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Hugo'}]}],'name':'Hugo@NP'}]">Hugo</w>
            <c> </c>
            <w lemma="[{'id':6849764,'ls':[{'id':244517401,'name':'presum','wfs':[{'id':244517403,'morph':{'Pos':'V'},'name':'preerat'}]}],'name':'praesum@V'}]">preerat</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':7964639,'ls':[{'id':9166,'name':'venio','wfs':[{'id':7965080,'morph':{'Casus':'ACCUSATIVE','ConjugationType':'FOURTH_CONJUGATION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PERFECT','VerbType':'INTRANSITIVE','Voice':'PASSIVE'},'name':'ventas'}]}],'name':'venio@V'}]">ventas</w>
            <c> </c>
            <w lemma="[{'id':2744416,'ls':[{'id':6625,'name':'omnis','wfs':[{'id':2744437,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnium'},{'id':2744430,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnium'},{'id':2744422,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnium'}]}],'name':'omnis@ADJ'}]">omnium</w>
            <c> </c>
            <w lemma="[{'id':1322669,'ls':[{'id':964024,'name':'res','wfs':[{'id':242825945,'morph':{'Casus':'GENITIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'rerum'}]}],'name':'res@NN'}]">rerum</w>
            <c> </c>
            <w lemma="[{'id':109962,'ls':[{'id':109963,'name':'que','wfs':[{'id':109964,'morph':{'Pos':'CON'},'name':'que'}]}],'name':'que@CON'}]">que</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':241346710,'ls':[{'id':241346711,'name':'duo','wfs':[{'id':241346714,'morph':{'Casus':'ABLATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NUM'},'name':'duabus'},{'id':743447635,'morph':{'Casus':'DATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NUM'},'name':'duabus'}]}],'name':'duo@NUM'}]">duabus</w>
            <c> </c>
            <w lemma="[{'id':980177,'ls':[{'id':3693,'name':'domus','wfs':[{'id':241428330,'morph':{'Casus':'DATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'domibus'},{'id':297496588,'morph':{'Casus':'ABLATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'domibus'}]}],'name':'domus@NN'}]">domibus</w>
            <c> </c>
            <w lemma="[{'id':1338699,'ls':[{'id':1338700,'name':'salina','wfs':[{'id':1338702,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'salinis'},{'id':1338703,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'salinis'}]}],'name':'salina@NN'}]">Salinis</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':233002,'ls':[{'id':8162,'name':'scilicet','wfs':[{'id':241336076,'morph':{'Pos':'ADV'},'name':'scilicet'}]}],'name':'scilicet@ADV'}]">scilicet</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':980177,'ls':[{'id':3693,'name':'domus','wfs':[{'id':243328052,'morph':{'Casus':'ABLATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'domo'},{'id':243328053,'morph':{'Casus':'DATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'domo'}]}],'name':'domus@NN'}]">domo</w>
            <c> </c>
            <w lemma="[{'id':393122,'ls':[{'id':393123,'name':'Ermenfredus','wfs':[{'id':393124,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Ermenfredi'},{'id':393135,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Ermenfredi'},{'id':393129,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Ermenfredi'},{'id':623479,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Ermenfredi'},{'id':393130,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Ermenfredi'}]}],'name':'Ermenfredus@NP'}]">Ermenfredi</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':980177,'ls':[{'id':3693,'name':'domus','wfs':[{'id':243328052,'morph':{'Casus':'ABLATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'domo'},{'id':243328053,'morph':{'Casus':'DATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'domo'}]}],'name':'domus@NN'}]">domo</w>
            <c> </c>
            <w>Bonodi</w>
            <c> </c>
            <w lemma="[{'id':7961542,'ls':[{'id':9158,'name':'vendo','wfs':[{'id':7962226,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'venduntur'}]}],'name':'vendo@V'}]">venduntur</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109622,'ls':[{'id':109623,'name':'praeter','wfs':[{'id':241279567,'morph':{'Pos':'AP'},'name':'preter'}]}],'name':'praeter@AP'}]">preter</w>
            <c> </c>
            <w lemma="[{'id':242776349,'ls':[{'id':242776462,'name':'ventus','wfs':[{'id':242776465,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'ventas'}]}],'name':'ventus@ADJ'}]">ventas</w>
            <c> </c>
            <w lemma="[{'id':1195742,'ls':[{'id':1195743,'name':'nundina','wfs':[{'id':1195747,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'nundinarum'}]}],'name':'nundina@NN'}]">nundinarum</w>
            <c> </c>
            <w lemma="[{'id':592238,'ls':[{'id':592239,'name':'Sanctus','wfs':[{'id':488483804,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancti'},{'id':592240,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Sancti'},{'id':592245,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Sancti'},{'id':592252,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancti'},{'id':592246,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Sancti'}]}],'name':'Sanctus@NP'}]">Sancti</w>
            <c> </c>
            <w lemma="[{'id':293687,'ls':[{'id':293688,'name':'Andreus','wfs':[{'id':293705,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Andree'},{'id':488395978,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Andree'}]}],'name':'Andreus@NP'}]">Andree</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':709997,'ls':[{'id':174,'name':'actus','wfs':[{'id':244531470,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'actum'}]}],'name':'actus@NN'}]">Actum</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':836489,'ls':[{'id':1662,'name':'castellum','wfs':[{'id':836500,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'castello'},{'id':836499,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'castello'}]}],'name':'castellum@NN'}]">castello</w>
            <c> </c>
            <w>Argentiaco</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109538,'ls':[{'id':109539,'name':'coram','wfs':[{'id':109540,'morph':{'Pos':'AP'},'name':'coram'}]}],'name':'coram@AP'}]">coram</w>
            <c> </c>
            <w lemma="[{'id':979873,'ls':[{'id':979891,'name':'domnus','wfs':[{'id':979902,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'domno'},{'id':979903,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'domno'}]}],'name':'dominus@NN'}]">domno</w>
            <c> </c>
            <w lemma="[{'id':608889,'ls':[{'id':608890,'name':'Stephanus','wfs':[{'id':608901,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Stephano'},{'id':608902,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Stephano'}]}],'name':'Stephanus@NP'}]">Stephano</w>
            <c> </c>
            <w>Joret</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':578851,'ls':[{'id':578852,'name':'Richardus','wfs':[{'id':702030,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Richardo'},{'id':702031,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Richardo'},{'id':578862,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Richardo'},{'id':578863,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Richardo'}]}],'name':'Richardus@NP'}]">Richardo</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w>Narduino</w>
            <c> </c>
            <w lemma="[{'id':1040499,'ls':[{'id':4492,'name':'frater','wfs':[{'id':290588,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'},{'id':241435784,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'}]}],'name':'frater@NN'}]">fratribus</w>
            <c> </c>
            <w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510032,'morph':{'Casus':'GENITIVE','Genus':'COMMON','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ejus'}]}],'name':'is@PRO'}]">ejus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':109538,'ls':[{'id':109539,'name':'coram','wfs':[{'id':109540,'morph':{'Pos':'AP'},'name':'coram'}]}],'name':'coram@AP'}]">coram</w>
            <c> </c>
            <w lemma="[{'id':241384721,'ls':[{'id':241384722,'name':'Rainaldus','wfs':[{'id':241384726,'morph':{'Pos':'NP'},'name':'rainaldo'}]}],'name':'Rainaldus@NP'}]">Rainaldo</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w>Argentiaco</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':109538,'ls':[{'id':109539,'name':'coram','wfs':[{'id':109540,'morph':{'Pos':'AP'},'name':'coram'}]}],'name':'coram@AP'}]">coram</w>
            <c> </c>
            <w lemma="[{'id':279567,'ls':[{'id':279568,'name':'Ainardus','wfs':[{'id':488391244,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Ainardo'},{'id':279580,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Ainardo'},{'id':488391245,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Ainardo'},{'id':279579,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Ainardo'}]}],'name':'Ainardus@NP'}]">Ainardo</w>
            <c> </c>
            <w>Borel</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':241367693,'ls':[{'id':241367694,'name':'Guido','wfs':[{'id':488440516,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Guidone'}]}],'name':'Guido@NP'}]">Guidone</w>
            <c> </c>
            <w>Profund</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':244511016,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382851706,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382871756,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':594773492,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]">qui</w>
            <c> </c>
            <w lemma="[{'id':2744416,'ls':[{'id':6625,'name':'omnis','wfs':[{'id':2744425,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnes'},{'id':2744424,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnes'},{'id':740473043,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'omnes'},{'id':2744431,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnes'},{'id':2744417,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnes'},{'id':2744432,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnes'},{'id':2744423,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'omnes'}]}],'name':'omnis@ADJ'}]">omnes</w>
            <c> </c>
            <w lemma="[{'id':1415950,'ls':[{'id':1415951,'name':'testis','wfs':[{'id':1415959,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415969,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':498535603,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415968,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415952,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415961,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'},{'id':1415960,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'testes'}]}],'name':'testis@NN'}]">testes</w>
            <c> </c>
            <w lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':242825914,'morph':{'Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'sunt'}]}],'name':'sum@V'}]">sunt</w>
            <c> </c>
            <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':242816036,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hujus'},{'id':244509685,'morph':{'Casus':'GENITIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hujus'},{'id':36189655,'morph':{'Casus':'GENITIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hujus'}]}],'name':'hic@PRO'}]">hujus</w>
            <c> </c>
            <w lemma="[{'id':990534,'ls':[{'id':505851544,'name':'elemosina','wfs':[{'id':482022858,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'elemosine'},{'id':482022857,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'elemosine'},{'id':482022856,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'elemosine'}]}],'name':'eleemosyna@NN'}]">elemosine</w>
            <c>.</c>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
