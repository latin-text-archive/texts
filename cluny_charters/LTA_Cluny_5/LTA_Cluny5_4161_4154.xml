<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 4154</title>
        <title type="sub">BERNARDUS CONSTANTINUS RELIQUIT MONACHIS CLUNIACENSIBUS MILLE
                    SOLIDOS IN CLAUSO AMALIACO.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1150 ca.</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1150</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">513</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=4154</bibl>
          <bibl type="Reference">(B. s. 24.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Charter</classCode>
        <classCode scheme="Text_Type_Class">Legal</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:27:24 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:27:24 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <pb n="513"/>
        <p>
          <s>
            <w lemma="[{'id':327353,'ls':[{'id':327354,'name':'Bernardus','wfs':[{'id':327368,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Bernardus'},{'id':593380,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Bernardus'}]}],'name':'Bernardus@NP'}]">Bernardus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':869904,'ls':[{'id':2018,'name':'cognomentum','wfs':[{'id':869929,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'cognomento'},{'id':869930,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'cognomento'}]}],'name':'cognomen@NN'}]">cognomento</w>
            <c> </c>
            <w lemma="[{'id':358959,'ls':[{'id':358960,'name':'Constantinus','wfs':[{'id':358975,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Constantinus'}]}],'name':'Constantinus@NP'}]">Constantinus</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':7131443,'ls':[{'id':7131444,'name':'relinquo','wfs':[{'id':7131771,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'reliquit'}]}],'name':'relinquo@V'}]">reliquit</w>
            <c> </c>
            <w lemma="[{'id':255539,'ls':[{'id':255540,'name':'mille','wfs':[{'id':255585,'morph':{'Casus':'INDECLINABLE','Pos':'NUM'},'name':'mille'}]}],'name':'mille@NUM'}]">mille</w>
            <c> </c>
            <w lemma="[{'id':1365593,'ls':[{'id':1365594,'name':'solidus','wfs':[{'id':243462906,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'solidos'}]}],'name':'solidus@NN'}]">solidos</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':863167,'ls':[{'id':863168,'name':'clausus','wfs':[{'id':863180,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'clauso'},{'id':863179,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'clauso'}]}],'name':'clausus@NN'}]">clauso</w>
            <c> </c>
            <w>Amaliaco</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':1444118,'ls':[{'id':9084,'name':'uxor','wfs':[{'id':1444132,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'uxor'},{'id':243491696,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'uxor'}]}],'name':'uxor@NN'}]">uxor</w>
            <c> </c>
            <w lemma="[{'id':244511757,'ls':[{'id':244511758,'name':'suus','wfs':[{'id':244511827,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'sua'},{'id':244511838,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'sua'},{'id':252932518,'morph':{'Casus':'ABLATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'sua'},{'id':245550215,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'sua'}]}],'name':'suus@PRO'}]">sua</w>
            <c> </c>
            <w lemma="[{'id':5475468,'ls':[{'id':4230,'name':'facio','wfs':[{'id':5475778,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'FUTURE','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'faciet'}]}],'name':'facio@V'}]">faciet</w>
            <c> </c>
            <w lemma="[{'id':242816114,'ls':[{'id':242816115,'name':'ille','wfs':[{'id':242816141,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illud'},{'id':244509871,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illud'}]}],'name':'ille@PRO'}]">illud</w>
            <c> </c>
            <w lemma="[{'id':109453,'ls':[{'id':109454,'name':'ad','wfs':[{'id':109455,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'ad@AP'},{'id':109433,'ls':[{'id':607803638,'name':'ad','wfs':[{'id':607803639,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'a@AP'}]">ad</w>
            <c> </c>
            <w lemma="[{'id':1161055,'ls':[{'id':1161056,'name':'medietas','wfs':[{'id':243392635,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'medietatem'}]}],'name':'medietas@NN'}]">medietatem</w>
            <c>;</c>
            <c> </c>
            <w lemma="[{'id':244511757,'ls':[{'id':244511758,'name':'suus','wfs':[{'id':244511777,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'suamque'}]}],'name':'suus@PRO'}]">suamque</w>
            <c> </c>
            <w lemma="[{'id':1227224,'ls':[{'id':6813,'name':'pars','wfs':[{'id':243414707,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'partem'}]}],'name':'pars@NN'}]">partem</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w lemma="[{'id':1465875,'ls':[{'id':9263,'name':'vinum','wfs':[{'id':1465918,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'vino'},{'id':1465919,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'vino'}]}],'name':'vinum@NN'}]">vino</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109986,'ls':[{'id':109987,'name':'quod','wfs':[{'id':109988,'morph':{'Pos':'CON'},'name':'quod'}]}],'name':'quod@CON'}]">quod</w>
            <c> </c>
            <w lemma="[{'id':5355997,'ls':[{'id':4065,'name':'exeo','wfs':[{'id':434708,'morph':{'Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'exierit'},{'id':5356146,'morph':{'Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'exierit'},{'id':5356141,'morph':{'Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'exierit'},{'id':434713,'morph':{'Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'exierit'}]}],'name':'exeo@V'}]">exierit</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w lemma="[{'id':242816114,'ls':[{'id':242816115,'name':'ille','wfs':[{'id':296170475,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illo'},{'id':244509816,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illo'}]}],'name':'ille@PRO'}]">illo</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':5109084,'ls':[{'id':3001,'name':'do','wfs':[{'id':5109376,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'FUTURE','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'dabit'}]}],'name':'do@V'}]">dabit</w>
            <c> </c>
            <w lemma="[{'id':242816286,'ls':[{'id':242816287,'name':'nos','wfs':[{'id':242816288,'morph':{'Casus':'DATIVE','Genus':'COMMON','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'nobis'},{'id':594760049,'morph':{'Casus':'DATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'nobis'},{'id':555363025,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'nobis'},{'id':273666740,'morph':{'Casus':'ABLATIVE','Genus':'COMMON','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'nobis'}]}],'name':'nos@PRO'}]">nobis</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':1312049,'ls':[{'id':7790,'name':'redemptio','wfs':[{'id':243444458,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'redemptionem'}]}],'name':'redemptio@NN'}]">redemptionem</w>
            <c> </c>
            <w lemma="[{'id':255539,'ls':[{'id':255540,'name':'mille','wfs':[{'id':255585,'morph':{'Casus':'INDECLINABLE','Pos':'NUM'},'name':'mille'}]}],'name':'mille@NUM'}]">mille</w>
            <c> </c>
            <w lemma="[{'id':1365593,'ls':[{'id':1365594,'name':'solidus','wfs':[{'id':1365598,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'solidorum'}]}],'name':'solidus@NN'}]">solidorum</w>
            <c> </c>
            <w lemma="[{'id':242816852,'ls':[{'id':242816858,'name':'unusquisque','wfs':[{'id':501623062,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO'},'name':'unoquoque'},{'id':244512267,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'INDEFINITE'},'name':'unoquoque'}]}],'name':'unusquisque@PRO'}]">unoquoque</w>
            <c> </c>
            <w lemma="[{'id':759853,'ls':[{'id':807,'name':'annus','wfs':[{'id':759865,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'anno'},{'id':759864,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'anno'}]}],'name':'annus@NN'}]">anno</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':109766,'ls':[{'id':109767,'name':'at','wfs':[{'id':109768,'morph':{'Pos':'CON'},'name':'at'}]}],'name':'at@CON'}]">At</w>
            <c> </c>
            <w lemma="[{'id':252583,'ls':[{'id':9197,'name':'vero','wfs':[{'id':550000524,'morph':{'Pos':'ADV'},'name':'vero'}]}],'name':'vero@ADV'}]">vero</w>
            <c> </c>
            <w lemma="[{'id':110020,'ls':[{'id':110021,'name':'si','wfs':[{'id':110022,'morph':{'Pos':'CON'},'name':'si'}]}],'name':'si@CON'}]">si</w>
            <c> </c>
            <w lemma="[{'id':242816153,'ls':[{'id':242816154,'name':'ipse','wfs':[{'id':244509982,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ipsa'},{'id':244510007,'morph':{'Casus':'ABLATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ipsa'},{'id':242816156,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ipsa'},{'id':244509988,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ipsa'}]}],'name':'ipse@PRO'}]">ipsa</w>
            <c> </c>
            <w lemma="[{'id':131082,'ls':[{'id':1339,'name':'bene','wfs':[{'id':131085,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'bene'}]}],'name':'bene@ADV'}]">bene</w>
            <c> </c>
            <w lemma="[{'id':5475468,'ls':[{'id':4230,'name':'facio','wfs':[{'id':5475814,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'fecerit'},{'id':5475820,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'fecerit'}]}],'name':'facio@V'}]">fecerit</w>
            <c> </c>
            <w lemma="[{'id':242816114,'ls':[{'id':242816115,'name':'ille','wfs':[{'id':242816141,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illud'},{'id':244509871,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illud'}]}],'name':'ille@PRO'}]">illud</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':3854248,'ls':[{'id':1200,'name':'aufero','wfs':[{'id':243182172,'morph':{'Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'FUTURE','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'auferemus'},{'id':3854436,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'FUTURE','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'auferemus'}]}],'name':'aufero@V'}]">auferemus</w>
            <c> </c>
            <w lemma="[{'id':109436,'ls':[{'id':109437,'name':'ab','wfs':[{'id':109438,'morph':{'Pos':'AP'},'name':'ab'}]}],'name':'ab@AP'}]">ab</w>
            <c> </c>
            <w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':242816209,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'ea'},{'id':244510088,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'ea'},{'id':422727873,'morph':{'Casus':'ABLATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ea'},{'id':244510094,'morph':{'Casus':'ABLATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ea'},{'id':244510108,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ea'},{'id':595329987,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ea'}]}],'name':'is@PRO'}]">ea</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':1034174,'ls':[{'id':4372,'name':'filius','wfs':[{'id':297486299,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'filii'},{'id':1034175,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'filii'},{'id':252932508,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'filii'}]}],'name':'filius@NN'}]">Filii</w>
            <c> </c>
            <w lemma="[{'id':244511757,'ls':[{'id':244511758,'name':'suus','wfs':[{'id':244511849,'morph':{'Casus':'GENITIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'REFLEXIVE'},'name':'sui'},{'id':245550207,'morph':{'Casus':'GENITIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'sui'},{'id':608010491,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'REFLEXIVE'},'name':'sui'},{'id':253418516,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'sui'},{'id':244511821,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'sui'}]}],'name':'suus@PRO'}]">sui</w>
            <c> </c>
            <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':244509722,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187641,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187642,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':242816022,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'}]}],'name':'hic@PRO'}]">hoc</w>
            <c> </c>
            <w lemma="[{'id':109834,'ls':[{'id':109835,'name':'etiam','wfs':[{'id':109836,'morph':{'Pos':'CON'},'name':'etiam'}]}],'name':'etiam@CON'}]">etiam</w>
            <c> </c>
            <w lemma="[{'id':6130438,'ls':[{'id':5644,'name':'laudo','wfs':[{'id':6130748,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'laudaverunt'}]}],'name':'laudo@V'}]">laudaverunt</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':608889,'ls':[{'id':608890,'name':'Stephanus','wfs':[{'id':608905,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Stephanus'}]}],'name':'Stephanus@NP'}]">Stephanus</w>
            <c> </c>
            <w lemma="[{'id':358959,'ls':[{'id':358960,'name':'Constantinus','wfs':[{'id':358975,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Constantinus'}]}],'name':'Constantinus@NP'}]">Constantinus</w>
            <c> </c>
            <w lemma="[{'id':253356,'ls':[{'id':9254,'name':'videlicet','wfs':[{'id':253359,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'videlicet'}]}],'name':'videlicet@ADV'}]">videlicet</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':420413,'ls':[{'id':420414,'name':'Gaufredus','wfs':[{'id':488433441,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Gaufredus'},{'id':420429,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Gaufredus'}]}],'name':'Gaufredus@NP'}]">Gaufredus</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':256816,'ls':[{'id':256817,'name':'unus','wfs':[{'id':531794819,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NUM'},'name':'unus'}]}],'name':'unus@NUM'}]">Unus</w>
            <c> </c>
            <w lemma="[{'id':864007,'ls':[{'id':1960,'name':'clericus','wfs':[{'id':864022,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'clericus'}]}],'name':'clericus@NN'}]">clericus</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':244509722,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187641,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187642,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':242816022,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'}]}],'name':'hic@PRO'}]">Hoc</w>
            <c> </c>
            <w lemma="[{'id':109834,'ls':[{'id':109835,'name':'etiam','wfs':[{'id':109836,'morph':{'Pos':'CON'},'name':'etiam'}]}],'name':'etiam@CON'}]">etiam</w>
            <c> </c>
            <w lemma="[{'id':1192980,'ls':[{'id':1192981,'name':'notus','wfs':[{'id':1192990,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'notum'}]}],'name':'notus@NN'}]">notum</w>
            <c> </c>
            <w lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':244519866,'morph':{'Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'sit'}]}],'name':'sum@V'}]">sit</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109986,'ls':[{'id':109987,'name':'quod','wfs':[{'id':109988,'morph':{'Pos':'CON'},'name':'quod'}]}],'name':'quod@CON'}]">quod</w>
            <c> </c>
            <w lemma="[{'id':1161055,'ls':[{'id':1161056,'name':'medietas','wfs':[{'id':243392635,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'medietatem'}]}],'name':'medietas@NN'}]">medietatem</w>
            <c> </c>
            <w lemma="[{'id':256816,'ls':[{'id':256817,'name':'unus','wfs':[{'id':256839,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NUM'},'name':'unam'}]}],'name':'unus@NUM'}]">unam</w>
            <c> </c>
            <w lemma="[{'id':5109084,'ls':[{'id':3001,'name':'do','wfs':[{'id':5109405,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'dedit'}]}],'name':'do@V'}]">dedit</w>
            <c> </c>
            <w lemma="[{'id':242816153,'ls':[{'id':242816154,'name':'ipse','wfs':[{'id':244509994,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ipse'},{'id':607800356,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ipse'}]}],'name':'ipse@PRO'}]">ipse</w>
            <c> </c>
            <w lemma="[{'id':327353,'ls':[{'id':327354,'name':'Bernardus','wfs':[{'id':327368,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Bernardus'},{'id':593380,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Bernardus'}]}],'name':'Bernardus@NP'}]">Bernardus</w>
            <c> </c>
            <w lemma="[{'id':109629,'ls':[{'id':109630,'name':'pro','wfs':[{'id':109631,'morph':{'Pos':'AP'},'name':'pro'}]}],'name':'pro@AP'}]">pro</w>
            <c> </c>
            <w lemma="[{'id':757606,'ls':[{'id':791,'name':'anima','wfs':[{'id':757623,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'anima'},{'id':757622,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'anima'},{'id':757617,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'anima'}]}],'name':'anima@NN'}]">anima</w>
            <c> </c>
            <w lemma="[{'id':1406744,'ls':[{'id':1406745,'name':'suum','wfs':[{'id':497738334,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'sua'},{'id':595926658,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'sua'}]}],'name':'suum@NN'}]">sua</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':226546,'ls':[{'id':226547,'name':'quousque','wfs':[{'id':226549,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'quousque'}]}],'name':'quousque@ADV'}]">quousque</w>
            <c> </c>
            <w lemma="[{'id':1195507,'ls':[{'id':6458,'name':'nummus','wfs':[{'id':1195514,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nummi'},{'id':1195520,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'nummi'},{'id':1195513,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nummi'},{'id':1195508,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'nummi'}]}],'name':'nummus@NN'}]">nummi</w>
            <c> </c>
            <w lemma="[{'id':183425,'ls':[{'id':183428,'name':'jam','wfs':[{'id':183429,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'jam'}]}],'name':'iam@ADV'}]">jam</w>
            <c> </c>
            <w lemma="[{'id':963599,'ls':[{'id':3493,'name':'dictum','wfs':[{'id':963611,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'dicti'}]}],'name':'dictum@NN'}]">dicti</w>
            <c> </c>
            <w lemma="[{'id':6658115,'ls':[{'id':7010,'name':'persolvo','wfs':[{'id':6658914,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'IMPERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'persolverentur'}]}],'name':'persolvo@V'}]">persolverentur</w>
            <c>.</c>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
