<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">Chartes de l'abbaye de Cluny vol. 5: 4347</title>
        <title type="sub">CHARTA QUA ODO, FILIUS DUCIS BURGUNDIÆ, LAUDAT ELEEMOSYNAM A PATRE
                    SUO FACTAM MONASTERIO CLUNIACENSI APUD BERNAM ET CASTELLIONEM.</title>
        <author xml:lang="lat">Diplomata Cluniacensis</author>
        <editor key="TG" corresp="#LTACorpusEditor">
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
        </editor>
        <editor key="AE" corresp="#LTACorpusEditor">
          <persName>
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
        </editor>
        <editor key="FW" corresp="#LTACorpusPublisher">
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
        </editor>
        <respStmt>
          <orgName type="provider" ref="http://www.cbma-project.eu/">CBMA Project (Corpus
                        de la Bourgogne du Moyen Âge)</orgName>
          <resp>
            <note type="remarkResponsibility">Provider of the source material</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Ernst</surname>
            <forename>Alexandra</forename>
          </persName>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation of the TEI-document</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName key="VK">
            <surname>Koch</surname>
            <forename>Vincent</forename>
          </persName>
          <persName key="JD">
            <surname>Doepp</surname>
            <forename>Joscha</forename>
          </persName>
          <persName key="JS">
            <surname>Schulz</surname>
            <forename>Jashty</forename>
          </persName>
          <persName key="CS">
            <surname>Splettsen</surname>
            <forename>Convin</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Preparation support</note>
          </resp>
        </respStmt>
        <respStmt>
          <persName ref="http://viaf.org/viaf/185313129">
            <surname>Geelhaar</surname>
            <forename>Tim</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note>
          </resp>
        </respStmt>
        <respStmt>
          <persName>
            <surname>Wiegand</surname>
            <forename>Frank</forename>
          </persName>
          <resp>
            <note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note>
          </resp>
        </respStmt>
        <respStmt>
          <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
          <resp>
            <note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note>
            <ref target="https://www.bbaw.de"/>
          </resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition>LTA Edition 1.0</edition>
      </editionStmt>
      <publicationStmt>
        <publisher xml:id="LTACorpusPublisher">
          <orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences
                        and Humanities</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>lta@bbaw.de</email>
          <address n="1">
            <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
          </address>
        </publisher>
        <publisher xml:id="LTACorpusEditor">
          <orgName n="2" role="hostingInstitution">Goethe University Frankfurt</orgName>
          <orgName role="project">Latin Text Archive</orgName>
          <email>jussen@em.uni-frankfurt.de</email>
          <email>geelhaar@em.uni-frankfurt.de</email>
          <address n="2">
            <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
          </address>
        </publisher>
        <pubPlace>Frankfurt am Main</pubPlace>
        <date type="publication-online" when="2021-01-15">2021-01-15</date>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0/">
            <p>CC BY-NC 4.0</p>
          </licence>
        </availability>
        <idno type="C_Stat">12</idno>
        <idno type="C1">12</idno>
        <idno type="C2">12</idno>
        <idno type="Time">1190 ca.</idno>
        <idno type="VIAF_(Expression)">not available</idno>
        <idno type="VIAF_(Person)">not available</idno>
        <idno type="Year_of_Publication_(interpreted)">1190</idno>
      </publicationStmt>
      <sourceDesc>
        <p>
          <bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 5: 1091–1210. Paris, Imprimerie Nationale, 1894</bibl>
          <bibl type="Volume">5</bibl>
          <bibl type="Column">711</bibl>
          <bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl>
          <bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=4347</bibl>
          <bibl type="Reference">(D. 280.)</bibl>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <editorialDecl>
        <p>
          <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        Bernard/Bruel edition and prepared at Goethe University Frankfurt using the
                        Historical Semantics Corpus Management (HSCM) on the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
          <desc type="Status">automatically lemmatized</desc>
          <desc type="Commentary" resp="TG">dated according to the edition (TG)</desc>
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="lat">Latin</language>
      </langUsage>
      <textClass>
        <classCode scheme="Text_Type">Charter</classCode>
        <classCode scheme="Text_Type_Class">Legal</classCode>
        <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <listChange>
        <change type="Annotation_created_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_created_on">Mon Feb 15 10:13:41 CET 2021</change>
        <change type="Annotation_last_changed_by" resp="TG">Tim Geelhaar</change>
        <change type="Annotation_last_changed_on">Mon Feb 15 10:13:41 CET 2021</change>
        <change type="version_released_by" resp="TG">Tim Geelhaar</change>
        <change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <p>
          <s>
            <w lemma="[{'id':7319987,'ls':[{'id':8159,'name':'scio','wfs':[{'id':7320171,'morph':{'ConjugationType':'FOURTH_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'sciant'}]}],'name':'scio@V'}]">Sciant</w>
            <c> </c>
            <w lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':244511016,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382851706,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382871756,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':594773492,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]">qui</w>
            <c> </c>
            <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':532695117,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO'},'name':'has'},{'id':242816006,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'has'}]}],'name':'hic@PRO'}]">has</w>
            <c> </c>
            <w lemma="[{'id':1142644,'ls':[{'id':5789,'name':'littera','wfs':[{'id':1142660,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'litteras'}]}],'name':'littera@NN'}]">litteras</w>
            <c> </c>
            <w lemma="[{'id':8004098,'ls':[{'id':9253,'name':'video','wfs':[{'id':8004782,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'viderint'},{'id':8004786,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'viderint'}]}],'name':'video@V'}]">viderint</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109986,'ls':[{'id':109987,'name':'quod','wfs':[{'id':109988,'morph':{'Pos':'CON'},'name':'quod'}]}],'name':'quod@CON'}]">quod</w>
            <c> </c>
            <w lemma="[{'id':242815965,'ls':[{'id':242815966,'name':'ego','wfs':[{'id':244509626,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509632,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509629,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'}]}],'name':'ego@PRO'}]">ego</w>
            <c> </c>
            <w lemma="[{'id':244954053,'ls':[{'id':244954052,'name':'Oddo','wfs':[{'id':488470477,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Oddo'},{'id':245547129,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Oddo'}]}],'name':'Oddo@NP'}]">Oddo</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1034174,'ls':[{'id':4372,'name':'filius','wfs':[{'id':297486300,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'filius'},{'id':1034176,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'filius'}]}],'name':'filius@NN'}]">filius</w>
            <c> </c>
            <w lemma="[{'id':984318,'ls':[{'id':3752,'name':'dux','wfs':[{'id':243329683,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ducis'},{'id':984332,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'ducis'}]}],'name':'dux@NN'}]">ducis</w>
            <c> </c>
            <w lemma="[{'id':241358124,'ls':[{'id':241358125,'name':'Burgundia','wfs':[{'id':487746186,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NE'},'name':'Burgundiæ'},{'id':487746190,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NE'},'name':'Burgundiæ'},{'id':487746187,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NE'},'name':'Burgundiæ'}]}],'name':'Burgundia@NE'}]">Burgundiæ</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':4371976,'ls':[{'id':244530661,'name':'concedo','wfs':[{'id':4372072,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_1','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'concedo'}]}],'name':'concedo@V'}]">concedo</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':1133332,'ls':[{'id':5649,'name':'laus','wfs':[{'id':1133345,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'laudo'},{'id':1133344,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'laudo'}]}],'name':'laus@NN'}]">laudo</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':2948837,'ls':[{'id':7732,'name':'ratus','wfs':[{'id':2948900,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'ratam'}]}],'name':'ratus@ADJ'}]">ratam</w>
            <c> </c>
            <w lemma="[{'id':5618354,'ls':[{'id':4757,'name':'habeo','wfs':[{'id':5618516,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_1','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'habeo'}]}],'name':'habeo@V'}]">habeo</w>
            <c> </c>
            <w lemma="[{'id':990534,'ls':[{'id':505851544,'name':'elemosina','wfs':[{'id':990558,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'elemosinam'}]}],'name':'eleemosyna@NN'}]">elemosinam</w>
            <c> </c>
            <w lemma="[{'id':225505,'ls':[{'id':7683,'name':'quam','wfs':[{'id':225506,'morph':{'Pos':'ADV'},'name':'quam'}]}],'name':'quam@ADV'}]">quam</w>
            <c> </c>
            <w lemma="[{'id':979873,'ls':[{'id':3699,'name':'dominus','wfs':[{'id':979888,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'dominus'}]}],'name':'dominus@NN'}]">dominus</w>
            <c> </c>
            <w lemma="[{'id':1228946,'ls':[{'id':6842,'name':'pater','wfs':[{'id':510638003,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'pater'},{'id':1228960,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'pater'}]}],'name':'pater@NN'}]">pater</w>
            <c> </c>
            <w lemma="[{'id':244510189,'ls':[{'id':244510190,'name':'meus','wfs':[{'id':244510247,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'meus'}]}],'name':'meus@PRO'}]">meus</w>
            <c> </c>
            <w lemma="[{'id':984318,'ls':[{'id':3752,'name':'dux','wfs':[{'id':243329688,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'dux'},{'id':984333,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'dux'},{'id':243329684,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'dux'},{'id':243329685,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'dux'}]}],'name':'dux@NN'}]">dux</w>
            <c> </c>
            <w lemma="[{'id':5475468,'ls':[{'id':4230,'name':'facio','wfs':[{'id':5475808,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'fecit'}]}],'name':'facio@V'}]">fecit</w>
            <c> </c>
            <w lemma="[{'id':985146,'ls':[{'id':3766,'name':'ecclesia','wfs':[{'id':497261383,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesiæ'},{'id':497261382,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesiæ'},{'id':241428906,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'ecclesiæ'}]}],'name':'ecclesia@NN'}]">ecclesiæ</w>
            <c> </c>
            <w lemma="[{'id':1481107,'ls':[{'id':1481108,'name':'Cluniacensis','wfs':[{'id':1481151,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481140,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481146,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481139,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481152,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481145,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'}]}],'name':'Cluniacensis@ADJ'}]">Cluniacensi</w>
            <c> </c>
            <w lemma="[{'id':109487,'ls':[{'id':109488,'name':'apud','wfs':[{'id':109489,'morph':{'Pos':'AP'},'name':'apud'}]}],'name':'apud@AP'}]">apud</w>
            <c> </c>
            <w lemma="[{'id':810656,'ls':[{'id':810657,'name':'berna','wfs':[{'id':810666,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'bernam'}]}],'name':'berna@NN'}]">Bernam</w>
            <c> </c>
            <w lemma="[{'id':109629,'ls':[{'id':109630,'name':'pro','wfs':[{'id':109631,'morph':{'Pos':'AP'},'name':'pro'}]}],'name':'pro@AP'}]">pro</w>
            <c> </c>
            <w lemma="[{'id':1010698,'ls':[{'id':4037,'name':'excessus','wfs':[{'id':244750468,'morph':{'Casus':'DATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'excessibus'},{'id':244750476,'morph':{'Casus':'ABLATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'excessibus'}]}],'name':'excessus@NN'}]">excessibus</w>
            <c> </c>
            <w lemma="[{'id':1406782,'ls':[{'id':488516594,'name':'suus','wfs':[{'id':488516607,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'suis'},{'id':488516608,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'suis'}]}],'name':'suus@NN'}]">suis</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':235785,'ls':[{'id':8331,'name':'similiter','wfs':[{'id':235788,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'similiter'}]}],'name':'similiter@ADV'}]">Similiter</w>
            <c> </c>
            <w lemma="[{'id':109834,'ls':[{'id':109835,'name':'etiam','wfs':[{'id':109836,'morph':{'Pos':'CON'},'name':'etiam'}]}],'name':'etiam@CON'}]">etiam</w>
            <c> </c>
            <w lemma="[{'id':4371976,'ls':[{'id':244530661,'name':'concedo','wfs':[{'id':4372072,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_1','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'concedo'}]}],'name':'concedo@V'}]">concedo</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':2948837,'ls':[{'id':7732,'name':'ratus','wfs':[{'id':2948900,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'ratam'}]}],'name':'ratus@ADJ'}]">ratam</w>
            <c> </c>
            <w lemma="[{'id':5618354,'ls':[{'id':4757,'name':'habeo','wfs':[{'id':5618516,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_1','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'habeo'}]}],'name':'habeo@V'}]">habeo</w>
            <c> </c>
            <w lemma="[{'id':990534,'ls':[{'id':505851544,'name':'elemosina','wfs':[{'id':990558,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'elemosinam'}]}],'name':'eleemosyna@NN'}]">elemosinam</w>
            <c> </c>
            <w lemma="[{'id':225505,'ls':[{'id':7683,'name':'quam','wfs':[{'id':225506,'morph':{'Pos':'ADV'},'name':'quam'}]}],'name':'quam@ADV'}]">quam</w>
            <c> </c>
            <w lemma="[{'id':5475468,'ls':[{'id':4230,'name':'facio','wfs':[{'id':5475808,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'fecit'}]}],'name':'facio@V'}]">fecit</w>
            <c> </c>
            <w lemma="[{'id':242816067,'ls':[{'id':242816068,'name':'idem','wfs':[{'id':244509767,'morph':{'Casus':'DATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'eidem'},{'id':242816078,'morph':{'Casus':'DATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'eidem'},{'id':244509785,'morph':{'Casus':'DATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'eidem'}]}],'name':'idem@PRO'}]">eidem</w>
            <c> </c>
            <w lemma="[{'id':985146,'ls':[{'id':3766,'name':'ecclesia','wfs':[{'id':497261383,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesiæ'},{'id':497261382,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesiæ'},{'id':241428906,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'ecclesiæ'}]}],'name':'ecclesia@NN'}]">ecclesiæ</w>
            <c> </c>
            <w lemma="[{'id':1481107,'ls':[{'id':1481108,'name':'Cluniacensis','wfs':[{'id':1481151,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481140,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481146,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481139,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481152,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'},{'id':1481145,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensi'}]}],'name':'Cluniacensis@ADJ'}]">Cluniacensi</w>
            <c> </c>
            <w lemma="[{'id':109487,'ls':[{'id':109488,'name':'apud','wfs':[{'id':109489,'morph':{'Pos':'AP'},'name':'apud'}]}],'name':'apud@AP'}]">apud</w>
            <c> </c>
            <w lemma="[{'id':241359238,'ls':[{'id':241359239,'name':'Castellio','wfs':[{'id':238787866,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NE'},'name':'Castellionem'}]}],'name':'Castellio@NP'}]">Castellionem</w>
            <c> </c>
            <w lemma="[{'id':109629,'ls':[{'id':109630,'name':'pro','wfs':[{'id':109631,'morph':{'Pos':'AP'},'name':'pro'}]}],'name':'pro@AP'}]">pro</w>
            <c> </c>
            <w lemma="[{'id':1339480,'ls':[{'id':8076,'name':'salus','wfs':[{'id':243454551,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'salute'}]}],'name':'salus@NN'}]">salute</w>
            <c> </c>
            <w lemma="[{'id':757606,'ls':[{'id':791,'name':'anima','wfs':[{'id':241403265,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'animæ'},{'id':497261385,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'animæ'},{'id':497261384,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'animæ'}]}],'name':'anima@NN'}]">animæ</w>
            <c> </c>
            <w lemma="[{'id':244511757,'ls':[{'id':244511758,'name':'suus','wfs':[{'id':251222967,'morph':{'Casus':'GENITIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'suæ'}]}],'name':'suus@PRO'}]">suæ</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':1406782,'ls':[{'id':488516594,'name':'suus','wfs':[{'id':488516606,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'suorum'}]}],'name':'suus@NN'}]">suorum</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':205326,'ls':[{'id':205327,'name':'necnon','wfs':[{'id':205328,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'necnon'}]}],'name':'necnon@ADV'}]">necnon</w>
            <c> </c>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w>
            <c> </c>
            <w lemma="[{'id':109629,'ls':[{'id':109630,'name':'pro','wfs':[{'id':109631,'morph':{'Pos':'AP'},'name':'pro'}]}],'name':'pro@AP'}]">pro</w>
            <c> </c>
            <w lemma="[{'id':757606,'ls':[{'id':791,'name':'anima','wfs':[{'id':757623,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'anima'},{'id':757622,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'anima'},{'id':757617,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'anima'}]}],'name':'anima@NN'}]">anima</w>
            <c> </c>
            <w lemma="[{'id':429672,'ls':[{'id':429673,'name':'Girardus','wfs':[{'id':429686,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Girardi'},{'id':429679,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Girardi'},{'id':488436839,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Girardi'},{'id':429674,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Girardi'},{'id':429680,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Girardi'}]}],'name':'Girardus@NP'}]">Girardi</w>
            <c> </c>
            <w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w>
            <c> </c>
            <w>Reun</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':2322664,'ls':[{'id':4360,'name':'fidelis','wfs':[{'id':242036828,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'fidelis'},{'id':2322757,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'fidelis'},{'id':2322763,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'fidelis'},{'id':2322777,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'fidelis'},{'id':2322696,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'fidelis'},{'id':2322760,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'fidelis'},{'id':2322783,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'fidelis'},{'id':2322798,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'fidelis'},{'id':2322780,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'fidelis'},{'id':2322668,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'fidelis'}]}],'name':'fidelis@ADJ'}]">fidelis</w>
            <c> </c>
            <w lemma="[{'id':1406782,'ls':[{'id':488516594,'name':'suus','wfs':[{'id':488516598,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'sui'},{'id':488516603,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'sui'},{'id':488516604,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'sui'}]}],'name':'suus@NN'}]">sui</w>
            <c>.</c>
            <c> </c>
          </s>
          <s>
            <w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">Et</w>
            <c> </c>
            <w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':36189678,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hæc'},{'id':6095323,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hæc'},{'id':36189679,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hæc'}]}],'name':'hic@PRO'}]">hæc</w>
            <c> </c>
            <w lemma="[{'id':2874459,'ls':[{'id':242457998,'name':'praesens','wfs':[{'id':242458009,'morph':{'Pos':'ADJ'},'name':'præsentis'}]}],'name':'praesens@ADJ'}]">præsentis</w>
            <c> </c>
            <w lemma="[{'id':849184,'ls':[{'id':835608,'name':'carta','wfs':[{'id':487791183,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'cartæ'},{'id':487791186,'morph':{'Casus':'GENITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'cartæ'},{'id':487791182,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'cartæ'}]}],'name':'charta@NN'}]">cartæ</w>
            <c> </c>
            <w lemma="[{'id':244510189,'ls':[{'id':244510190,'name':'meus','wfs':[{'id':251222954,'morph':{'Casus':'GENITIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'meæ'}]}],'name':'meus@PRO'}]">meæ</w>
            <c> </c>
            <w lemma="[{'id':1415946,'ls':[{'id':8812,'name':'testimonium','wfs':[{'id':243482019,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'testimonio'},{'id':243482018,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'testimonio'}]}],'name':'testimonium@NN'}]">testimonio</w>
            <c> </c>
            <w lemma="[{'id':4428053,'ls':[{'id':2420,'name':'confirmo','wfs':[{'id':4428220,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_1','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'confirmavi'}]}],'name':'confirmo@V'}]">confirmavi</w>
            <c>,</c>
            <c> </c>
            <w lemma="[{'id':109868,'ls':[{'id':109869,'name':'ne','wfs':[{'id':109870,'morph':{'Pos':'CON'},'name':'ne'}]}],'name':'ne@CON'}]">ne</w>
            <c> </c>
            <w lemma="[{'id':249448,'ls':[{'id':249449,'name':'ullatenus','wfs':[{'id':249450,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'ullatenus'}]}],'name':'ullatenus@ADV'}]">ullatenus</w>
            <c> </c>
            <w lemma="[{'id':6741602,'ls':[{'id':7228,'name':'possum','wfs':[{'id':244516514,'morph':{'Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'possint'}]}],'name':'possum@V'}]">possint</w>
            <c> </c>
            <w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w>
            <c> </c>
            <w lemma="[{'id':1265110,'ls':[{'id':1265111,'name':'posterus','wfs':[{'id':1265120,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'posterum'}]}],'name':'posterus@NN'}]">posterum</w>
            <c> </c>
            <w lemma="[{'id':5841356,'ls':[{'id':5208,'name':'infirmo','wfs':[{'id':5841438,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'infirmari'}]}],'name':'infirmo@V'}]">infirmari</w>
            <c>.</c>
          </s>
        </p>
      </div>
    </body>
  </text>
</TEI>
