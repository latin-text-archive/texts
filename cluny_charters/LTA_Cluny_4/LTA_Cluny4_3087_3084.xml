<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">Chartes de l'abbaye de Cluny vol. 4: 3084</title>
                <title type="sub">CHARTA QUA NOTUM EST HUMBERTUM DE CURTEVAS VERCHERIAM DE PUTEO ET
                    TERRAM QUAMDAM MONASTERIO CLUNIACENSI DEDISSE.</title>
                <author xml:lang="lat">Diplomata Cluniacensis</author>
                <editor key="TG" corresp="#LTACorpusEditor"><persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor key="AE" corresp="#LTACorpusEditor"><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor key="FW" corresp="#LTACorpusPublisher"><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.cbma-project.eu/" type="provider">CBMA Project (Corpus de la Bourgogne
                        du Moyen Âge)</orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName key="VK">
                        <surname>Koch</surname>
                        <forename>Vincent</forename>
                    </persName>
                    <persName key="JD">
                        <surname>Doepp</surname>
                        <forename>Joscha</forename>
                    </persName>
                    <persName key="JS">
                        <surname>Schulz</surname>
                        <forename>Jashty</forename>
                    </persName>
                    <persName key="CS">
                        <surname>Splettsen</surname>
                        <forename>Convin</forename>
                    </persName>
                    <resp>
                        <note type="remarkResponsibility">Preparation support</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"/></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <edition>LTA Edition 1.0</edition>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences and Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName n="2" role="hostingInstitution">Goethe
                        University Frankfurt</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-01-15">2021-01-15</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">11</idno>
                
                <idno type="C1">11</idno><idno type="C2">11</idno>
                <idno type="Time">1049 - 1109 (?)</idno>
                <idno type="VIAF_(Expression)">not available</idno>
                <idno type="VIAF_(Person)">not available</idno>
                <idno type="Year_of_Publication_(interpreted)">1050</idno>
            </publicationStmt>
            <sourceDesc>
                <p><bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 4: 1027–1090. Paris, Imprimerie Nationale, 1888</bibl><bibl type="Volume">4</bibl><bibl type="Column">260</bibl><bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl><bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3084</bibl>
                    <bibl type="Reference">(B. h. 201, cciii.)</bibl>
                </p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p><desc type="Digital_Edition_Statement">The text has been retrieved from the Bernard/Bruel edition and prepared at Goethe
                    University Frankfurt using the Historical Semantics Corpus Management (HSCM) on
                    the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                    edition as it offers only the main reading of the text for analytical purposes.
                    Brackets, hyphenation, and quotation marks have been removed. The Medieval Latin
                    orthography and the pagination of the edition have been preserved.</desc><desc type="Status">automatically lemmatized</desc><desc type="Commentary" resp="TG">The
                        Bernard/Bruel edition contains 301 documents, which the editors could only
                        date to a period between 1048 and 1109 at the latest. For a chronological
                        classification that allows statistical analysis, these documents were
                        distributed evenly over the three quarters of a century between 1050 and
                        1109 according to the order of the documents in the Cluny edition. If more
                        precise chronological allocations can be made on the basis of semantic
                        features, the allocations by quarter of a century are also revised on this
                        basis. (TG)</desc></p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Charter</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange><change type="Annotation_created_by" resp="TG">Tim Geelhaar</change><change type="Annotation_created_on">Mon Jan 25 13:39:13 CET 2021</change><change type="Annotation_last_changed_by" resp="TG"> Tim Geelhaar</change><change type="Annotation_last_changed_on">Mon Jan 25 13:39:13 CET
                    2021</change>
            <change type="version_released_by" resp="TG">Tim Geelhaar</change><change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change></listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div>
                <p><s><w lemma="[{'id':1118606,'ls':[{'id':1118607,'name':'itis','wfs':[{'id':243377937,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Numerus':'SINGULAR','Pos':'NN'},'name':'item'}]}],'name':'itis@NN'}]">Item</w><c> </c><w lemma="[{'id':7319987,'ls':[{'id':8159,'name':'scio','wfs':[{'id':7320026,'morph':{'ConjugationType':'FOURTH_CONJUGATION','Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'sciri'}]}],'name':'scio@V'}]">sciri</w><c> </c><w lemma="[{'id':4707704,'ls':[{'id':3046,'name':'decerno','wfs':[{'id':4707831,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'decrevimus'}]}],'name':'decerno@V'}]">decrevimus</w><c>,</c><c> </c><w lemma="[{'id':110002,'ls':[{'id':110003,'name':'quoniam','wfs':[{'id':110004,'morph':{'Pos':'CON'},'name':'quoniam'}]}],'name':'quoniam@CON'}]">quoniam</w><c> </c><w lemma="[{'id':473266,'ls':[{'id':473267,'name':'Humbertus','wfs':[{'id':473281,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Humbertus'},{'id':661229,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Humbertus'}]}],'name':'Humbertus@NP'}]">Humbertus</w><c> </c><w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w><c> </c><w>Curtevas</w><c> </c><w lemma="[{'id':5109084,'ls':[{'id':3001,'name':'do','wfs':[{'id':5109405,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'dedit'}]}],'name':'do@V'}]">dedit</w><c> </c><w lemma="[{'id':488416956,'ls':[{'id':488416957,'name':'Deo','wfs':[{'id':488416964,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NP'},'name':'Deo'}]}],'name':'Deo@NP'}]">Deo</w><c> </c><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w><c> </c><w lemma="[{'id':2859931,'ls':[{'id':2860044,'name':'predictus','wfs':[{'id':2860051,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'predictis'},{'id':2860090,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'predictis'},{'id':2860074,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'predictis'},{'id':2860055,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'predictis'},{'id':2860093,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'predictis'},{'id':2860077,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'predictis'}]}],'name':'praedictus@ADJ'}]">predictis</w><c> </c><w lemma="[{'id':767705,'ls':[{'id':244741368,'name':'apostolus','wfs':[{'id':767707,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'apostolis'},{'id':767708,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'apostolis'}]}],'name':'apostolus@NN'}]">apostolis</w><c> </c><w lemma="[{'id':1173036,'ls':[{'id':1173037,'name':'monachus','wfs':[{'id':483609891,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'monachisque'},{'id':483609893,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'monachisque'}]}],'name':'monachus@NN'}]">monachisque</w><c> </c><w lemma="[{'id':1481107,'ls':[{'id':1481108,'name':'Cluniacensis','wfs':[{'id':1481111,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cluniacensibus'},{'id':1481132,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cluniacensibus'},{'id':1481130,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cluniacensibus'},{'id':1481123,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cluniacensibus'},{'id':1481121,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cluniacensibus'},{'id':1481113,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cluniacensibus'}]}],'name':'Cluniacensis@ADJ'}]">Cluniacensibus</w><c> </c><w>vercheriam</w><c> </c><w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w><c> </c><w lemma="[{'id':244494563,'ls':[{'id':244494560,'name':'ProperName','wfs':[{'id':743417377,'morph':{'Pos':'NE'},'name':'Puteo'}]}],'name':'ProperName@NE'}]">Puteo</w><c>,</c><c> </c><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w><c> </c><w lemma="[{'id':1414743,'ls':[{'id':8799,'name':'terra','wfs':[{'id':1414768,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'terram'}]}],'name':'terra@NN'}]">terram</w><c> </c><w lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':457578108,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'quæ'},{'id':457578107,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'quæ'},{'id':457578113,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'quæ'},{'id':6095322,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'quæ'}]}],'name':'qui@PRO'}]">quæ</w><c> </c><w lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':242825881,'morph':{'Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'est'}]}],'name':'sum@V'}]">est</w><c> </c><w lemma="[{'id':109587,'ls':[{'id':109588,'name':'inter','wfs':[{'id':109589,'morph':{'Pos':'AP'},'name':'inter'}]}],'name':'inter@AP'}]">inter</w><c> </c><w lemma="[{'id':1460892,'ls':[{'id':9241,'name':'via','wfs':[{'id':1460953,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'viam'}]}],'name':'via@NN'}]">viam</w><c> </c><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w><c> </c><w>concisiam</w><c> </c><w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w><c> </c><w>Romunto</w><c>,</c><c> </c><w lemma="[{'id':6130438,'ls':[{'id':5644,'name':'laudo','wfs':[{'id':6130440,'morph':{'Casus':'ABLATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'laudantibus'},{'id':6130456,'morph':{'Casus':'ABLATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'laudantibus'},{'id':6130448,'morph':{'Casus':'ABLATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'laudantibus'},{'id':6130442,'morph':{'Casus':'DATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'laudantibus'},{'id':6130458,'morph':{'Casus':'DATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'laudantibus'},{'id':6130450,'morph':{'Casus':'DATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'laudantibus'}]}],'name':'laudo@V'}]">laudantibus</w><c> </c><w lemma="[{'id':1034174,'ls':[{'id':4372,'name':'filius','wfs':[{'id':243347992,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'filiis'},{'id':243347991,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'filiis'}]}],'name':'filius@NN'}]">filiis</w><c> </c><w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510032,'morph':{'Casus':'GENITIVE','Genus':'COMMON','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ejus'}]}],'name':'is@PRO'}]">ejus</w><c> </c><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w><c> </c><w lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':244509722,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187641,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':36187642,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'},{'id':242816022,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'hoc'}]}],'name':'hic@PRO'}]">hoc</w><c> </c><w lemma="[{'id':3848706,'ls':[{'id':3848707,'name':'auctorizo','wfs':[{'id':3848719,'morph':{'Casus':'DATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'auctorizantibus'},{'id':3848727,'morph':{'Casus':'DATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'auctorizantibus'},{'id':3848711,'morph':{'Casus':'DATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'auctorizantibus'},{'id':3848717,'morph':{'Casus':'ABLATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'auctorizantibus'},{'id':3848725,'morph':{'Casus':'ABLATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'auctorizantibus'},{'id':3848709,'morph':{'Casus':'ABLATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'FEMININE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'auctorizantibus'}]}],'name':'auctorizo@V'}]">auctorizantibus</w><c>.</c></s></p>
            </div>
        </body>
    </text>
</TEI>
