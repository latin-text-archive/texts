<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">Chartes de l'abbaye de Cluny vol. 4: 3514</title>
                <title type="sub">CHARTA QUA ISARNUS, TOLOSANUS EPISCOPUS, TRADIT TOLOSANAM
                    ECCLESIAM BEATÆ MARIÆ MONASTERIO CLUNIACENSI, UT MONASTICUS ORDO IBI MANEAT;
                    LOCUM AUTEM IPSUM LIBERUM ESSE CONCEDIT, EXCEPTO CENSU ANNUO ET JUDÆI
                    SERVITIO.</title>
                <author xml:lang="lat">Diplomata Cluniacensis</author>
                <editor key="TG" corresp="#LTACorpusEditor"><persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor key="AE" corresp="#LTACorpusEditor"><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor key="FW" corresp="#LTACorpusPublisher"><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.cbma-project.eu/" type="provider">CBMA Project (Corpus de la Bourgogne
                        du Moyen Âge)</orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName key="VK">
                        <surname>Koch</surname>
                        <forename>Vincent</forename>
                    </persName>
                    <persName key="JD">
                        <surname>Doepp</surname>
                        <forename>Joscha</forename>
                    </persName>
                    <persName key="JS">
                        <surname>Schulz</surname>
                        <forename>Jashty</forename>
                    </persName>
                    <persName key="CS">
                        <surname>Splettsen</surname>
                        <forename>Convin</forename>
                    </persName>
                    <resp>
                        <note type="remarkResponsibility">Preparation support</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"/></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <edition>LTA Edition 1.0</edition>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences and Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName n="2" role="hostingInstitution">Goethe
                        University Frankfurt</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-01-15">2021-01-15</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">11</idno>
                
                <idno type="C1">11</idno><idno type="C2">11</idno>
                <idno type="Time">1077</idno>
                <idno type="VIAF_(Expression)">not available</idno>
                <idno type="VIAF_(Person)">not available</idno>
                <idno type="Year_of_Publication_(interpreted)">1077</idno>
            </publicationStmt>
            <sourceDesc>
                <p><bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 4: 1027–1090. Paris, Imprimerie Nationale, 1888</bibl><bibl type="Volume">4</bibl><bibl type="Column">630</bibl><bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl><bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3514</bibl><bibl type="Reference">(Catel, Mémoires de Languedoc, p. 871; Antiquité de
                        Notre-Dame-de-la-Daurade, p. 113; Gallia Sammarth., t. I, p. 680, et Gallia
                        nova, t. XIII, instr. c. 9.)</bibl></p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p><desc type="Digital_Edition_Statement">The text has been retrieved from the Bernard/Bruel edition and prepared at Goethe
                    University Frankfurt using the Historical Semantics Corpus Management (HSCM) on
                    the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                    edition as it offers only the main reading of the text for analytical purposes.
                    Brackets, hyphenation, and quotation marks have been removed. The Medieval Latin
                    orthography and the pagination of the edition have been preserved.</desc><desc type="Status">automatically lemmatized</desc><desc type="Commentary" resp="TG">dated
                        according to the edition (TG)</desc></p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Charter</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange><change type="Annotation_created_by" resp="TG">Tim Geelhaar</change><change type="Annotation_created_on">Mon Jan 25 13:34:35 CET 2021</change><change type="Annotation_last_changed_by" resp="TG"> Tim Geelhaar</change><change type="Annotation_last_changed_on">Mon Jan 25 13:34:35 CET
                    2021</change>
            <change type="version_released_by" resp="TG">Tim Geelhaar</change><change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change></listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div>
                <p><s><w lemma="[{'id':109917,'ls':[{'id':109918,'name':'postquam','wfs':[{'id':109919,'morph':{'Pos':'CON'},'name':'postquam'}]}],'name':'postquam@CON'}]">Postquam</w><c> </c><w lemma="[{'id':979873,'ls':[{'id':3699,'name':'dominus','wfs':[{'id':979888,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'dominus'}]}],'name':'dominus@NN'}]">Dominus</w><c> </c><w lemma="[{'id':244510439,'ls':[{'id':244510440,'name':'noster','wfs':[{'id':244510477,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'noster'}]}],'name':'noster@PRO'}]">noster</w><c> </c><w lemma="[{'id':241370321,'ls':[{'id':241370347,'name':'Jesus','wfs':[{'id':241370352,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Jesus'},{'id':241370348,'morph':{'Genus':'MASCULINE','Pos':'NP'},'name':'Jesus'}]}],'name':'Iesus@NP'}]">Jesus</w><c> </c><w lemma="[{'id':352906,'ls':[{'id':244512690,'name':'Christus','wfs':[{'id':244512691,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Christus'}]}],'name':'Christus@NP'}]">Christus</w><c> </c><w lemma="[{'id':1597095,'ls':[{'id':645,'name':'altus','wfs':[{'id':1597146,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'alta'},{'id':1597186,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'alta'},{'id':1597163,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'alta'},{'id':1597173,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'alta'},{'id':1597166,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'alta'},{'id':1597183,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'alta'}]}],'name':'altus@ADJ'}]">alta</w><c> </c><w lemma="[{'id':1254025,'ls':[{'id':7092,'name':'pietas','wfs':[{'id':243422490,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'pietate'}]}],'name':'pietas@NN'}]">pietate</w><c> </c><w lemma="[{'id':6251915,'ls':[{'id':6251916,'name':'misereor','wfs':[{'id':6251975,'morph':{'ConjugationType':'SECOND_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_1','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'misertus'},{'id':648411256,'morph':{'Casus':'NOMINATIVE','ConjugationType':'SECOND_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'SINGULAR','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'misertus'},{'id':597979586,'morph':{'ConjugationType':'SECOND_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'misertus'}]}],'name':'misereor@V'}]">misertus</w><c> </c><w lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':242825881,'morph':{'Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'est'}]}],'name':'sum@V'}]">est</w><pb n="631"/><w lemma="[{'id':1052280,'ls':[{'id':4639,'name':'genus','wfs':[{'id':243353697,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'generi'}]}],'name':'genus@NN'}]">generi</w><c> </c><w lemma="[{'id':2396607,'ls':[{'id':4890,'name':'humanus','wfs':[{'id':2396727,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'humano'},{'id':2396704,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'humano'},{'id':2396730,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'humano'},{'id':2396707,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'humano'}]}],'name':'humanus@ADJ'}]">humano</w><c>,</c><c> </c><w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w><c>.</c><note><s><w lemma="[{'id':241356359,'ls':[{'id':241356360,'name':'Bernard','wfs':[{'id':241356362,'morph':{'Pos':'NP'},'name':'bernard'},{'id':241356363,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NP'},'name':'Bernard'},{'id':241356361,'morph':{'Pos':'NP'},'name':'Bernard'}]}],'name':'Bernard@NP'}]">Bernard</w><c> </c><w>and</w><c> </c><w>Bruel</w><c> </c><w lemma="[{'id':3876797,'ls':[{'id':3877212,'name':'haveo','wfs':[{'id':3877310,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'IMPERATIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'INTRANSITIVE','Voice':'ACTIVE'},'name':'have'}]}],'name':'aveo@V'}]">have</w><c> </c><w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':245547189,'morph':{'Pos':'FM'},'name':'not'}]}],'name':'NON_LATIN@FM'}]">not</w><c> </c><w>inserted</w><c> </c><w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':256721224,'morph':{'Pos':'FM'},'name':'the'}]}],'name':'NON_LATIN@FM'}]">the</w><c> </c><w>letter</w><c> </c><w>which</w><c> </c><w lemma="[{'id':829845,'ls':[{'id':1558,'name':'canon','wfs':[{'id':244750546,'morph':{'Pos':'NN'},'name':'can'}]}],'name':'canon@NN'}]">can</w><c> </c><w lemma="[{'id':810862,'ls':[{'id':810863,'name':'bes','wfs':[{'id':243268621,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'be'}]}],'name':'bes@NN'}]">be</w><c> </c><w>found</w><c> </c><w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w><c> </c><w>Catel</w><c>,</c><c> </c><w>Mémoires</w><c> </c><w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w><c> </c><w>Languedoc</w><c>,</c><c> </c><w lemma="[{'id':1294819,'ls':[{'id':743418636,'name':'p','wfs':[{'id':743418639,'morph':{'Casus':'ACCUSATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418637,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418643,'morph':{'Casus':'DATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418642,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418641,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'}]}],'name':'puer@NN'}]">p</w><c>.</c><c> </c><w>871</w><c>.</c><c> </c></s></note></s></p>
            </div>
        </body>
    </text>
</TEI>
