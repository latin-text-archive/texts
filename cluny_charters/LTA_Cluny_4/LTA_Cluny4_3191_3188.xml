<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">Chartes de l'abbaye de Cluny vol. 4: 3188</title>
                <title type="sub">CHARTA QUA SIGALDUS, PRÆPOSITUS CLUNIACENSIS, DAT CUIDAM MARTINO
                    ET UXORI ET FILIO EJUS VINEAM AD MEDIUM PLANTUM.</title>
                <author xml:lang="lat">Diplomata Cluniacensis</author>
                <editor key="TG" corresp="#LTACorpusEditor"><persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor key="AE" corresp="#LTACorpusEditor"><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor key="FW" corresp="#LTACorpusPublisher"><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.cbma-project.eu/" type="provider">CBMA Project (Corpus de la Bourgogne
                        du Moyen Âge)</orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName key="VK">
                        <surname>Koch</surname>
                        <forename>Vincent</forename>
                    </persName>
                    <persName key="JD">
                        <surname>Doepp</surname>
                        <forename>Joscha</forename>
                    </persName>
                    <persName key="JS">
                        <surname>Schulz</surname>
                        <forename>Jashty</forename>
                    </persName>
                    <persName key="CS">
                        <surname>Splettsen</surname>
                        <forename>Convin</forename>
                    </persName>
                    <resp>
                        <note type="remarkResponsibility">Preparation support</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"/></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <edition>LTA Edition 1.0</edition>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences and Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName n="2" role="hostingInstitution">Goethe
                        University Frankfurt</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-01-15">2021-01-15</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">11</idno>
                
                <idno type="C1">11</idno><idno type="C2">11</idno>
                <idno type="Time">1049 - 1109 (?)</idno>
                <idno type="VIAF_(Expression)">not available</idno>
                <idno type="VIAF_(Person)">not available</idno>
                <idno type="Year_of_Publication_(interpreted)">1075</idno>
            </publicationStmt>
            <sourceDesc>
                <p><bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 4: 1027–1090. Paris, Imprimerie Nationale, 1888</bibl><bibl type="Volume">4</bibl><bibl type="Column">332</bibl><bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl><bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3188</bibl><bibl type="Reference">(B. h. 411, ccccxiiii.)</bibl></p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p><desc type="Digital_Edition_Statement">The text has been retrieved from the Bernard/Bruel edition and prepared at Goethe
                    University Frankfurt using the Historical Semantics Corpus Management (HSCM) on
                    the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                    edition as it offers only the main reading of the text for analytical purposes.
                    Brackets, hyphenation, and quotation marks have been removed. The Medieval Latin
                    orthography and the pagination of the edition have been preserved.</desc><desc type="Status">automatically lemmatized</desc><desc type="Commentary" resp="TG">The
                        Bernard/Bruel edition contains 301 documents, which the editors could only
                        date to a period between 1048 and 1109 at the latest. For a chronological
                        classification that allows statistical analysis, these documents were
                        distributed evenly over the three quarters of a century between 1050 and
                        1109 according to the order of the documents in the Cluny edition. If more
                        precise chronological allocations can be made on the basis of semantic
                        features, the allocations by quarter of a century are also revised on this
                        basis. (TG)</desc></p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Charter</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange><change type="Annotation_created_by" resp="TG">Tim Geelhaar</change><change type="Annotation_created_on">Mon Jan 25 13:48:27 CET 2021</change><change type="Annotation_last_changed_by" resp="TG"> Tim Geelhaar</change><change type="Annotation_last_changed_on">Mon Jan 25 13:48:27 CET
                    2021</change>
            <change type="version_released_by" resp="TG">Tim Geelhaar</change><change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change></listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div>
                <p><s><w lemma="[{'id':1192980,'ls':[{'id':1192981,'name':'notus','wfs':[{'id':1192990,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'notum'}]}],'name':'notus@NN'}]">Notum</w><c> </c><w lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':244519866,'morph':{'Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'sit'}]}],'name':'sum@V'}]">sit</w><c> </c><w lemma="[{'id':245290,'ls':[{'id':8736,'name':'tam','wfs':[{'id':245291,'morph':{'Pos':'ADV'},'name':'tam'}]}],'name':'tam@ADV'}]">tam</w><c> </c><w lemma="[{'id':2874459,'ls':[{'id':242458017,'name':'presens','wfs':[{'id':244213486,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213439,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213461,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213466,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213434,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'},{'id':244213491,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'presentibus'}]}],'name':'praesens@ADJ'}]">presentibus</w><c> </c><w lemma="[{'id':225505,'ls':[{'id':7683,'name':'quam','wfs':[{'id':225506,'morph':{'Pos':'ADV'},'name':'quam'}]}],'name':'quam@ADV'}]">quam</w><c> </c><w lemma="[{'id':1504494,'ls':[{'id':241597290,'name':'absens','wfs':[{'id':243653404,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'absentibus'},{'id':243653399,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'absentibus'},{'id':243653374,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'absentibus'},{'id':243653352,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'absentibus'},{'id':243653347,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'absentibus'},{'id':243653379,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'absentibus'}]}],'name':'absens@ADJ'}]">absentibus</w><c>,</c><c> </c><w lemma="[{'id':225474,'ls':[{'id':225475,'name':'qualiter','wfs':[{'id':225477,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'qualiter'}]}],'name':'qualiter@ADV'}]">qualiter</w><c> </c><w lemma="[{'id':979873,'ls':[{'id':979891,'name':'domnus','wfs':[{'id':979906,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'domnus'}]}],'name':'dominus@NN'}]">domnus</w><c> </c><w>Sigaldus</w><c>,</c><c> </c><w lemma="[{'id':1272841,'ls':[{'id':543834077,'name':'prepositus','wfs':[{'id':543834090,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'prepositus'}]}],'name':'praepositus@NN'}]">prepositus</w><c> </c><w lemma="[{'id':1481107,'ls':[{'id':1481108,'name':'Cluniacensis','wfs':[{'id':1481142,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensis'},{'id':1481110,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cluniacensis'},{'id':1481143,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensis'},{'id':1481141,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensis'},{'id':241518241,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensis'},{'id':1481120,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'Cluniacensis'},{'id':1481153,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensis'},{'id':1481148,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensis'},{'id':1481149,'morph':{'Casus':'VOCATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensis'},{'id':241518238,'morph':{'Pos':'ADJ'},'name':'Cluniacensis'},{'id':1481147,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'Cluniacensis'}]}],'name':'Cluniacensis@ADJ'}]">Cluniacensis</w><c>,</c><c> </c><w lemma="[{'id':5109084,'ls':[{'id':3001,'name':'do','wfs':[{'id':5109405,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'dedit'}]}],'name':'do@V'}]">dedit</w><c> </c><w lemma="[{'id':1465588,'ls':[{'id':9271,'name':'vinea','wfs':[{'id':1465632,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'vineam'}]}],'name':'vinea@NN'}]">vineam</w><c> </c><w lemma="[{'id':256816,'ls':[{'id':256817,'name':'unus','wfs':[{'id':256839,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NUM'},'name':'unam'}]}],'name':'unus@NUM'}]">unam</w><c> </c><w lemma="[{'id':109453,'ls':[{'id':109454,'name':'ad','wfs':[{'id':109455,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'ad@AP'},{'id':109433,'ls':[{'id':607803638,'name':'ad','wfs':[{'id':607803639,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'a@AP'}]">ad</w><c> </c><w lemma="[{'id':1161055,'ls':[{'id':1161056,'name':'medietas','wfs':[{'id':243392635,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'medietatem'}]}],'name':'medietas@NN'}]">medietatem</w><c> </c><w lemma="[{'id':1257456,'ls':[{'id':7146,'name':'plantatio','wfs':[{'id':1257459,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'plantationis'},{'id':243423672,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'plantationis'}]}],'name':'plantatio@NN'}]">plantationis</w><c> </c><w lemma="[{'id':519812,'ls':[{'id':519813,'name':'Martinus','wfs':[{'id':519823,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Martino'},{'id':519824,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Martino'}]}],'name':'Martinus@NP'}]">Martino</w><c> </c><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w><c> </c><w lemma="[{'id':1444118,'ls':[{'id':9084,'name':'uxor','wfs':[{'id':1444130,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'uxori'},{'id':243491695,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'uxori'}]}],'name':'uxor@NN'}]">uxori</w><c> </c><w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510032,'morph':{'Casus':'GENITIVE','Genus':'COMMON','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ejus'}]}],'name':'is@PRO'}]">ejus</w><c> </c><w>Sufisæ</w><c> </c><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w><c> </c><w lemma="[{'id':256816,'ls':[{'id':256817,'name':'unus','wfs':[{'id':256849,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NUM'},'name':'uni'},{'id':531794822,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NUM'},'name':'uni'},{'id':531794810,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NUM'},'name':'uni'},{'id':256858,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NUM'},'name':'uni'},{'id':256841,'morph':{'Casus':'DATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NUM'},'name':'uni'},{'id':256831,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NUM'},'name':'uni'},{'id':256830,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NUM'},'name':'uni'}]}],'name':'unus@NUM'}]">uni</w><c> </c><w lemma="[{'id':1034174,'ls':[{'id':4372,'name':'filius','wfs':[{'id':243348000,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'filio'},{'id':243347999,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'filio'}]}],'name':'filius@NN'}]">filio</w><c> </c><w lemma="[{'id':1406782,'ls':[{'id':488516594,'name':'suus','wfs':[{'id':488516599,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'suo'},{'id':488516600,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'suo'}]}],'name':'suus@NN'}]">suo</w><c>,</c><c> </c><w lemma="[{'id':1191767,'ls':[{'id':6409,'name':'nomen','wfs':[{'id':243402774,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'nomine'}]}],'name':'nomen@NN'}]">nomine</w><c> </c><w lemma="[{'id':358978,'ls':[{'id':358979,'name':'Constantio','wfs':[{'id':488414660,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Constantio'},{'id':358980,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Constantio'}]}],'name':'Constantio@NP'}]">Constantio</w><c>,</c><c> </c><w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w><c> </c><w lemma="[{'id':1468658,'ls':[{'id':9303,'name':'vita','wfs':[{'id':1468686,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'vita'},{'id':1468691,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'vita'},{'id':1468692,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'vita'}]}],'name':'vita@NN'}]">vita</w><c> </c><w lemma="[{'id':539256847,'ls':[{'id':539256848,'name':'tantum','wfs':[{'id':787138603,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'tantum'},{'id':539256850,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'tantum'}]}],'name':'tantum@NN'}]">tantum</w><c> </c><w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510043,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'eorum'},{'id':242816228,'morph':{'Casus':'GENITIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'eorum'},{'id':644441610,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'eorum'}]}],'name':'is@PRO'}]">eorum</w><c>.</c><c> </c></s><s><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">Et</w><c> </c><w lemma="[{'id':218253,'ls':[{'id':7234,'name':'post','wfs':[{'id':218254,'morph':{'Pos':'ADV'},'name':'post'}]}],'name':'post@ADV'}]">post</w><c> </c><w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510043,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'eorum'},{'id':242816228,'morph':{'Casus':'GENITIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'eorum'},{'id':644441610,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'eorum'}]}],'name':'is@PRO'}]">eorum</w><c> </c><w>descessum</w><c>,</c><c> </c><w lemma="[{'id':595330133,'ls':[{'id':595330592,'name':'alius','wfs':[{'id':741310,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'alii'},{'id':595330602,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'alii'}]}],'name':'alius@NN'}]">alii</w><c> </c><w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510043,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'eorum'},{'id':242816228,'morph':{'Casus':'GENITIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'eorum'},{'id':644441610,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'eorum'}]}],'name':'is@PRO'}]">eorum</w><c> </c><w lemma="[{'id':1226219,'ls':[{'id':6802,'name':'parens','wfs':[{'id':1226226,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'parentes'},{'id':1226234,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'parentes'},{'id':1226224,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'parentes'},{'id':1226225,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'parentes'},{'id':1226233,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'parentes'},{'id':1226220,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'parentes'}]}],'name':'parens@NN'}]">parentes</w><c> </c><w lemma="[{'id':7783838,'ls':[{'id':8779,'name':'teneo','wfs':[{'id':7784141,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'teneant'}]}],'name':'teneo@V'}]">teneant</w><c> </c><w lemma="[{'id':114720,'ls':[{'id':114721,'name':'ad','wfs':[{'id':416013046,'morph':{'Pos':'ADV'},'name':'ad'}]}],'name':'ad@ADV'}]">ad</w><c> </c><w lemma="[{'id':1161055,'ls':[{'id':1161056,'name':'medietas','wfs':[{'id':243392635,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'medietatem'}]}],'name':'medietas@NN'}]">medietatem</w><c>.</c></s></p>
            </div>
        </body>
    </text>
</TEI>
