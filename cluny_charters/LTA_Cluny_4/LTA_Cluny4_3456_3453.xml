<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">Chartes de l'abbaye de Cluny vol. 4: 3453</title>
                <title type="sub">EPISTOLA GREGORII PAPÆ VII AD HUGONEM, ABBATEM CLUNIACENSEM, UT
                    LITTERAS IPSIUS AD MANASSEM, REMENSEM ARCHIEPISCOPUM, MITTAT, ET MONACHOS SANCTI
                    REMIGII TUEATUR.</title>
                <author xml:lang="lat">Diplomata Cluniacensis</author>
                <author xml:lang="lat" xml:id="author-2">Gregorius VII. papa</author>
                <author xml:lang="ger" corresp="#author-2">Gregor VII., Papst</author>
                <editor key="TG" corresp="#LTACorpusEditor"><persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor key="AE" corresp="#LTACorpusEditor"><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor key="FW" corresp="#LTACorpusPublisher"><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.cbma-project.eu/" type="provider">CBMA Project (Corpus de la Bourgogne
                        du Moyen Âge)</orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName key="VK">
                        <surname>Koch</surname>
                        <forename>Vincent</forename>
                    </persName>
                    <persName key="JD">
                        <surname>Doepp</surname>
                        <forename>Joscha</forename>
                    </persName>
                    <persName key="JS">
                        <surname>Schulz</surname>
                        <forename>Jashty</forename>
                    </persName>
                    <persName key="CS">
                        <surname>Splettsen</surname>
                        <forename>Convin</forename>
                    </persName>
                    <resp>
                        <note type="remarkResponsibility">Preparation support</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"/></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <edition>LTA Edition 1.0</edition>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences and Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName n="2" role="hostingInstitution">Goethe
                        University Frankfurt</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-01-15">2021-01-15</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">11</idno>
                
                <idno type="C1">11</idno><idno type="C2">11</idno>
                <idno type="Time">1073.06.30</idno>
                <idno type="VIAF_(Expression)">not available</idno>
                <idno type="VIAF_(Person)" corresp="#author-2">95301181</idno>
                <idno type="Year_of_Publication_(interpreted)">1073</idno>
            </publicationStmt>
            <sourceDesc>
                <p><bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 4: 1027–1090. Paris, Imprimerie Nationale, 1888</bibl><bibl type="Volume">4</bibl><bibl type="Column">562</bibl><bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl><bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3453</bibl><bibl type="Reference">(Labbe, Concil., t. X, p. 16; Hardouin, Concil., t. VI, 1re
                        part., col. 1206; Mansi, Concil., t. XX, p. 71.)</bibl></p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p><desc type="Digital_Edition_Statement">The text has been retrieved from the Bernard/Bruel edition and prepared at Goethe
                    University Frankfurt using the Historical Semantics Corpus Management (HSCM) on
                    the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                    edition as it offers only the main reading of the text for analytical purposes.
                    Brackets, hyphenation, and quotation marks have been removed. The Medieval Latin
                    orthography and the pagination of the edition have been preserved.</desc><desc type="Status">automatically lemmatized</desc><desc type="Commentary" resp="TG">dated
                        according to the edition (TG)</desc></p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Letter (papal)</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Letters</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange><change type="Annotation_created_by" resp="TG">Tim Geelhaar</change><change type="Annotation_created_on">Mon Jan 25 16:34:44 CET 2021</change><change type="Annotation_last_changed_by" resp="TG"> Tim Geelhaar</change><change type="Annotation_last_changed_on">Mon Jan 25 16:34:44 CET
                    2021</change>
            <change type="version_released_by" resp="TG">Tim Geelhaar</change><change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change></listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div>
                <p><s><w lemma="[{'id':437870,'ls':[{'id':437871,'name':'Gregorius','wfs':[{'id':641801,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Gregorius'}]}],'name':'Gregorius@NP'}]">Gregorius</w><c>,</c><c> </c><w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w><c>.</c><c> </c></s><s><w lemma="[{'id':6327887,'ls':[{'id':6419,'name':'nosco','wfs':[{'id':6328700,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'noverit'},{'id':6328707,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'noverit'}]}],'name':'nosco@V'}]">Noverit</w><c> </c><w lemma="[{'id':1340677,'ls':[{'id':8096,'name':'sanctitas','wfs':[{'id':243454947,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'sanctitas'},{'id':1340690,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'sanctitas'}]}],'name':'sanctitas@NN'}]">sanctitas</w><c> </c><c>.</c><c>.</c><c>.</c><note><s><w lemma="[{'id':243116350,'ls':[{'id':243116351,'name':'NON_WORD','wfs':[{'id':244750471,'morph':{'Pos':'XY'},'name':'The'}]}],'name':'NON_WORD@XY'}]">The</w><c> </c><w>document</w><c> </c><w lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':657961675,'morph':{'Casus':'ABLATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'is'},{'id':244510104,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'is'}]}],'name':'is@PRO'}]">is</w><c> </c><w lemma="[{'id':109433,'ls':[{'id':109434,'name':'a','wfs':[{'id':109435,'morph':{'Pos':'AP'},'name':'a'}]}],'name':'a@AP'}]">a</w><c> </c><w>letter</w><c> </c><w>and</w><c> </c><w>therefore</w><c> </c><w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':245547189,'morph':{'Pos':'FM'},'name':'not'}]}],'name':'NON_LATIN@FM'}]">not</w><c> </c><w>integrated</w><c> </c><w>into</w><c> </c><w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':256721224,'morph':{'Pos':'FM'},'name':'the'}]}],'name':'NON_LATIN@FM'}]">the</w><c> </c><w lemma="[{'id':986187,'ls':[{'id':3784,'name':'editio','wfs':[{'id':986192,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'edition'}]}],'name':'editio@NN'}]">edition</w><c>,</c><c> </c><w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':256721224,'morph':{'Pos':'FM'},'name':'the'}]}],'name':'NON_LATIN@FM'}]">the</w><c> </c><w>letter</w><c> </c><w>itself</w><c> </c><w lemma="[{'id':1115979,'ls':[{'id':1115980,'name':'ion','wfs':[{'id':1115983,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'is'},{'id':1115982,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'is'}]}],'name':'ion@NN'}]">is</w><c> </c><w>edited</w><c> </c><w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w><c> </c></s><ref target="http://www.mgh.de/dmgh/resolving/MGH_Epp._sel._2,1_S._22"><w>MGH</w><c> </c><w>Epp</w><c>.</c><c> </c><w>sel</w><c>.</c><c> </c><w>2,1</w><c> </c><w>p</w><c>.</c><c> </c><w>22</w></ref></note></s></p>
            </div>
        </body>
    </text>
</TEI>
