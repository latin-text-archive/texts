<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">Chartes de l'abbaye de Cluny vol. 4: 3253</title>
                <title type="sub">RADVIZ DAT MONASTERIO CLUNIACENSI ANCILLAM NOMINE TEDRADAM CUM
                    INFANTIBUS.</title>
                <author xml:lang="lat">Diplomata Cluniacensis</author>
                <editor key="TG" corresp="#LTACorpusEditor"><persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor key="AE" corresp="#LTACorpusEditor"><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor key="FW" corresp="#LTACorpusPublisher"><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.cbma-project.eu/" type="provider">CBMA Project (Corpus de la Bourgogne
                        du Moyen Âge)</orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName key="VK">
                        <surname>Koch</surname>
                        <forename>Vincent</forename>
                    </persName>
                    <persName key="JD">
                        <surname>Doepp</surname>
                        <forename>Joscha</forename>
                    </persName>
                    <persName key="JS">
                        <surname>Schulz</surname>
                        <forename>Jashty</forename>
                    </persName>
                    <persName key="CS">
                        <surname>Splettsen</surname>
                        <forename>Convin</forename>
                    </persName>
                    <resp>
                        <note type="remarkResponsibility">Preparation support</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"/></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <edition>LTA Edition 1.0</edition>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences and Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName n="2" role="hostingInstitution">Goethe
                        University Frankfurt</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-01-15">2021-01-15</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">11</idno>
                
                <idno type="C1">11</idno><idno type="C2">11</idno>
                <idno type="Time">1049 - 1109 (?)</idno>
                <idno type="VIAF_(Expression)">not available</idno>
                <idno type="VIAF_(Person)">not available</idno>
                <idno type="Year_of_Publication_(interpreted)">1100</idno>
            </publicationStmt>
            <sourceDesc>
                <p><bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 4: 1027–1090. Paris, Imprimerie Nationale, 1888</bibl><bibl type="Volume">4</bibl><bibl type="Column">370</bibl><bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl><bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3253</bibl><bibl type="Reference">(B. h. 554, dlvii.)</bibl></p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p><desc type="Digital_Edition_Statement">The text has been retrieved from the Bernard/Bruel edition and prepared at Goethe
                    University Frankfurt using the Historical Semantics Corpus Management (HSCM) on
                    the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                    edition as it offers only the main reading of the text for analytical purposes.
                    Brackets, hyphenation, and quotation marks have been removed. The Medieval Latin
                    orthography and the pagination of the edition have been preserved.</desc><desc type="Status">automatically lemmatized</desc><desc type="Commentary" resp="TG">The
                        Bernard/Bruel edition contains 301 documents, which the editors could only
                        date to a period between 1048 and 1109 at the latest. For a chronological
                        classification that allows statistical analysis, these documents were
                        distributed evenly over the three quarters of a century between 1050 and
                        1109 according to the order of the documents in the Cluny edition. If more
                        precise chronological allocations can be made on the basis of semantic
                        features, the allocations by quarter of a century are also revised on this
                        basis. (TG)</desc></p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Charter</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange><change type="Annotation_created_by" resp="TG">Tim Geelhaar</change><change type="Annotation_created_on">Mon Jan 25 13:43:56 CET 2021</change><change type="Annotation_last_changed_by" resp="TG"> Tim Geelhaar</change><change type="Annotation_last_changed_on">Mon Jan 25 13:43:56 CET
                    2021</change>
            <change type="version_released_by" resp="TG">Tim Geelhaar</change><change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change></listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div>
                <p><s><w lemma="[{'id':1337038,'ls':[{'id':1337039,'name':'sacrum','wfs':[{'id':1337049,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'sacro'},{'id':1337050,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'sacro'}]}],'name':'sacrum@NN'}]">Sacro</w><c> </c><w lemma="[{'id':1143697,'ls':[{'id':5796,'name':'locus','wfs':[{'id':1143711,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'loco'},{'id':1143710,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'loco'}]}],'name':'locus@NN'}]">loco</w><c> </c><w lemma="[{'id':356971,'ls':[{'id':356972,'name':'Cluniacum','wfs':[{'id':356984,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NE'},'name':'Cluniaco'},{'id':356983,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NE'},'name':'Cluniaco'}]}],'name':'Cluniacum@NE'}]">Cluniaco</w><c>,</c><c> </c><w lemma="[{'id':242815965,'ls':[{'id':242815966,'name':'ego','wfs':[{'id':244509626,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509632,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509629,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'}]}],'name':'ego@PRO'}]">ego</w><c>,</c><c> </c><w lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]">in</w><c> </c><w lemma="[{'id':958093,'ls':[{'id':958094,'name':'deus','wfs':[{'id':958107,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'dei'},{'id':958100,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'dei'},{'id':958101,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'dei'}]}],'name':'deus@NN'}]">Dei</w><c> </c><w lemma="[{'id':1191767,'ls':[{'id':6409,'name':'nomen','wfs':[{'id':243402774,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'nomine'}]}],'name':'nomen@NN'}]">nomine</w><c>,</c><c> </c><w>Radviz</w><c> </c><w lemma="[{'id':980362,'ls':[{'id':3708,'name':'donum','wfs':[{'id':980372,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'dono'},{'id':980373,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'dono'}]}],'name':'donum@NN'}]">dono</w><c> </c><w lemma="[{'id':256816,'ls':[{'id':256817,'name':'unus','wfs':[{'id':256839,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NUM'},'name':'unam'}]}],'name':'unus@NUM'}]">unam</w><c> </c><w lemma="[{'id':755344,'ls':[{'id':764,'name':'ancilla','wfs':[{'id':755354,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ancillam'}]}],'name':'ancilla@NN'}]">ancillam</w><c>,</c><c> </c><w lemma="[{'id':1191767,'ls':[{'id':6409,'name':'nomen','wfs':[{'id':243402774,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'nomine'}]}],'name':'nomen@NN'}]">nomine</w><c> </c><w>Tedradam</w><c>,</c><c> </c><w lemma="[{'id':218253,'ls':[{'id':7234,'name':'post','wfs':[{'id':218254,'morph':{'Pos':'ADV'},'name':'post'}]}],'name':'post@ADV'}]">post</w><c> </c><w lemma="[{'id':1175053,'ls':[{'id':6225,'name':'mors','wfs':[{'id':242825976,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'mortem'}]}],'name':'mors@NN'}]">mortem</w><c> </c><w lemma="[{'id':244510189,'ls':[{'id':244510190,'name':'meus','wfs':[{'id':244510191,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'meam'}]}],'name':'meus@PRO'}]">meam</w><c> </c><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w><c> </c><w lemma="[{'id':242816114,'ls':[{'id':242816115,'name':'ille','wfs':[{'id':242816139,'morph':{'Casus':'ACCUSATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illos'}]}],'name':'ille@PRO'}]">illos</w><c> </c><w lemma="[{'id':1094353,'ls':[{'id':5187,'name':'infans','wfs':[{'id':1094361,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'infantes'},{'id':1094354,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'infantes'},{'id':1094370,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'infantes'},{'id':1094362,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'infantes'},{'id':1094371,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'infantes'},{'id':1094363,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'infantes'}]}],'name':'infans@NN'}]">infantes</w><c> </c><w lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':244511016,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382851706,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':382871756,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'},{'id':594773492,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]">qui</w><c> </c><w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w><c> </c><w>Arlaldo</w><c>,</c><c> </c><w lemma="[{'id':1358723,'ls':[{'id':8286,'name':'servus','wfs':[{'id':1358784,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'servo'},{'id':1358783,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'servo'}]}],'name':'servus@NN'}]">servo</w><c> </c><w lemma="[{'id':592238,'ls':[{'id':592239,'name':'Sanctus','wfs':[{'id':488483804,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancti'},{'id':592240,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Sancti'},{'id':592245,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Sancti'},{'id':592252,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Sancti'},{'id':592246,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'PLURAL','Pos':'NP'},'name':'Sancti'}]}],'name':'Sanctus@NP'}]">Sancti</w><c> </c><w lemma="[{'id':558713,'ls':[{'id':558714,'name':'Petrus','wfs':[{'id':558720,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NP'},'name':'Petri'},{'id':689091,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Petri'},{'id':558721,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NP'},'name':'Petri'}]}],'name':'Petrus@NP'}]">Petri</w><c>,</c><c> </c><w lemma="[{'id':6302962,'ls':[{'id':6320,'name':'nascor','wfs':[{'id':244478935,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PLUPERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478932,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PLUPERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478938,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478970,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'PLUPERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478911,'morph':{'Casus':'GENITIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'SINGULAR','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478941,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478973,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'PLUPERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244479010,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PLUPERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478979,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478944,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478976,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478982,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478918,'morph':{'Casus':'GENITIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'SINGULAR','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244479013,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PLUPERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244479019,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244479016,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244479022,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'},{'id':244478895,'morph':{'Casus':'NOMINATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PERFECT','VerbType':'DEPONENT','Voice':'PASSIVE'},'name':'nati'}]}],'name':'nascor@V'}]">nati</w><c> </c><w lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':244519838,'morph':{'Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'fuerunt'}]}],'name':'sum@V'}]">fuerunt</w><c>.</c><c> </c></s><s><w lemma="[{'id':243116350,'ls':[{'id':243116351,'name':'NON_WORD','wfs':[{'id':244750562,'morph':{'Pos':'XY'},'name':'S'},{'id':245538992,'morph':{'Pos':'XY'},'name':'s'}]}],'name':'NON_WORD@XY'}]">S</w><c>.</c><c> </c><w>Radviz</w><c>.</c><c> </c></s><s><w lemma="[{'id':243116350,'ls':[{'id':243116351,'name':'NON_WORD','wfs':[{'id':244750562,'morph':{'Pos':'XY'},'name':'S'},{'id':245538992,'morph':{'Pos':'XY'},'name':'s'}]}],'name':'NON_WORD@XY'}]">S</w><c>.</c><c> </c><w lemma="[{'id':244494563,'ls':[{'id':244494560,'name':'ProperName','wfs':[{'id':594911991,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NE'},'name':'Drogo'}]}],'name':'ProperName@NE'},{'id':377100,'ls':[{'id':377101,'name':'Drogo','wfs':[{'id':615528,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Drogo'},{'id':241362779,'morph':{'Casus':'NOMINATIVE','Numerus':'SINGULAR','Pos':'NP'},'name':'Drogo'},{'id':377102,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Drogo'},{'id':241362775,'morph':{'Pos':'NP'},'name':'Drogo'},{'id':488418881,'morph':{'Casus':'VOCATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Drogo'}]},{'id':488418882,'name':'Drogus','wfs':[{'id':488418885,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Drogo'},{'id':488418886,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Drogo'}]}],'name':'Drogo@NP'},{'id':377122,'ls':[{'id':377123,'name':'Drogus','wfs':[{'id':377135,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Drogo'},{'id':377134,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Drogo'}]}],'name':'Drogus@NP'}]">Drogo</w><c>.</c><c> </c></s><s><w lemma="[{'id':243116350,'ls':[{'id':243116351,'name':'NON_WORD','wfs':[{'id':244750562,'morph':{'Pos':'XY'},'name':'S'},{'id':245538992,'morph':{'Pos':'XY'},'name':'s'}]}],'name':'NON_WORD@XY'}]">S</w><c>.</c><c> </c><w>Ildinus</w><c>.</c></s></p>
            </div>
        </body>
    </text>
</TEI>
