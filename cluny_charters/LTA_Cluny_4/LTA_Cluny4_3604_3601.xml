<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">Chartes de l'abbaye de Cluny vol. 4: 3601</title>
                <title type="sub">CHARTA QUA ADEMARIUS ET ARTMANNUS VICECOMITES DANT MONACHIS
                    MOISIACENSIBUS ET CLUNIACENSIBUS MONASTERIUM QUOD CONSTRUERE DISPONUNT SUBTUS
                    BRUNECHILDUM CASTRUM.</title>
                <author xml:lang="lat">Diplomata Cluniacensis</author>
                <editor key="TG" corresp="#LTACorpusEditor"><persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor key="AE" corresp="#LTACorpusEditor"><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor key="FW" corresp="#LTACorpusPublisher"><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.cbma-project.eu/" type="provider">CBMA Project (Corpus de la Bourgogne
                        du Moyen Âge)</orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName key="VK">
                        <surname>Koch</surname>
                        <forename>Vincent</forename>
                    </persName>
                    <persName key="JD">
                        <surname>Doepp</surname>
                        <forename>Joscha</forename>
                    </persName>
                    <persName key="JS">
                        <surname>Schulz</surname>
                        <forename>Jashty</forename>
                    </persName>
                    <persName key="CS">
                        <surname>Splettsen</surname>
                        <forename>Convin</forename>
                    </persName>
                    <resp>
                        <note type="remarkResponsibility">Preparation support</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de"> Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW) </orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"/></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <edition>LTA Edition 1.0</edition>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences and Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName n="2" role="hostingInstitution">Goethe
                        University Frankfurt</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-01-15">2021-01-15</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">11</idno>
                
                <idno type="C1">11</idno><idno type="C2">11</idno>
                <idno type="Time">1083.09.05</idno>
                <idno type="VIAF_(Expression)">not available</idno>
                <idno type="VIAF_(Person)">not available</idno>
                <idno type="Year_of_Publication_(interpreted)">1083</idno>
            </publicationStmt>
            <sourceDesc>
                <p><bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 4: 1027–1090. Paris, Imprimerie Nationale, 1888</bibl><bibl type="Volume">4</bibl><bibl type="Column">760</bibl><bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151419</bibl><bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=3601</bibl><bibl type="Reference">(Gallia christ., t. I, pr. p. 39)</bibl></p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p><desc type="Digital_Edition_Statement">The text has been retrieved from the Bernard/Bruel edition and prepared at Goethe
                    University Frankfurt using the Historical Semantics Corpus Management (HSCM) on
                    the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                    edition as it offers only the main reading of the text for analytical purposes.
                    Brackets, hyphenation, and quotation marks have been removed. The Medieval Latin
                    orthography and the pagination of the edition have been preserved.</desc><desc type="Status">automatically lemmatized</desc><desc type="Commentary" resp="TG">dated
                        according to the edition (TG)</desc></p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Charter</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange><change type="Annotation_created_by" resp="TG">Tim Geelhaar</change><change type="Annotation_created_on">Mon Jan 25 13:57:55 CET 2021</change><change type="Annotation_last_changed_by" resp="TG"> Tim Geelhaar</change><change type="Annotation_last_changed_on">Mon Jan 25 13:57:55 CET
                    2021</change>
            <change type="version_released_by" resp="TG">Tim Geelhaar</change><change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change></listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div>
                <p><s><w lemma="[{'id':1137961,'ls':[{'id':5724,'name':'lex','wfs':[{'id':1137965,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'legibus'},{'id':1137963,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'legibus'}]}],'name':'lex@NN'}]">Legibus</w><c> </c><w lemma="[{'id':5925227,'ls':[{'id':5356,'name':'instruo','wfs':[{'id':5925498,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PLUPERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925433,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925337,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PLUPERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925278,'morph':{'Casus':'NOMINATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925502,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PLUPERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925523,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925360,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'FUTURE_PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925425,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925332,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PLUPERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925429,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925301,'morph':{'Casus':'GENITIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'SINGULAR','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925515,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925355,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925294,'morph':{'Casus':'GENITIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'SINGULAR','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925519,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925411,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'PLUPERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925350,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'},{'id':5925415,'morph':{'ConjugationType':'THIRD_CONJUGATION','Genus':'MASCULINE','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_2','Pos':'V','Tense':'PLUPERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'instructi'}]}],'name':'instruo@V'}]">instructi</w><c> </c><w lemma="[{'id':2213722,'ls':[{'id':3666,'name':'divinus','wfs':[{'id':2213920,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'divinis'},{'id':2213882,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'divinis'},{'id':2213898,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'divinis'},{'id':2213901,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'divinis'},{'id':2213917,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'divinis'},{'id':2213879,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'ADJ'},'name':'divinis'}]}],'name':'divinus@ADJ'}]">divinis</w><c>,</c><c> </c><w lemma="[{'id':242815993,'ls':[{'id':242815994,'name':'et cetera','wfs':[{'id':242815995,'morph':{'Pos':'PTC'},'name':'etc'}]}],'name':'et cetera@PTC'}]">etc</w><c>.</c><note><s><c>(</c><w lemma="[{'id':241366281,'ls':[{'id':241366282,'name':'Gallia','wfs':[{'id':241366290,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'Gallia'},{'id':241366283,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'Gallia'}]}],'name':'Gallia@NN'}]">Gallia</w><c> </c><w lemma="[{'id':352906,'ls':[{'id':244512690,'name':'Christus','wfs':[{'id':244512692,'morph':{'Pos':'NP'},'name':'christ'}]}],'name':'Christus@NP'}]">christ</w><c>.</c><c>,</c><c> </c><w>t</w><c>.</c><c> </c><w lemma="[{'id':244494563,'ls':[{'id':244494560,'name':'ProperName','wfs':[{'id':612295207,'morph':{'Pos':'NE'},'name':'I'}]}],'name':'ProperName@NE'}]">I</w><c>,</c><c> </c><w lemma="[{'id':218544,'ls':[{'id':218545,'name':'pr','wfs':[{'id':218546,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'pr'}]}],'name':'pr@ADV'}]">pr</w><c>.</c><c> </c></s><s><w lemma="[{'id':1294819,'ls':[{'id':743418636,'name':'p','wfs':[{'id':743418639,'morph':{'Casus':'ACCUSATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418637,'morph':{'Casus':'ABLATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418643,'morph':{'Casus':'DATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418642,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'},{'id':743418641,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'p'}]}],'name':'puer@NN'}]">p</w><c>.</c><c> </c><w>39</w><c>,</c><c> </c><w lemma="[{'id':109565,'ls':[{'id':109566,'name':'ex','wfs':[{'id':109567,'morph':{'Pos':'AP'},'name':'ex'}]}],'name':'ex@AP'}]">ex</w><c> </c><w>archiv</w><c>.</c><c> </c></s><s><w>Moisiac</w><c>.</c><c>)</c></s></note><note><s><w>Text</w><c> </c><w lemma="[{'id':244954026,'ls':[{'id':244954027,'name':'NON_LATIN','wfs':[{'id':245547189,'morph':{'Pos':'FM'},'name':'not'}]}],'name':'NON_LATIN@FM'}]">not</w><c> </c><w>found</w><c> </c><c>(</c><w>TG</w><c>)</c></s></note></s></p>
            </div>
        </body>
    </text>
</TEI>
