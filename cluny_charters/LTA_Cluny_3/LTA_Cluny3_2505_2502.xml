<?xml version="1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">Chartes de l'abbaye de Cluny vol. 3: 2502</title>
                <title type="sub">CHARTA QUA HUGO BOCHARDUS ET ALII WERPITIONEM FACIUNT SERVORUM
                    MONASTERIO CLUNIACENSI.</title>
                <author xml:lang="lat">Diplomata Cluniacensis</author>
                <editor key="TG" corresp="#LTACorpusEditor"><persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor key="AE" corresp="#LTACorpusEditor"><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor key="FW" corresp="#LTACorpusPublisher"><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.cbma-project.eu/" type="provider">CBMA Project (Corpus de la Bourgogne
                        du Moyen Âge)</orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName key="VK">
                        <surname>Koch</surname>
                        <forename>Vincent</forename>
                    </persName>
                    <persName key="JD">
                        <surname>Doepp</surname>
                        <forename>Joscha</forename>
                    </persName>
                    <persName key="JS">
                        <surname>Schulz</surname>
                        <forename>Jashty</forename>
                    </persName>
                    <persName key="CS">
                        <surname>Splettsen</surname>
                        <forename>Convin</forename>
                    </persName>
                    <resp>
                        <note type="remarkResponsibility">Preparation support</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de">Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW)</orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"/></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <edition>LTA Edition 1.0</edition>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution">Berlin-Brandenburg Academy of Sciences and Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName n="2" role="hostingInstitution">Goethe
                        University Frankfurt</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-01-15">2021-01-15</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">11</idno>
                
                <idno type="C1">11</idno><idno type="C2">10</idno>
                <idno type="Time">1000 ca.</idno>
                <idno type="VIAF_(Expression)">not available</idno>
                <idno type="VIAF_(Person)">not available</idno>
                <idno type="Year_of_Publication_(interpreted)">1000</idno>
            </publicationStmt>
            <sourceDesc>
                <p><bibl type="Edition">Recueil des chartes de l'abbaye de Cluny, ed. Auguste Bernard/Alexandre Bruel, t. 3: 987–1027. Paris, Imprimerie Nationale, 1884</bibl><bibl type="Volume">3</bibl><bibl type="Column">580</bibl><bibl type="Bibliographical_Link">https://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=140151400</bibl><bibl type="URL">https://www.uni-muenster.de/Fruehmittelalter/Projekte/Cluny/CCE/php/view.php?bb=2502</bibl><bibl type="Reference">(Bibl. nat. lat. nouv. acq. 2154, nº 77; cop. 16-236-2º; B.
                        h. 515, dxviii.)</bibl></p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p><desc type="Digital_Edition_Statement">The text has been retrieved from the Bernard/Bruel edition and prepared at Goethe
                    University Frankfurt using the Historical Semantics Corpus Management (HSCM) on
                    the <ref target="https://www.texttechnologylab.org/applications/ehumanities-desktop/">eHumanities Desktop</ref>. This edition does not replace the printed
                    edition as it offers only the main reading of the text for analytical purposes.
                    Brackets, hyphenation, and quotation marks have been removed. The Medieval Latin
                    orthography and the pagination of the edition have been preserved.</desc><desc type="Status">automatically lemmatized</desc><desc type="Commentary" resp="TG">dated
                        according to the edition (TG)</desc></p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Charter</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Cluny Charters</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange>
                <change type="Annotation_created_by" resp="TG"> Tim Geelhaar</change><change type="Annotation_created_on">Wed Jan 20 22:04:26 CET 2021</change><change type="Annotation_last_changed_by" resp="TG"> Tim Geelhaar</change><change type="Annotation_last_changed_on">Wed Jan 20 22:04:26 CET
                    2021</change>
            <change type="version_released_by" resp="TG">Tim Geelhaar</change><change type="version_released_on" when="2021-02-15T15:51:00">Mon Feb 15 15:51:00 CET 2021</change></listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div>
                <p><s><w lemma="[{'id':242815965,'ls':[{'id':242815966,'name':'ego','wfs':[{'id':244509626,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509632,'morph':{'Casus':'NOMINATIVE','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'},{'id':244509629,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'PERSONAL'},'name':'ego'}]}],'name':'ego@PRO'}]">Ego</w><c> </c><w lemma="[{'id':473020,'ls':[{'id':569771579,'name':'Huguo','wfs':[{'id':569771580,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Huguo'}]}],'name':'Hugo@NP'}]">Huguo</w><c> </c><w>Bochardus</w><c> </c><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w><c> </c><w lemma="[{'id':473020,'ls':[{'id':569771579,'name':'Huguo','wfs':[{'id':569771580,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Huguo'}]}],'name':'Hugo@NP'}]">Huguo</w><c> </c><w lemma="[{'id':1145278,'ls':[{'id':5820,'name':'lotos','wfs':[{'id':1145300,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'lotos'},{'id':1145308,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'lotos'},{'id':1145303,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'lotos'},{'id':1145295,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'lotos'},{'id':1145294,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'COMMON','Numerus':'SINGULAR','Pos':'NN'},'name':'lotos'}]}],'name':'lotos@NN'}]">Lotos</w><c> </c><w lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]">et</w><c> </c><w lemma="[{'id':245550014,'ls':[{'id':245550015,'name':'Girbaldus','wfs':[{'id':245550016,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Girbaldus'}]}],'name':'Girbaldus@NP'}]">Girbaldus</w><c> </c><w>Leverz</w><c> </c><w lemma="[{'id':109722,'ls':[{'id':109723,'name':'ac','wfs':[{'id':109724,'morph':{'Pos':'CON'},'name':'ac'}]}],'name':'ac@CON'}]">ac</w><c> </c><w lemma="[{'id':473020,'ls':[{'id':569771579,'name':'Huguo','wfs':[{'id':569771580,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Huguo'}]}],'name':'Hugo@NP'}]">Huguo</w><c> </c><pb n="581"/><c> </c><w lemma="[{'id':529740,'ls':[{'id':529741,'name':'Morellus','wfs':[{'id':529756,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Morellus'},{'id':488467147,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Morellus'}]}],'name':'Morellus@NP'}]">Morellus</w><c>,</c><c> </c><w lemma="[{'id':5475468,'ls':[{'id':4230,'name':'facio','wfs':[{'id':5475583,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'facimus'}]}],'name':'facio@V'}]">facimus</w><c> </c><w>vuerpicionem</w><c> </c><w lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]">de</w><c> </c><w lemma="[{'id':1358723,'ls':[{'id':8286,'name':'servus','wfs':[{'id':1358776,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'servis'},{'id':1358775,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'servis'}]}],'name':'servus@NN'}]">servis</w><c>,</c><c> </c><w lemma="[{'id':253356,'ls':[{'id':9254,'name':'videlicet','wfs':[{'id':253359,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'videlicet'}]}],'name':'videlicet@ADV'}]">videlicet</w><c> </c><w lemma="[{'id':279508,'ls':[{'id':279509,'name':'Aimo','wfs':[{'id':488391207,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Aimoni'}]}],'name':'Aimo@NP'}]">Aimoni</w><c>,</c><c> </c><w lemma="[{'id':1034174,'ls':[{'id':4372,'name':'filius','wfs':[{'id':297486299,'morph':{'Casus':'VOCATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'filii'},{'id':1034175,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'filii'},{'id':252932508,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'filii'}]}],'name':'filius@NN'}]">filii</w><c> </c><w>Magimberti</w><c>,</c><c> </c><w lemma="[{'id':109541,'ls':[{'id':109542,'name':'cum','wfs':[{'id':109543,'morph':{'Pos':'AP'},'name':'cum'}]}],'name':'cum@AP'}]">cum</w><c> </c><w lemma="[{'id':1040499,'ls':[{'id':4492,'name':'frater','wfs':[{'id':290588,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'},{'id':241435784,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'fratribus'}]}],'name':'frater@NN'}]">fratribus</w><c> </c><w lemma="[{'id':1406782,'ls':[{'id':488516594,'name':'suus','wfs':[{'id':488516607,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'suis'},{'id':488516608,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'suis'}]}],'name':'suus@NN'}]">suis</w><c>.</c><c> </c></s></p>
            </div>
        </body>
        <back>
            <div>
                <note type="Dos"><s><w lemma="[{'id':849184,'ls':[{'id':835608,'name':'carta','wfs':[{'id':835621,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'carta'},{'id':835617,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'carta'},{'id':835622,'morph':{'Casus':'VOCATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'carta'}]}],'name':'charta@NN'}]">Carta</w><c> </c><w lemma="[{'id':496497730,'ls':[{'id':496497731,'name':'Archimbaldus','wfs':[{'id':496497732,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Archimbaldi'}]}],'name':'Archimbaldus@NP'}]">Archimbaldi</w><c>.</c><c>.</c><c>.</c><c> </c></s><s><w>Vigoseto</w><c>.</c></s></note>
            </div>
        </back>
    </text>
</TEI>
