<?xml version="1.0" encoding="UTF-8"?><?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">MGH Capitularia 2: 208. Hludowici II.Capitulum italicum originis
                    incertae</title>
                <author xml:lang="lat">MGH Capitularia</author>
                <author xml:lang="ger">MGH Capitularia</author>
                <editor corresp="#LTACorpusEditor" key="TG"><persName
                        ref="http://viaf.org/viaf/185313129"
                            ><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor corresp="#LTACorpusEditor" key="AE"
                            ><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor corresp="#LTACorpusPublisher" key="FW"
                            ><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.mgh.de" type="provider">
                        <orgName>Monumenta Germaniae Historica (MGH)</orgName>
                        <orgName>Bavarian State Library (BSB), München</orgName></orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"
                            ><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"
                            ><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the<ref
                                target="https://www.texttechnologylab.org/applications/ehumanities-desktop/"
                                >eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de">Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW)</orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"></ref></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <p>LTA Edition 1.0</p>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution"
                        xml:lang="eng">Berlin-Brandenburg Academy of Sciences and
                        Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address
                        n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-05-20">2021-05-20</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">09</idno>
                <idno type="C1">09</idno>
                <idno type="C2">09</idno>
                <idno type="Time">844 - 850 (?)</idno>
                <idno type="VIAF_(Expression)">182997976</idno>
                <idno type="VIAF_(Expression)_Link">http://viaf.org/viaf/182997976</idno>
                <idno type="VIAF_(Person)">not available</idno>
                <idno type="VIAF_(Person)_Link">not available</idno>
                <idno type="Year_of_Publication_(interpreted)">0850</idno>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Capitvlaria regvm Francorvm 2</title>
                        <editor ref="http://viaf.org/viaf/2606355"><forename>Alfred</forename>
                            <surname>Boretius</surname></editor>
                    </titleStmt>
                    <publicationStmt>
                        <publisher>Hahn</publisher>
                        <pubPlace>Hannover</pubPlace>
                        <date>1897</date>
                    </publicationStmt>
                    <seriesStmt>
                        <title> MGH Capit.</title>
                        <biblScope unit="volume">2</biblScope>
                    </seriesStmt>
                </biblFull>
                <bibl type="Edition">MGH Capitularia 2</bibl>
                <bibl type="Volume">Capit. 2</bibl>
                <bibl type="Column">78</bibl>
                <bibl type="Bibliographical_Link"
                    >http://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=214328406</bibl>
                <bibl type="URL">http://www.mgh.de/dmgh/resolving/MGH_Capit._2_S._78</bibl>
                <bibl type="Capitularia_Edition">link not available yet</bibl>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p><desc type="Digital_Edition_Statement">The text has been retrieved from the
                        critical MGH edition and prepared by the <ref
                            target="https://capitularia.uni-koeln.de/">Capitularia Project</ref> at
                        Cologne University and by the CompHistSem workgroup at Goethe University
                        Frankfurt using the Historical Semantics Corpus Management (HSCM) on the
                            <ref
                            target="https://www.texttechnologylab.org/applications/ehumanities-desktop/"
                            >eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc><desc type="Status">controlled manually</desc><desc
                        type="Commentary">dated according to the edition (TG)</desc></p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Capitulary</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Frankish Capitularies</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange>
                <change type="Annotation_based_on">Edition</change>
                <change type="Annotation_created_by">Tim Geelhaar</change>
                <change type="Annotation_created_on">2016-03-10</change>
                <change type="Annotation_last_changed_by">Tim Geelhaar</change>
                <change type="Annotation_last_changed_on">Wed May 19 19:23:20 CEST 2021</change>
            </listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head n="1">
                    <s>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >208</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':674691,'ls':[{'id':488462163,'name':'Hludowicus','wfs':[{'id':657546,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Hludowici'}]}],'name':'Ludovicus@NP'}]"
                            >HLUDOWICI</w>
                        <c> </c>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >II</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':831740,'ls':[{'id':1588,'name':'capitulum','wfs':[{'id':831753,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'capitulum'}]}],'name':'capitulum@NN'}]"
                            >CAPITULUM</w>
                        <c> </c>
                        <w
                            lemma="[{'id':485884,'ls':[{'id':485885,'name':'Italicus','wfs':[{'id':485894,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Italicum'}]}],'name':'Italicus@NP'}]"
                            >ITALICUM</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1216755,'ls':[{'id':6700,'name':'origo','wfs':[{'id':1216758,'morph':{'Casus':'GENITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'originis'}]}],'name':'origo@NN'}]"
                            >ORIGINIS</w>
                        <c> </c>
                        <w
                            lemma="[{'id':2457340,'ls':[{'id':5060,'name':'incertus','wfs':[{'id':2457417,'morph':{'Casus':'GENITIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'incertae'}]}],'name':'incertus@ADJ'}]"
                            >INCERTAE</w>
                        <c>.</c>
                        <c> </c>
                    </s>
                    <s>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >844</w>
                        <c> </c>
                        <c>–</c>
                        <c> </c>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >850</w>
                        <c>?</c>
                    </s>
                </head>
                <p>
                    <s>
                        <w
                            lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]"
                            >De</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242815996,'ls':[{'id':242815997,'name':'hic','wfs':[{'id':36187618,'morph':{'Casus':'ABLATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'his'}]}],'name':'hic@PRO'}]"
                            >his</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1245784,'ls':[{'id':7011,'name':'persona','wfs':[{'id':1245786,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'personis'}]}],'name':'persona@NN'}]"
                            >personis</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]"
                            >qui</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1322669,'ls':[{'id':964024,'name':'res','wfs':[{'id':242825948,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'res'}]}],'name':'res@NN'}]"
                            >res</w>
                        <c> </c>
                        <w
                            lemma="[{'id':244511757,'ls':[{'id':244511758,'name':'suus','wfs':[{'id':245550209,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'suas'}]}],'name':'suus@PRO'}]"
                            >suas</w>
                        <c> </c>
                        <w
                            lemma="[{'id':183546,'ls':[{'id':183547,'name':'ideo','wfs':[{'id':183548,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'ideo'}]}],'name':'ideo@ADV'}]"
                            >ideo</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]"
                            >in</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1594858,'ls':[{'id':1594859,'name':'alter','wfs':[{'id':1594918,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'alteram'}]}],'name':'alter@ADJ'}]"
                            >alteram</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1245784,'ls':[{'id':7011,'name':'persona','wfs':[{'id':1245793,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'personam'}]}],'name':'persona@NN'}]"
                            >personam</w>
                        <c> </c>
                        <w
                            lemma="[{'id':4794348,'ls':[{'id':3180,'name':'delego','wfs':[{'id':4794648,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'delegant'}]}],'name':'delego@V'}]"
                            >delegant</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':110061,'ls':[{'id':110062,'name':'ut','wfs':[{'id':110063,'morph':{'Pos':'CON'},'name':'ut'}]}],'name':'ut@CON'}]"
                            >ut</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109453,'ls':[{'id':109454,'name':'ad','wfs':[{'id':109455,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'ad@AP'}]"
                            >ad</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1256749,'ls':[{'id':1256750,'name':'placitum','wfs':[{'id':1256758,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'placitum'}]}],'name':'placitum@NN'}]"
                            >placitum</w>
                        <c> </c>
                        <w
                            lemma="[{'id':7964639,'ls':[{'id':9166,'name':'venio','wfs':[{'id':7965122,'morph':{'ConjugationType':'FOURTH_CONJUGATION','Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'INTRANSITIVE','Voice':'ACTIVE'},'name':'venire'}]}],'name':'venio@V'}]"
                            >venire</w>
                        <c> </c>
                        <w
                            lemma="[{'id':206035,'ls':[{'id':6416,'name':'non','wfs':[{'id':241322627,'morph':{'Pos':'ADV'},'name':'non'}]}],'name':'non@ADV'}]"
                            >non</w>
                        <c> </c>
                        <w
                            lemma="[{'id':4319444,'ls':[{'id':244530660,'name':'conpello','wfs':[{'id':4320709,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'conpellantur'}]}],'name':'compello@V'}]"
                            >conpellantur</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':110061,'ls':[{'id':110062,'name':'ut','wfs':[{'id':110063,'morph':{'Pos':'CON'},'name':'ut'}]}],'name':'ut@CON'}]"
                            >ut</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1154228,'ls':[{'id':5923,'name':'malum','wfs':[{'id':1154229,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'mala'}]}],'name':'malum@NN'}]"
                            >mala</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':109962,'ls':[{'id':109963,'name':'que','wfs':[{'id':109964,'morph':{'Pos':'CON'},'name':'que'}]}],'name':'que@CON'}]"
                            >que</w>
                        <c> </c>
                        <w
                            lemma="[{'id':5475468,'ls':[{'id':4230,'name':'facio','wfs':[{'id':5475523,'morph':{'Casus':'ACCUSATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'facta'}]}],'name':'facio@V'}]"
                            >facta</w>
                        <c> </c>
                        <w
                            lemma="[{'id':5618354,'ls':[{'id':4757,'name':'habeo','wfs':[{'id':5618654,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'habent'}]}],'name':'habeo@V'}]"
                            >habent</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':206035,'ls':[{'id':6416,'name':'non','wfs':[{'id':241322627,'morph':{'Pos':'ADV'},'name':'non'}]}],'name':'non@ADV'}]"
                            >non</w>
                        <c> </c>
                        <w
                            lemma="[{'id':5210352,'ls':[{'id':3851,'name':'emendo','wfs':[{'id':5210655,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'emendent'}]}],'name':'emendo@V'}]"
                            >emendent</w>
                        <c>:</c>
                        <c> </c>
                        <w
                            lemma="[{'id':109994,'ls':[{'id':109995,'name':'quodsi','wfs':[{'id':109996,'morph':{'Pos':'CON'},'name':'quodsi'}]}],'name':'quodsi@CON'}]"
                            >quodsi</w>
                        <c> </c>
                        <w
                            lemma="[{'id':6525932,'ls':[{'id':6787,'name':'pareo','wfs':[{'id':6526390,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'INTRANSITIVE','Voice':'ACTIVE'},'name':'paruerit'}]}],'name':'pareo@V'}]"
                            >paruerit</w>
                        <c> </c>
                        <w
                            lemma="[{'id':183546,'ls':[{'id':183547,'name':'ideo','wfs':[{'id':183548,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'ideo'}]}],'name':'ideo@ADV'}]"
                            >ideo</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':242816216,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'eas'}]}],'name':'is@PRO'}]"
                            >eas</w>
                        <c> </c>
                        <w
                            lemma="[{'id':4794348,'ls':[{'id':3180,'name':'delego','wfs':[{'id':245538527,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INFINITIVE','Pos':'V','Tense':'PERFECT','Voice':'ACTIVE'},'name':'delegasse'}]}],'name':'delego@V'}]"
                            >delegasse</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':109868,'ls':[{'id':109869,'name':'ne','wfs':[{'id':109870,'morph':{'Pos':'CON'},'name':'ne'}]}],'name':'ne@CON'}]"
                            >ne</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1123571,'ls':[{'id':1123572,'name':'iustitia','wfs':[{'id':1123581,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'iustitiam'}]}],'name':'iustitia@NN'}]"
                            >iustitiam</w>
                        <c> </c>
                        <w
                            lemma="[{'id':5475468,'ls':[{'id':4230,'name':'facio','wfs':[{'id':5475745,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'IMPERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'facerent'}]}],'name':'facio@V'}]"
                            >facerent</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':8038609,'ls':[{'id':9327,'name':'volo','wfs':[{'id':243116354,'morph':{'Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_1','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'volumus'}]}],'name':'volo@V'}]"
                            >volumus</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':110061,'ls':[{'id':110062,'name':'ut','wfs':[{'id':110063,'morph':{'Pos':'CON'},'name':'ut'}]}],'name':'ut@CON'}]"
                            >ut</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816153,'ls':[{'id':242816154,'name':'ipse','wfs':[{'id':244509974,'morph':{'Casus':'NOMINATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ipsae'}]}],'name':'ipse@PRO'}]"
                            >ipsae</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1322669,'ls':[{'id':964024,'name':'res','wfs':[{'id':242825943,'morph':{'Casus':'NOMINATIVE','DeclensionType':'FIFTH_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'res'}]}],'name':'res@NN'}]"
                            >res</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]"
                            >in</w>
                        <c> </c>
                        <w
                            lemma="[{'id':806963,'ls':[{'id':806964,'name':'bannus','wfs':[{'id':806973,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'bannum'}]}],'name':'bannus@NN'}]"
                            >bannum</w>
                        <c> </c>
                        <w
                            lemma="[{'id':6256794,'ls':[{'id':6151,'name':'mitto','wfs':[{'id':6257076,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'mittantur'}]}],'name':'mitto@V'}]"
                            >mittantur</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':226546,'ls':[{'id':226547,'name':'quousque','wfs':[{'id':226549,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'quousque'}]}],'name':'quousque@ADV'}]"
                            >quousque</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816114,'ls':[{'id':242816115,'name':'ille','wfs':[{'id':244509845,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illi'}]}],'name':'ille@PRO'}]"
                            >illi</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]"
                            >qui</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':242816216,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'eas'}]}],'name':'is@PRO'}]"
                            >eas</w>
                        <c> </c>
                        <w
                            lemma="[{'id':5618354,'ls':[{'id':4757,'name':'habeo','wfs':[{'id':5618654,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'habent'}]}],'name':'habeo@V'}]"
                            >habent</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':796579,'ls':[{'id':1181,'name':'auctor','wfs':[{'id':243264117,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'auctorem'}]}],'name':'auctor@NN'}]"
                            >auctorem</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109453,'ls':[{'id':109454,'name':'ad','wfs':[{'id':109455,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'ad@AP'}]"
                            >ad</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1256749,'ls':[{'id':1256750,'name':'placitum','wfs':[{'id':1256758,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'placitum'}]}],'name':'placitum@NN'}]"
                            >placitum</w>
                        <c> </c>
                        <w
                            lemma="[{'id':3437243,'ls':[{'id':214,'name':'adduco','wfs':[{'id':3437524,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'adducant'}]}],'name':'adduco@V'}]"
                            >adducant</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]"
                            >et</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816067,'ls':[{'id':242816068,'name':'idem','wfs':[{'id':244509761,'morph':{'Casus':'DATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'isdem'}]}],'name':'idem@PRO'}]"
                            >isdem</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1153452,'ls':[{'id':5934,'name':'malefactor','wfs':[{'id':1153465,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'malefactor'}]}],'name':'malefactor@NN'}]"
                            >malefactor</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1123571,'ls':[{'id':1123572,'name':'iustitia','wfs':[{'id':1123581,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'iustitiam'}]}],'name':'iustitia@NN'}]"
                            >iustitiam</w>
                        <c> </c>
                        <w
                            lemma="[{'id':5475468,'ls':[{'id':4230,'name':'facio','wfs':[{'id':5475805,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'SINGULAR','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'faciat'}]}],'name':'facio@V'}]"
                            >faciat</w>
                        <c>.</c>
                        <pb n="79"/>
                    </s>
                </p>
            </div>
        </body>
    </text>
</TEI>
