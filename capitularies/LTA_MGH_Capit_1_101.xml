<?xml version="1.0" encoding="UTF-8"?><?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">MGH Capitularia 1: 101. Karoli capitula italica</title>
                <author xml:lang="lat" ref="viaf:160658152">Carolus Magnus imperator
                    et rex Francorum</author>
                <author xml:lang="ger" ref="viaf:160658152">Karl der Große, Kaiser
                    und König der Franken</author>
                <editor corresp="#LTACorpusEditor" key="TG"><persName
                        ref="http://viaf.org/viaf/185313129"
                            ><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor corresp="#LTACorpusEditor" key="AE"
                            ><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor corresp="#LTACorpusPublisher" key="FW"
                            ><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.mgh.de" type="provider">
                        <orgName>Monumenta Germaniae Historica (MGH)</orgName>
                        <orgName>Bavarian State Library (BSB), München</orgName></orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"
                            ><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"
                            ><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the<ref
                                target="https://www.texttechnologylab.org/applications/ehumanities-desktop/"
                                >eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de">Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW)</orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"></ref></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <p>LTA Edition 1.0</p>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution"
                        xml:lang="eng">Berlin-Brandenburg Academy of Sciences and
                        Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address
                        n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-05-20">2021-05-20</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">08</idno>
                <idno type="C1">08</idno>
                <idno type="C2">09</idno>
                <idno type="Time">790 - 810</idno>
                <idno type="VIAF_(Expression)">182997976</idno>
                <idno type="VIAF_(Expression)_Link">http://viaf.org/viaf/182997976</idno>
                <idno type="VIAF_(Person)">not available</idno>
                <idno type="VIAF_(Person)_Link">not available</idno>
                <idno type="Year_of_Publication_(interpreted)">0810</idno>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Capitvlaria regvm Francorvm 1</title>
                        <editor ref="http://viaf.org/viaf/2606355"><forename>Alfred</forename>
                            <surname>Boretius</surname></editor>
                    </titleStmt>
                    <publicationStmt>
                        <publisher>Hahn</publisher>
                        <pubPlace>Hannover</pubPlace>
                        <date>1881</date>
                    </publicationStmt>
                    <seriesStmt>
                        <title> MGH Capit.</title>
                        <biblScope unit="volume">1</biblScope>
                    </seriesStmt>
                </biblFull>
                <bibl type="Edition">MGH Capitularia 1</bibl>
                <bibl type="Volume">Capit. 1</bibl>
                <bibl type="Column">208</bibl>
                <bibl type="Bibliographical_Link"
                    >http://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=214325024</bibl>
                <bibl type="URL">http://www.mgh.de/dmgh/resolving/MGH_Capit._1_S._208</bibl>
                <bibl type="Capitularia_Edition">link not available yet</bibl>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p><desc type="Digital_Edition_Statement">The text has been retrieved from the
                        critical MGH edition and prepared by the <ref
                            target="https://capitularia.uni-koeln.de/">Capitularia Project</ref> at
                        Cologne University and by the CompHistSem workgroup at Goethe University
                        Frankfurt using the Historical Semantics Corpus Management (HSCM) on the
                            <ref
                            target="https://www.texttechnologylab.org/applications/ehumanities-desktop/"
                            >eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc><desc type="Status">controlled manually</desc><desc
                        type="Commentary">dated according to the edition, see also <ref
                            target="https://capitularia.uni-koeln.de/en/capit/pre814/bk-nr-101"
                            >“Karoli Magni capitula Italica” [BK 101]</ref>, in: Capitularia.
                        Edition of the Frankish Capitularies, ed. by Karl Ubl and collaborators,
                        Cologne 2014 ff. (TG)</desc>
                </p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Capitulary</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Frankish Capitularies</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange>
                <change type="Annotation_based_on">Edition</change>
                <change type="Annotation_created_by">Tim Geelhaar</change>
                <change type="Annotation_created_on">2016-03-10</change>
                <change type="Annotation_last_changed_by">Tim Geelhaar</change>
                <change type="Annotation_last_changed_on">Wed May 19 17:57:28 CEST 2021</change>
            </listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head n="1">
                    <s>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >101</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':489396,'ls':[{'id':489397,'name':'Karolus','wfs':[{'id':489409,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NP'},'name':'Karoli'}]}],'name':'Karolus@NP'}]"
                            >KAROLI</w>
                        <c> </c>
                        <w
                            lemma="[{'id':831740,'ls':[{'id':1588,'name':'capitulum','wfs':[{'id':831746,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'capitula'}]}],'name':'capitulum@NN'}]"
                            >CAPITULA</w>
                        <c> </c>
                        <w
                            lemma="[{'id':241538658,'ls':[{'id':241538659,'name':'Italicus','wfs':[{'id':243574464,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'Italica'}]}],'name':'Italicus@ADJ'}]"
                            >ITALICA</w>
                        <c>.</c>
                        <c> </c>
                    </s>
                    <s>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >790</w>
                        <c> </c>
                        <c>–</c>
                        <c> </c>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >810</w>
                        <c>?</c>
                    </s>
                </head>
                <p>
                    <s>
                        <w
                            lemma="[{'id':109986,'ls':[{'id':109987,'name':'quod','wfs':[{'id':109988,'morph':{'Pos':'CON'},'name':'quod'}]}],'name':'quod@CON'}]"
                            >Quod</w>
                        <c> </c>
                        <w
                            lemma="[{'id':958093,'ls':[{'id':958094,'name':'deus','wfs':[{'id':958105,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'deo'}]}],'name':'deus@NN'}]"
                            >Deo</w>
                        <c> </c>
                        <w
                            lemma="[{'id':6252239,'ls':[{'id':6137,'name':'miseror','wfs':[{'id':6252708,'morph':{'Casus':'ABLATIVE','ConjugationType':'FIRST_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'SINGULAR','Pos':'V','Tense':'PRESENT','VerbType':'DEPONENT','Voice':'ACTIVE'},'name':'miserante'}]}],'name':'miseror@V'}]"
                            >miserante</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1034174,'ls':[{'id':4372,'name':'filius','wfs':[{'id':252932508,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'filii'}]}],'name':'filius@NN'}]"
                            >filii</w>
                        <c> </c>
                        <w
                            lemma="[{'id':244510439,'ls':[{'id':244510440,'name':'noster','wfs':[{'id':244510465,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'nostri'}]}],'name':'noster@PRO'}]"
                            >nostri</w>
                        <c> </c>
                        <w
                            lemma="[{'id':731884,'ls':[{'id':731908,'name':'etas','wfs':[{'id':243244789,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'etatem'}]}],'name':'aetas@NN'}]"
                            >etatem</w>
                        <c> </c>
                        <w
                            lemma="[{'id':5618354,'ls':[{'id':4757,'name':'habeo','wfs':[{'id':5618370,'morph':{'Casus':'NOMINATIVE','ConjugationType':'SECOND_CONJUGATION','Genus':'MASCULINE','Mood':'PARTICIPLE','Numerus':'PLURAL','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'habentes'}]}],'name':'habeo@V'}]"
                            >habentes</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1010527,'ls':[{'id':1010528,'name':'exceptus','wfs':[{'id':1010539,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'excepto'}]}],'name':'exceptus@NN'}]"
                            >excepto</w>
                        <c> </c>
                        <w
                            lemma="[{'id':2770718,'ls':[{'id':2770719,'name':'paternus','wfs':[{'id':2770782,'morph':{'Casus':'ABLATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'paterna'}]}],'name':'paternus@ADJ'}]"
                            >paterna</w>
                        <c> </c>
                        <w
                            lemma="[{'id':903100,'ls':[{'id':2555,'name':'consolatio','wfs':[{'id':243301046,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'consolatione'}]}],'name':'consolatio@NN'}]"
                            >consolatione</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109610,'ls':[{'id':109611,'name':'per','wfs':[{'id':109612,'morph':{'Pos':'AP'},'name':'per'}]}],'name':'per@AP'}]"
                            >per</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816792,'ls':[{'id':242816793,'name':'sui','wfs':[{'id':242816794,'morph':{'Casus':'ACCUSATIVE','Genus':'COMMON','Numerus':'SINGULAR','Pos':'PRO','PronounType':'REFLEXIVE'},'name':'se'}]}],'name':'sui@PRO'}]"
                            >se</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1842878,'ls':[{'id':1842879,'name':'ceterus','wfs':[{'id':1842889,'morph':{'Casus':'DATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'ceteris'}]}],'name':'ceterus@ADJ'}]"
                            >ceteris</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1068431,'ls':[{'id':4856,'name':'homo','wfs':[{'id':243358892,'morph':{'Casus':'DATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'hominibus'}]}],'name':'homo@NN'}]"
                            >hominibus</w>
                        <c> </c>
                        <w
                            lemma="[{'id':6754078,'ls':[{'id':244530739,'name':'praecello','wfs':[{'id':6754165,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'praecellere'}]}],'name':'praecello@V'}]"
                            >praecellere</w>
                        <c> </c>
                        <w
                            lemma="[{'id':4694501,'ls':[{'id':3027,'name':'debeo','wfs':[{'id':4694801,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'debent'}]}],'name':'debeo@V'}]"
                            >debent</w>
                        <c>.</c>
                    </s>
                </p>
                <p>
                    <s>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >2</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]"
                            >De</w>
                        <c> </c>
                        <w
                            lemma="[{'id':902616,'ls':[{'id':902617,'name':'consiliarius','wfs':[{'id':243300805,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'consiliariis'}]}],'name':'consiliarius@NN'}]"
                            >consiliariis</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]"
                            >et</w>
                        <c> </c>
                        <w
                            lemma="[{'id':110061,'ls':[{'id':110062,'name':'ut','wfs':[{'id':110063,'morph':{'Pos':'CON'},'name':'ut'}]}],'name':'ut@CON'}]"
                            >ut</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816114,'ls':[{'id':242816115,'name':'ille','wfs':[{'id':244509845,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illi'}]}],'name':'ille@PRO'}]"
                            >illi</w>
                        <c> </c>
                        <w
                            lemma="[{'id':234266,'ls':[{'id':8236,'name':'semper','wfs':[{'id':241336670,'morph':{'Pos':'ADV'},'name':'semper'}]}],'name':'semper@ADV'}]"
                            >semper</w>
                        <c> </c>
                        <w
                            lemma="[{'id':3852536,'ls':[{'id':1192,'name':'audio','wfs':[{'id':3852843,'morph':{'ConjugationType':'FOURTH_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'audiantur'}]}],'name':'audio@V'}]"
                            >audiantur</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]"
                            >qui</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109453,'ls':[{'id':109454,'name':'ad','wfs':[{'id':109455,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'ad@AP'}]"
                            >ad</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1282225,'ls':[{'id':7444,'name':'profectus','wfs':[{'id':241469402,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FOURTH_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'profectum'}]}],'name':'profectus@NN'}]"
                            >profectum</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]"
                            >et</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1443775,'ls':[{'id':9082,'name':'utilitas','wfs':[{'id':243491567,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'utilitatem'}]}],'name':'utilitas@NN'}]"
                            >utilitatem</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1940098,'ls':[{'id':2182,'name':'communis','wfs':[{'id':1940158,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'ADJ'},'name':'communem'}]}],'name':'communis@ADJ'}]"
                            >communem</w>
                        <c> </c>
                        <w
                            lemma="[{'id':902655,'ls':[{'id':2542,'name':'consilium','wfs':[{'id':243300861,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'consilia'}]}],'name':'consilium@NN'}]"
                            >consilia</w>
                        <c> </c>
                        <w
                            lemma="[{'id':244511757,'ls':[{'id':244511758,'name':'suus','wfs':[{'id':245550215,'morph':{'Casus':'ACCUSATIVE','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'sua'}]}],'name':'suus@PRO'}]"
                            >sua</w>
                        <c> </c>
                        <w
                            lemma="[{'id':6914468,'ls':[{'id':7447,'name':'profero','wfs':[{'id':6914631,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'proferunt'}]}],'name':'profero@V'}]"
                            >proferunt</w>
                        <c>;</c>
                        <c> </c>
                        <w
                            lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]"
                            >qui</w>
                        <c> </c>
                        <w
                            lemma="[{'id':252583,'ls':[{'id':9197,'name':'vero','wfs':[{'id':550000524,'morph':{'Pos':'ADV'},'name':'vero'}]}],'name':'vero@ADV'}]"
                            >vero</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]"
                            >de</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1287034,'ls':[{'id':1287035,'name':'proprium','wfs':[{'id':243435176,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'propriis'}]}],'name':'proprium@NN'}]"
                            >propriis</w>
                        <c> </c>
                        <w
                            lemma="[{'id':218458,'ls':[{'id':218459,'name':'pote','wfs':[{'id':218460,'morph':{'ComparisonDegree':'COMPARATIVE','Pos':'ADV'},'name':'potius'}]}],'name':'pote@ADV'}]"
                            >potius</w>
                        <c> </c>
                        <w
                            lemma="[{'id':225505,'ls':[{'id':7683,'name':'quam','wfs':[{'id':225506,'morph':{'Pos':'ADV'},'name':'quam'}]}],'name':'quam@ADV'}]"
                            >quam</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]"
                            >de</w>
                        <c> </c>
                        <w
                            lemma="[{'id':881277,'ls':[{'id':881278,'name':'communa','wfs':[{'id':881280,'morph':{'Casus':'ABLATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'communis'}]}],'name':'communa@NN'}]"
                            >communis</w>
                        <c> </c>
                        <w
                            lemma="[{'id':4492596,'ls':[{'id':2537,'name':'considero','wfs':[{'id':4492677,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'considerare'}]}],'name':'considero@V'}]"
                            >considerare</w>
                        <c> </c>
                        <w
                            lemma="[{'id':7393257,'ls':[{'id':7393258,'name':'solo','wfs':[{'id':7393561,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'solent'}]}],'name':'solo@V'}]"
                            >solent</w>
                        <c> </c>
                        <w
                            lemma="[{'id':7123261,'ls':[{'id':7834,'name':'reicio','wfs':[{'id':7123705,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'reiciantur'}]}],'name':'reicio@V'}]"
                            >reiciantur</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]"
                            >de</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1143697,'ls':[{'id':5796,'name':'locus','wfs':[{'id':1143710,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'loco'}]}],'name':'locus@NN'}]"
                            >loco</w>
                        <c> </c>
                        <w
                            lemma="[{'id':902616,'ls':[{'id':902617,'name':'consiliarius','wfs':[{'id':243300807,'morph':{'Casus':'GENITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'consiliariorum'}]}],'name':'consiliarius@NN'}]"
                            >consiliariorum</w>
                        <c>.</c>
                    </s>
                </p>
                <p>
                    <s>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >3</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':226320,'ls':[{'id':226321,'name':'quomodo','wfs':[{'id':241332192,'morph':{'Pos':'ADV'},'name':'quomodo'}]}],'name':'quomodo@ADV'}]"
                            >Quomodo</w>
                        <c> </c>
                        <w
                            lemma="[{'id':839994,'ls':[{'id':1705,'name':'causa','wfs':[{'id':840003,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'causam'}]}],'name':'causa@NN'}]"
                            >causam</w>
                        <c> </c>
                        <w
                            lemma="[{'id':241818243,'ls':[{'id':241818244,'name':'confinalis','wfs':[{'id':241818282,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'confinales'}]}],'name':'confinalis@ADJ'}]"
                            >confinales</w>
                        <c> </c>
                        <w
                            lemma="[{'id':244510439,'ls':[{'id':244510440,'name':'noster','wfs':[{'id':244510465,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'nostri'}]}],'name':'noster@PRO'}]"
                            >nostri</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1208313,'ls':[{'id':6587,'name':'odium','wfs':[{'id':243409207,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'odio'}]}],'name':'odium@NN'}]"
                            >odio</w>
                        <c> </c>
                        <w
                            lemma="[{'id':234266,'ls':[{'id':8236,'name':'semper','wfs':[{'id':241336670,'morph':{'Pos':'ADV'},'name':'semper'}]}],'name':'semper@ADV'}]"
                            >semper</w>
                        <c> </c>
                        <w
                            lemma="[{'id':5618354,'ls':[{'id':4757,'name':'habeo','wfs':[{'id':5618654,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'habent'}]}],'name':'habeo@V'}]"
                            >habent</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109535,'ls':[{'id':109536,'name':'contra','wfs':[{'id':109537,'morph':{'Pos':'AP'},'name':'contra'}]}],'name':'contra@AP'}]"
                            >contra</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816114,'ls':[{'id':242816115,'name':'ille','wfs':[{'id':242816139,'morph':{'Casus':'ACCUSATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'illos'}]}],'name':'ille@PRO'}]"
                            >illos</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]"
                            >qui</w>
                        <c> </c>
                        <w
                            lemma="[{'id':6527957,'ls':[{'id':6786,'name':'paro','wfs':[{'id':6528270,'morph':{'ConjugationType':'FIRST_CONJUGATION','Genus':'MASCULINE','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'parati'}]}],'name':'paro@V'}]"
                            >parati</w>
                        <c> </c>
                        <w
                            lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':242825914,'morph':{'Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'VERBA_ANOMALA','Voice':'ACTIVE'},'name':'sunt'}]}],'name':'sum@V'}]"
                            >sunt</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1098285,'ls':[{'id':1098286,'name':'inimicus','wfs':[{'id':1098289,'morph':{'Casus':'DATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'inimicis'}]}],'name':'inimicus@NN'}]"
                            >inimicis</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1101786,'ls':[{'id':1101787,'name':'insidiae','wfs':[{'id':502870425,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'PLURAL','Pos':'NN'},'name':'insidiasque'}]}],'name':'insidiae@NN'}]"
                            >insidias</w>
                        <c> </c>
                        <w
                            lemma="[{'id':5475468,'ls':[{'id':4230,'name':'facio','wfs':[{'id':5475552,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'facere'}]}],'name':'facio@V'}]"
                            >facere</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]"
                            >et</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1157529,'ls':[{'id':1157530,'name':'marca','wfs':[{'id':1157539,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'marcam'}]}],'name':'marca@NN'}]"
                            >marcam</w>
                        <c> </c>
                        <w
                            lemma="[{'id':244510439,'ls':[{'id':244510440,'name':'noster','wfs':[{'id':244510441,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'nostram'}]}],'name':'noster@PRO'}]"
                            >nostram</w>
                        <c> </c>
                        <w
                            lemma="[{'id':3616081,'ls':[{'id':736,'name':'amplio','wfs':[{'id':3616162,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INFINITIVE','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'ampliare'}]}],'name':'amplio@V'}]"
                            >ampliare</w>
                        <c>.</c>
                        <pb n="209"/>
                    </s>
                </p>
            </div>
        </body>
    </text>
</TEI>
