<?xml version="1.0" encoding="UTF-8"?><?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">MGH Capitularia 2: 223. Widonis regis capitulum singillatim
                    traditum</title>
                <author xml:lang="lat" ref="viaf:34833945">Wido imperator et rex Italiae</author>
                <author xml:lang="ger" ref="viaf:34833945">Wido, Kaiser und König Italiens</author>
                <editor corresp="#LTACorpusEditor" key="TG"><persName
                        ref="http://viaf.org/viaf/185313129"
                            ><surname>Geelhaar</surname><forename>Tim</forename></persName></editor>
                <editor corresp="#LTACorpusEditor" key="AE"
                            ><persName><surname>Ernst</surname><forename>Alexandra</forename></persName></editor>
                <editor corresp="#LTACorpusPublisher" key="FW"
                            ><persName><surname>Wiegand</surname><forename>Frank</forename></persName></editor>
                <respStmt>
                    <orgName ref="http://www.mgh.de" type="provider">
                        <orgName>Monumenta Germaniae Historica (MGH)</orgName>
                        <orgName>Bavarian State Library (BSB), München</orgName></orgName>
                    <resp><note type="remarkResponsibility">Provider of the source
                        material</note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Ernst</surname><forename>Alexandra</forename></persName>
                    <persName ref="http://viaf.org/viaf/185313129"
                            ><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Preparation of the
                        TEI-document</note></resp>
                </respStmt>
                <respStmt>
                    <persName ref="http://viaf.org/viaf/185313129"
                            ><surname>Geelhaar</surname><forename>Tim</forename></persName>
                    <resp><note type="remarkResponsibility">Tokenization, Sentence-Split and
                            Lemmatization using the<ref
                                target="https://www.texttechnologylab.org/applications/ehumanities-desktop/"
                                >eHumanities Desktop</ref></note></resp>
                </respStmt>
                <respStmt>
                    <persName><surname>Wiegand</surname><forename>Frank</forename></persName>
                    <resp><note type="remarkResponsibility">Implementation in the Latin Text
                            Archive</note></resp>
                </respStmt>
                <respStmt>
                    <orgName ref="https://www.bbaw.de">Berlin-Brandenburgische Akademie der
                        Wissenschaften (BBAW)</orgName>
                    <resp><note type="remarkResponsibility">Long-term provision of the Latin Text
                            Archive</note><ref target="https://www.bbaw.de"></ref></resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <p>LTA Edition 1.0</p>
            </editionStmt>
            <publicationStmt>
                <publisher xml:id="LTACorpusPublisher"><orgName n="1" role="hostingInstitution"
                        xml:lang="eng">Berlin-Brandenburg Academy of Sciences and
                        Humanities</orgName><orgName role="project">Latin Text
                        Archive</orgName><email>lta@bbaw.de</email><address n="1">
                        <addrLine>Jägerstr. 22/23, 10117 Berlin</addrLine>
                    </address></publisher>
                <publisher xml:id="LTACorpusEditor"><orgName role="project">Latin Text
                        Archive</orgName><email>jussen@em.uni-frankfurt.de</email><email>geelhaar@em.uni-frankfurt.de</email><address
                        n="2">
                        <addrLine>Nobert-Wollheim-Platz 1, 60629 Frankfurt am Main</addrLine>
                    </address></publisher>
                <pubPlace>Frankfurt am Main</pubPlace>
                <date type="publication-online" when="2021-05-20">2021-05-20</date>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"><p>CC BY-NC
                            4.0</p></licence>
                </availability>
                <idno type="C_Stat">09</idno>
                <idno type="C1">09</idno>
                <idno type="C2">09</idno>
                <idno type="Time">889.02.00</idno>
                <idno type="VIAF_(Expression)">182997976</idno>
                <idno type="VIAF_(Expression)_Link">http://viaf.org/viaf/182997976</idno>
                <idno type="VIAF_(Person)">34833945</idno>
                <idno type="VIAF_(Person)_Link">http://viaf.org/viaf/34833945</idno>
                <idno type="Year_of_Publication_(interpreted)">0889</idno>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Capitvlaria regvm Francorvm 2</title>
                        <editor ref="http://viaf.org/viaf/2606355"><forename>Alfred</forename>
                            <surname>Boretius</surname></editor>
                    </titleStmt>
                    <publicationStmt>
                        <publisher>Hahn</publisher>
                        <pubPlace>Hannover</pubPlace>
                        <date>1897</date>
                    </publicationStmt>
                    <seriesStmt>
                        <title> MGH Capit.</title>
                        <biblScope unit="volume">2</biblScope>
                    </seriesStmt>
                </biblFull>
                <bibl type="Edition">MGH Capitularia 2</bibl>
                <bibl type="Volume">Capit. 2</bibl>
                <bibl type="Column">106</bibl>
                <bibl type="Bibliographical_Link"
                    >http://stabikat.de/DB=1/XMLPRS=N/PPN?PPN=214328406</bibl>
                <bibl type="URL">http://www.mgh.de/dmgh/resolving/MGH_Capit._2_S._106</bibl>
                <bibl type="Capitularia_Edition">link not available yet</bibl>
                <bibl></bibl>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p>
                    <desc type="Digital_Edition_Statement">The text has been retrieved from the
                        critical MGH edition and prepared by the <ref
                            target="https://capitularia.uni-koeln.de/">Capitularia Project</ref> at
                        Cologne University and by the CompHistSem workgroup at Goethe University
                        Frankfurt using the Historical Semantics Corpus Management (HSCM) on the
                            <ref
                            target="https://www.texttechnologylab.org/applications/ehumanities-desktop/"
                            >eHumanities Desktop</ref>. This edition does not replace the printed
                        edition as it offers only the main reading of the text for analytical
                        purposes. Brackets, hyphenation, and quotation marks have been removed. The
                        Medieval Latin orthography and the pagination of the edition have been
                        preserved.</desc>
                    <desc type="Status">controlled manually</desc>
                    <desc type="Commentary">dated according to the edition (TG)</desc></p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
            </langUsage>
            <textClass>
                <classCode scheme="Text_Type">Capitulary</classCode>
                <classCode scheme="Text_Type_Class">Legal</classCode>
                <classCode scheme="Type_of_Corpus">Corpus of Frankish Capitularies</classCode>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange>
                <change type="Annotation_based_on">Edition</change>
                <change type="Annotation_created_by">Tim Geelhaar</change>
                <change type="Annotation_created_on">2016-03-10</change>
                <change type="Annotation_last_changed_by">Tim Geelhaar</change>
                <change type="Annotation_last_changed_on">Wed May 19 19:31:07 CEST 2021</change>
            </listChange>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head n="1">
                    <s>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >223</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':689274,'ls':[{'id':689275,'name':'Wido','wfs':[{'id':241396036,'morph':{'Pos':'NP'},'name':'widonis'}]}],'name':'Wido@NP'}]"
                            >WIDONIS</w>
                        <c> </c>
                        <w
                            lemma="[{'id':7119425,'ls':[{'id':7820,'name':'rego','wfs':[{'id':7119656,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'SINGULAR','Person':'PERSON_2','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'regis'}]}],'name':'rego@V'}]"
                            >REGIS</w>
                        <c> </c>
                        <w
                            lemma="[{'id':831740,'ls':[{'id':1588,'name':'capitulum','wfs':[{'id':831753,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'SINGULAR','Pos':'NN'},'name':'capitulum'}]}],'name':'capitulum@NN'}]"
                            >CAPITULUM</w>
                        <c> </c>
                        <w
                            lemma="[{'id':235905,'ls':[{'id':235906,'name':'singillatim','wfs':[{'id':235907,'morph':{'ComparisonDegree':'POSITIVE','Pos':'ADV'},'name':'singillatim'}]}],'name':'singillatim@ADV'}]"
                            >SINGILLATIM</w>
                        <c> </c>
                        <w
                            lemma="[{'id':7814011,'ls':[{'id':8888,'name':'trado','wfs':[{'id':7814086,'morph':{'Casus':'NOMINATIVE','ConjugationType':'THIRD_CONJUGATION','Genus':'NEUTER','Mood':'PARTICIPLE','Numerus':'SINGULAR','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'PASSIVE'},'name':'traditum'}]}],'name':'trado@V'}]"
                            >TRADITUM</w>
                        <c>.</c>
                        <c> </c>
                    </s>
                    <s>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >889</w>
                        <c>.</c>
                        <w
                            lemma="[{'id':1029595,'ls':[{'id':1029596,'name':'februarius','wfs':[{'id':503560474,'morph':{'Pos':'NN'},'name':'febr'}]}],'name':'februarius@NN'}]"
                            >Febr</w>
                        <c>.</c>
                        <c>–</c>
                        <c> </c>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >891</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':1029595,'ls':[{'id':1029596,'name':'februarius','wfs':[{'id':503560474,'morph':{'Pos':'NN'},'name':'febr'}]}],'name':'februarius@NN'}]"
                            >Febr</w>
                        <c>.</c>
                    </s>
                </head>
                <p>
                    <s>
                        <w
                            lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]"
                            >De</w>
                        <c> </c>
                        <w
                            lemma="[{'id':864007,'ls':[{'id':1960,'name':'clericus','wfs':[{'id':864009,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'clericis'}]}],'name':'clericus@NN'}]"
                            >clericis</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]"
                            >qui</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]"
                            >in</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816449,'ls':[{'id':242816450,'name':'quicumque','wfs':[{'id':244511045,'morph':{'Pos':'PRO','PronounType':'INDEFINITE'},'name':'quacumque'}]}],'name':'quicumque@PRO'}]"
                            >quacumque</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1352388,'ls':[{'id':8220,'name':'seditio','wfs':[{'id':243458352,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'seditione'}]}],'name':'seditio@NN'}]"
                            >seditione</w>
                        <c> </c>
                        <w
                            lemma="[{'id':780999,'ls':[{'id':781000,'name':'arma','wfs':[{'id':243258649,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'arma'}]}],'name':'arma@NN'}]"
                            >arma</w>
                        <c> </c>
                        <w
                            lemma="[{'id':7755891,'ls':[{'id':8690,'name':'suscipio','wfs':[{'id':7757026,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PERFECT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'susceperint'}]}],'name':'suscipio@V'}]"
                            >susceperint</w>
                        <c>.</c>
                        <c> </c>
                    </s>
                    <s>
                        <w
                            lemma="[{'id':689274,'ls':[{'id':689275,'name':'Wido','wfs':[{'id':689276,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Numerus':'SINGULAR','Pos':'NP'},'name':'Wido'}]}],'name':'Wido@NP'}]"
                            >Wido</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1329481,'ls':[{'id':7965,'name':'rex','wfs':[{'id':1329499,'morph':{'Casus':'NOMINATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'rex'}]}],'name':'rex@NN'}]"
                            >rex</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':109545,'ls':[{'id':109546,'name':'de','wfs':[{'id':109547,'morph':{'Pos':'AP'},'name':'de'}]}],'name':'de@AP'}]"
                            >De</w>
                        <c> </c>
                        <w
                            lemma="[{'id':864007,'ls':[{'id':1960,'name':'clericus','wfs':[{'id':864009,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'clericis'}]}],'name':'clericus@NN'}]"
                            >clericis</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':296826592,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'RELATIVE'},'name':'qui'}]}],'name':'qui@PRO'}]"
                            >qui</w>
                        <c> </c>
                        <w
                            lemma="[{'id':780999,'ls':[{'id':781000,'name':'arma','wfs':[{'id':243258649,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'arma'}]}],'name':'arma@NN'}]"
                            >arma</w>
                        <c> </c>
                        <w
                            lemma="[{'id':3889069,'ls':[{'id':3889347,'name':'baiulo','wfs':[{'id':3889620,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'baiulant'}]}],'name':'baiulo@V'}]"
                            >baiulant</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]"
                            >et</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]"
                            >in</w>
                        <c> </c>
                        <w
                            lemma="[{'id':244511757,'ls':[{'id':244511758,'name':'suus','wfs':[{'id':245550206,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'PRO','PronounType':'POSSESSIVE'},'name':'suis'}]}],'name':'suus@PRO'}]"
                            >suis</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1287034,'ls':[{'id':1287035,'name':'proprium','wfs':[{'id':243435176,'morph':{'Casus':'ABLATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'propriis'}]}],'name':'proprium@NN'}]"
                            >propriis</w>
                        <c> </c>
                        <w
                            lemma="[{'id':7197248,'ls':[{'id':7915,'name':'resideo','wfs':[{'id':7198077,'morph':{'ConjugationType':'SECOND_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'resident'}]}],'name':'resideo@V'}]"
                            >resident</w>
                        <c> </c>
                        <c>(</c>
                        <w
                            lemma="[{'id':241346594,'ls':[{'id':241346595,'name':'NUMERAL','wfs':[{'id':241346596,'morph':{'Pos':'NUM'},'name':'NUMERAL'}]}],'name':'NUMERAL@NUM'}]"
                            >i</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':7617143,'ls':[{'id':7617144,'name':'sum','wfs':[{'id':539853814,'morph':{'Pos':'V'},'name':'e'}]}],'name':'sum@V'}]"
                            >e</w>
                        <c>.</c>
                        <c> </c>
                        <w
                            lemma="[{'id':5619231,'ls':[{'id':4759,'name':'habito','wfs':[{'id':5619531,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'habitant'}]}],'name':'habito@V'}]"
                            >habitant</w>
                        <c>)</c>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':109453,'ls':[{'id':109454,'name':'ad','wfs':[{'id':109455,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'ad@AP'}]"
                            >ad</w>
                        <c> </c>
                        <w
                            lemma="[{'id':985146,'ls':[{'id':3766,'name':'ecclesia','wfs':[{'id':985183,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'FIRST_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'ecclesiam'}]}],'name':'ecclesia@NN'}]"
                            >ecclesiam</w>
                        <c> </c>
                        <w
                            lemma="[{'id':110016,'ls':[{'id':110017,'name':'seu','wfs':[{'id':110018,'morph':{'Pos':'CON'},'name':'seu'}]}],'name':'seu@CON'}]"
                            >seu</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109453,'ls':[{'id':109454,'name':'ad','wfs':[{'id':109455,'morph':{'Pos':'AP'},'name':'ad'}]}],'name':'ad@AP'}]"
                            >ad</w>
                        <c> </c>
                        <w
                            lemma="[{'id':998014,'ls':[{'id':3910,'name':'episcopus','wfs':[{'id':998023,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'SINGULAR','Pos':'NN'},'name':'episcopum'}]}],'name':'episcopus@NN'}]"
                            >episcopum</w>
                        <c> </c>
                        <w
                            lemma="[{'id':206035,'ls':[{'id':6416,'name':'non','wfs':[{'id':241322627,'morph':{'Pos':'ADV'},'name':'non'}]}],'name':'non@ADV'}]"
                            >non</w>
                        <c> </c>
                        <w
                            lemma="[{'id':6240530,'ls':[{'id':6240531,'name':'milito','wfs':[{'id':6240831,'morph':{'ConjugationType':'FIRST_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'INTRANSITIVE','Voice':'ACTIVE'},'name':'militant'}]}],'name':'milito@V'}]"
                            >militant</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':1188647,'ls':[{'id':6369,'name':'negotium','wfs':[{'id':243401857,'morph':{'Casus':'ACCUSATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'NN'},'name':'negotia'}]}],'name':'negotium@NN'}]"
                            >negotia</w>
                        <c> </c>
                        <w
                            lemma="[{'id':3004211,'ls':[{'id':242581151,'name':'secularis','wfs':[{'id':244279029,'morph':{'Casus':'ACCUSATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'THIRD_DECLENSION','Genus':'NEUTER','Numerus':'PLURAL','Pos':'ADJ'},'name':'secularia'}]}],'name':'saecularis@ADJ'}]"
                            >secularia</w>
                        <c> </c>
                        <w
                            lemma="[{'id':3560733,'ls':[{'id':556,'name':'ago','wfs':[{'id':3561010,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'INDICATIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'agunt'}]}],'name':'ago@V'}]"
                            >agunt</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':109577,'ls':[{'id':109578,'name':'in','wfs':[{'id':109579,'morph':{'Pos':'AP'},'name':'in'}]}],'name':'in@AP'}]"
                            >in</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1137961,'ls':[{'id':5724,'name':'lex','wfs':[{'id':243384648,'morph':{'Casus':'ABLATIVE','DeclensionType':'THIRD_DECLENSION','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'NN'},'name':'lege'}]}],'name':'lex@NN'}]"
                            >lege</w>
                        <c> </c>
                        <w
                            lemma="[{'id':8032801,'ls':[{'id':9316,'name':'vivo','wfs':[{'id':8034295,'morph':{'ConjugationType':'THIRD_CONJUGATION','Mood':'SUBJUNCTIVE','Numerus':'PLURAL','Person':'PERSON_3','Pos':'V','Tense':'PRESENT','VerbType':'TRANSITIVE','Voice':'ACTIVE'},'name':'vivant'}]}],'name':'vivo@V'}]"
                            >vivant</w>
                        <c> </c>
                        <w
                            lemma="[{'id':235568,'ls':[{'id':8310,'name':'sicut','wfs':[{'id':235569,'morph':{'Pos':'ADV'},'name':'sicut'}]}],'name':'sicut@ADV'}]"
                            >sicut</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]"
                            >et</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1842878,'ls':[{'id':1842879,'name':'ceterus','wfs':[{'id':1842892,'morph':{'Casus':'NOMINATIVE','ComparisonDegree':'POSITIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'ADJ'},'name':'ceteri'}]}],'name':'ceterus@ADJ'}]"
                            >ceteri</w>
                        <c> </c>
                        <w
                            lemma="[{'id':241373014,'ls':[{'id':241373015,'name':'Langobardi','wfs':[{'id':241373016,'morph':{'Pos':'NP'},'name':'Langobardi'}]}],'name':'Langobardi@NP'}]"
                            >Langobardi</w>
                        <c>,</c>
                        <c> </c>
                        <w
                            lemma="[{'id':245290,'ls':[{'id':8736,'name':'tam','wfs':[{'id':245291,'morph':{'Pos':'ADV'},'name':'tam'}]}],'name':'tam@ADV'}]"
                            >tam</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816153,'ls':[{'id':242816154,'name':'ipse','wfs':[{'id':244509977,'morph':{'Casus':'NOMINATIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'DEMONSTRATIVE'},'name':'ipsi'}]}],'name':'ipse@PRO'}]"
                            >ipsi</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816363,'ls':[{'id':242816364,'name':'qui','wfs':[{'id':244510910,'morph':{'Casus':'ACCUSATIVE','Genus':'FEMININE','Numerus':'SINGULAR','Pos':'PRO','PronounType':'RELATIVE'},'name':'quam'}]}],'name':'qui@PRO'}]"
                            >quam</w>
                        <c> </c>
                        <w
                            lemma="[{'id':109828,'ls':[{'id':109829,'name':'et','wfs':[{'id':109830,'morph':{'Pos':'CON'},'name':'et'}]}],'name':'et@CON'}]"
                            >et</w>
                        <c> </c>
                        <w
                            lemma="[{'id':1034174,'ls':[{'id':4372,'name':'filius','wfs':[{'id':252932508,'morph':{'Casus':'NOMINATIVE','DeclensionType':'SECOND_DECLENSION','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'NN'},'name':'filii'}]}],'name':'filius@NN'}]"
                            >filii</w>
                        <c> </c>
                        <w
                            lemma="[{'id':242816207,'ls':[{'id':242816208,'name':'is','wfs':[{'id':244510043,'morph':{'Casus':'GENITIVE','Genus':'MASCULINE','Numerus':'PLURAL','Pos':'PRO','PronounType':'PERSONAL'},'name':'eorum'}]}],'name':'is@PRO'}]"
                            >eorum</w>
                        <c>.</c>
                        <pb n="107"/>
                    </s>
                </p>
            </div>
        </body>
    </text>
</TEI>
